#!/bin/bash
#
# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

# -------------------------------------------------------------------------- #

cENVNAME="amsel-py"

function main() {
    local toolsdir="$cBASEDIR/tools"

    local profilelog="./profile.log"
    local profiling=false
    local profile=()

    if [[ "$1" == "profile" ]]; then
        echo "===== PROFILING RUN ====="
        printf -- "%s\n" "writing profile log to: $profilelog"

        shift
        profiling=true
        profile=(-m cProfile -o "$profilelog")
    fi

    if [[ "$1" == "tool" || "$1" == "tools" || "$1" == "to" ]]; then
        if [[ -z "$2" ]]; then
            echo "error: no tool selected"
            exit 128
        fi

        if [[ ! -d "$toolsdir" ]]; then
            echo "error: could not find tools installation directory" >&2
            printf -- "%s\n" "expected to find $toolsdir" >&2
            exit 255
        fi

        if [[ "$2" == "list" || "$2" == "ls" ]]; then
            echo "Available tools are:"
            mapfile -d $'\0' -t found_tools < <(find "$toolsdir" -maxdepth 2 -mindepth 1 -iname "*.py" -print0)

            for i in "${found_tools[@]}"; do
                printf -- "- %s\n" "$(realpath --relative-to="$toolsdir" -- "$(dirname -- "$i")")/$(basename --suffix .py -- "$i")"
            done
        elif [[ "$2" == "-h" || "$2" == "--help" ]]; then
            printf -- "%s\n" "** Amsel tools **" \
                             "" \
                             "The 'amsel tool' command is a shorthand for calling external tools" \
                             "building on the amsel library. Run 'amsel tool list' (or 'amsel to ls')" \
                             "to get a list of available tools." \
                             "" \
                             "Each tool has its own command line interface and provides usage details" \
                             "through the '-h' or '--help' arguments." \
                             "" \
                             "Example: to extract the XML representation of the sheep unit, run the" \
                             "following tool commands:" \
                             "" \
                             "    amsel tool combine-figuren" \
                             "    amsel tool pselect Figuren.dat 'ANNO/FIGUREN/ENTRY@103' -nx" \
                             "" \
                             "" \
                             "** Custom tools **" \
                             "" \
                             "You can add custom tools by dropping a Python file in amsel's tools/ directory." \
                             "Simply copy an existing tool to get started."
        else
            selected="$toolsdir/${2%.py}.py"

            if [[ ! -f "$selected" ]]; then
                local found_tools=()
                local found=()
                mapfile -d $'\0' -t found_tools < <(find "$toolsdir" -maxdepth 2 -mindepth 1 -iname "*.py" -print0)

                for i in "${found_tools[@]}"; do
                    printable="$(realpath --relative-to="$toolsdir" -- "$(dirname -- "$i")")/$(basename --suffix .py -- "$i")"

                    if [[ "$printable" == "./$2"* ]]; then
                        matched+=("$i")
                    elif [[ "$printable" == *"/$2"* ]]; then
                        matched+=("$i")
                    fi
                done

                if (( ${#matched[@]} == 1 )); then
                    selected="${matched[0]}"
                elif (( ${#matched[@]} > 1 )); then
                    printf -- "%s\n" "error: more than one tool matched '$2'" >&2
                    printf -- "- %s\n" "${matched[@]}" | sed 's/\.py$//g' >&2
                    exit 255
                else
                    printf -- "%s\n" "error: unknown tool '$2'" >&2
                    printf -- "%s\n" "could not find $selected" >&2
                    exit 255
                fi
            fi

            shift 2
            PYTHONPATH="$cBASEDIR:$PYTHONPATH" python3 "${profile[@]}" "$selected" "$@"
        fi
    else
        PYTHONPATH="$cBASEDIR:$PYTHONPATH" python3 "${profile[@]}" -m amsel "$@"
    fi

    if [[ "$profiling" == true ]]; then
        python3 -m pstats "$profilelog" <<< "$(printf "%s\n" "sort cumulative" "stats")" > "$profilelog.cumulative.txt"
        python3 -m pstats "$profilelog" <<< "$(printf "%s\n" "sort nfl" "stats")" > "$profilelog.nfl.txt"
        python3 -m pstats "$profilelog" <<< "$(printf "%s\n" "sort time" "stats")" > "$profilelog.time.txt"
    fi
}

# -------------------------------------------------------------------------- #

cBASEDIR="$(dirname -- "$(realpath -- "$0")")"
cWORKDIR="$(pwd)"

pushd "$cBASEDIR" >/dev/null || {
    echo "error: cannot find base directory" >&2
    exit 1
}

cENVDIR="$cBASEDIR/env/$cENVNAME"

if [[ -d "$cENVDIR" ]]; then
    source -- "$cENVDIR/bin/activate"
else
    echo "initializing Python virtual environment..."

    mkdir -p "$cENVDIR"
    python3 -m venv "$cENVDIR"
    printf -- "%s\n" "$cENVNAME" > "$cBASEDIR/.venv"

    source -- "$cENVDIR/bin/activate"

    pip3 install --upgrade pip
    pip3 install -r "$cBASEDIR/requirements.txt"
fi

popd >/dev/null

main "$@"
