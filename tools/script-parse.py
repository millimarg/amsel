#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import sys
import argparse

from parsimonious.grammar import Grammar

from amsel.parser import Parser


# TODO use a Visitor class to build a Kate syntax definition from the parsed tree
# TODO include script docs in generated docs


def main(script: str) -> None:
    grammar = Grammar(r"""
        expr            = ws (  comment
                              / section_objdef
                              / section_areadef
                              / emptyline
                          )*
        comment         = ws dollar ws anything

        section_objdef  = objdef_head (  comment
                                       / objdef_entry
                                       / emptyline
                          )*
        objdef_head     = lpar "OBJECT_DEFINE" rpar ws
        objdef_entry    = (objdef_unit / objdef_city / objdef_site)
        objdef_unit     = "OBJECT_FIGUR" ws quoted ws number
        objdef_city     = "OBJECT_STADT" ws quoted ws number
        objdef_site     = "OBJECT_ANLAGE" ws quoted ws number comma number comma number

        section_areadef = areadef_head (  comment
                                        / areadef_entry
                                        / emptyline
                          )*
        areadef_head    = lpar "AREA_DEFINE" rpar ws
        areadef_entry   = quoted ws number comma ws number comma ws number comma ws number

        # unused
        entry          = section pair*
        section        = lpar word rpar ws
        pair           = key equal value ws?
        text           = ~"[A-Z 0-9]*"
        key            = word+
        value          = (word / quoted)+
        word           = ~r"[-\w]+"
        equal          = ws? "=" ws?

        quoted         = ~'"[^\"]+"'
        number         = ~"[0-9]+"
        lpar           = "["
        rpar           = "]"
        dollar         = "$"
        comma          = ","
        anything       = ~".*"
        ws             = ~"\s*"
        emptyline      = ws+
    """)

    tree = grammar.parse(script)
    print(tree)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse scripts from scenario files.')
    parser.add_argument('scenario', type=str, help='extracted input file to parse (use script-extract to get the file)')
    # parser.add_argument('outfile', type=str, help='output file to write')
    # parser.add_argument('--force', '-f', action="store_true", help='overwrite existing files')
    args = parser.parse_args()

    if args.scenario.endswith('.a2s'):
        with open(args.scenario, 'r') as f:
            script = f.read()

        print(f"loaded script from {args.scenario}")
    else:
        with Parser(args.scenario) as p:
            try:
                p.parsePath('ANNO/SCENE/BIGBROTHER/*')
                script = p.parsedRoot.getFirstPath('ANNO/SCENE/BIGBROTHER/data')
            except Exception:
                print(f"failed to parse {args.scenario}")
                sys.exit(1)

            if not script:
                print(f"no script found in {args.scenario}")
                sys.exit(1)
            else:
                script = script.decodeToStr()

            print(f"extracted script from {args.scenario}")

    # now to the parsing...
    main(script)
