#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
import timeit
import logging as log

from amsel.parser import NewDataFactory
from amsel.spec import SpecNodeFactory


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build empty structures from a spec.')
    parser.add_argument('xmlfile', type=str, help='input file to convert')
    parser.add_argument('outfile', type=str, help='output file to write')

    group_ex = parser.add_mutually_exclusive_group(required=True)
    group_ex.add_argument('--xml', '-x', action="store_true", help='save as XML')
    group_ex.add_argument('--bin', '-b', action="store_true", help='save as binary')

    args = parser.parse_args()

    spec = SpecNodeFactory.loadSpecFile(args.xmlfile)
    root = NewDataFactory.makeEmptyNodes(spec)
    root.printTree()

    if args.xml:
        log.info(timeit.timeit(lambda: root.bakeXmlToFile(args.outfile), number=1, setup='gc.enable()'))
    else:
        log.info(timeit.timeit(lambda: root.bakeToFile(args.outfile), number=1, setup='gc.enable()'))
