#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
import sys
import shutil
from pathlib import Path
from bs4 import BeautifulSoup


def split(args):
    if not Path(args.infile).exists():
        print(f"error: input file '{args.infile}' does not exist")
        sys.exit(2)

    output = Path(args.outdir)

    if output.exists() and not args.force:
        print(f"error: output path '{output}' exists (use -F to overwrite)")
        sys.exit(2)
    else:
        shutil.rmtree(output, ignore_errors=True)
        output.mkdir()

    print("loading... (this may take a while for large extracts)")
    with open(args.infile, 'r') as fd:
        sp = BeautifulSoup(fd.read(), "xml")

    print("splitting...")
    for i, node in enumerate(sp.select(args.selector)):
        uid = 'noid'
        if args.idselect:
            if sel := node.select_one(args.idselect):
                uid = f"{int(sel.attrs.get('value', '0')):04d}"

        title = 'untitled'
        if args.nameselect:
            if sel := node.select_one(args.nameselect):
                title = sel.attrs.get('value', 'untitled')

        print(f'saving #{i:-4d}: {uid} {title}')

        to_save = BeautifulSoup('', 'xml')
        wrapper = sp.new_tag('file', attrs={"source": sp.file["source"],
                                            "title": f'{uid}_{title}_n{i:04d}',
                                            "type": f'{sp.file["type"]}@{args.selector}',
                                            })
        to_save.append(node.wrap(wrapper))

        with open(f'{output}/{uid}_{title}_n{i:04d}.xml', 'w') as fd:
            fd.write(to_save.prettify())

    print("done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Split XML files. Either specify the selectors '
                                                 '(at least "selector" is required), or use a predefined '
                                                 'set of selectors (e.g. -a for "anlagen" extracts).')
    parser.add_argument('infile', type=str, help='input file to analyse')
    parser.add_argument('outdir', type=str, help='CSS selector for getting a name from each part')
    parser.add_argument('selector', type=str, help='CSS selector for parts to extract', default='', nargs='?')
    parser.add_argument('idselect', type=str,
                        help='CSS selector for getting an ID from each part', default='', nargs='?')
    parser.add_argument('nameselect', type=str,
                        help='CSS selector for getting a name from each part', default='', nargs='?')
    parser.add_argument('--force', '-F', action='store_true', default=False,
                        help='remove output directory if it exists')

    group = parser.add_argument_group('Predefined splitters')
    group_ex = group.add_mutually_exclusive_group()
    group_ex.add_argument('--anlagen', '-a', action='store_true', default=False,
                          help='split "anlagen" entries')
    group_ex.add_argument('--anlagen-mod', '-m', action='store_true', default=False,
                          help='split "anlagen" entries from "Anlagen.mod" extracts')
    group_ex.add_argument('--figuren', '-f', action='store_true', default=False,
                          help='split "figuren" entries')

    args = parser.parse_args()

    if args.anlagen:
        args.selector = 'file > head > head > head'
        args.idselect = '#entry-id'
        args.nameselect = 'head[title="TITLE"] > data'
    elif args.anlagen_mod:
        args.selector = 'file > head > head'
        args.idselect = '#entry-id'
        args.nameselect = 'head[title="TITLE"] > data'
    elif args.figuren:
        args.selector = 'file > head > head > head'
        args.idselect = '#entry-id'
        args.nameselect = 'head[title="PATH"] > data'
    elif not args.selector:
        print("error: no selector specified")
        sys.exit(1)

    split(args)
