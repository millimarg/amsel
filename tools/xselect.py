#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
from bs4 import BeautifulSoup

from amsel.tools import save_xml


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test CSS selectors on XML files.')
    parser.add_argument('select', type=str, help='CSS selector')
    parser.add_argument('infile', type=str, help='input file to analyse')
    parser.add_argument('outfile', default='-', nargs="?", type=str,
                        help='optional output file to save selected nodes')
    parser.add_argument('--force', '-f', action='store_true', default=False,
                        help='replace output file with result')

    args = parser.parse_args()

    with open(args.infile, 'r') as fd:
        sp = BeautifulSoup(fd.read(), 'xml')

    selected = sp.select(args.select)
    tosave = BeautifulSoup('', 'xml')

    for sel in selected:
        tosave.append(sel)

    save_xml(args.outfile, tosave, args.force)
