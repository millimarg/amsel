#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
from functools import cache

from typing import List
from typing import Tuple
from typing import Dict
from typing import Any

import png

import amsel.tools as tools
from amsel.tools import setup_logging
from amsel.parser import Parser
from amsel.parser import ParsedNode

from amsel.decoders import DecIslandTexture
from amsel.decoders import DecColorList
from amsel.graphics import Palette
# from amsel.graphics import Image


setup_logging(logfile=None)


def read_chunked_data(node: ParsedNode, chunk_id_base: str, compact: bool = False) -> List[List[Any]]:
    parts = []
    for i in node.findNodesByIdent(f'#{chunk_id_base}-chunk'):
        parts.append({'x': i.findFirstByIdent(f'#{chunk_id_base}-chunk-x').decodeToPy(),
                      'y': i.findFirstByIdent(f'#{chunk_id_base}-chunk-y').decodeToPy(),
                      'w': i.findFirstByIdent(f'#{chunk_id_base}-chunk-w').decodeToPy(),
                      'h': i.findFirstByIdent(f'#{chunk_id_base}-chunk-h').decodeToPy(),
                      ('fields' if compact else 'rows'): i.findFirstByIdent(f'#{chunk_id_base}-chunk-data').decodeToPy()})

    return tools.combine_matrix(parts, compact=compact)


def heightmap(args):
    with Parser(dataFile=args.infile) as parser:
        parser.parseFully()
        matrix = read_chunked_data(parser.parsedRoot, 'height')

    if not args.as_png:
        print(matrix)
    else:
        tools.save_matrix_png(matrix, 'single', '-', force=True)


def texture(args):
    with Parser(dataFile=args.infile) as parser:
        parser.parseFully()
        matrix = read_chunked_data(parser.parsedRoot, 'texture')

    if args.raw:
        print(matrix)
    else:
        tools.save_matrix_png(matrix, 'attr', '-', force=True, r='base', g='quart', b='ori')


def vegetation(args):
    with Parser(dataFile=args.infile) as parser:
        parser.parseFully()
        matrix = read_chunked_data(parser.parsedRoot, 'vegetation', compact=True)

    if args.raw:
        print(matrix)
    else:
        tools.save_matrix_png(matrix, 'attr', '-', force=True, r='item_id', g='height', b='height', compact=True)


def obj_object(args):
    with Parser(dataFile=args.infile) as parser:
        parser.parseFully()
        # heightmap = read_chunked_data(parser.parsedRoot, 'height')
        # vegetation = read_chunked_data(parser.parsedRoot, 'vegetation', compact=True)
        # texture = read_chunked_data(parser.parsedRoot, 'texture')

    # n = [x['grow'] for x in flatten(vegetation) if x['grow'] != 0]
    # print(max(n), min(n))

    raise NotImplementedError()


def terrain_info(args):
    with Parser(dataFile=args.infile) as parser:
        parser.parseFully()
        height_matrix = read_chunked_data(parser.parsedRoot, 'height')
        texture_matrix = read_chunked_data(parser.parsedRoot, 'texture')

    for y, hty in enumerate(zip(height_matrix, texture_matrix)):
        height_row, tex_row = hty

        for x, htx in enumerate(zip(height_row, tex_row)):
            height, tex = htx

            # x,y,height,terrain,encoded,quarter,row,column,rotation
            print(f'{x} {y} {height} {tex.base} {tex._decoded_from} {tex.quart} {tex.row} {tex.col} {tex.ori}'.replace(' ', ','))


def mapview(args):
    tex_raw: Dict[int, bytes] = {}
    tex_pixels: Dict[Tuple[int, int, int, int, int], List[List[Tuple[int, int, int, int]]]] = {}
    palettes = {}

    quarts = {
        0: (0,   0), 3: (0,   128),
        1: (128, 0), 2: (128, 128)
    }

    cell_size = 42
    # cell_size = 256

    def rotate_clockwise(m):
        # 123      521
        # 233  =>  432
        # 543      333
        return [list(reversed([m[j][i] for j in range(len(m))])) for i in range(len(m[0]))]

    def rotate_counterclockwise(m):
        # 123      333
        # 233  =>  234
        # 543      125
        return [list(reversed(i)) for i in reversed(rotate_clockwise(m))]

    def flip_left_right(m):
        return [list(reversed(row)) for row in m]

    def flip_top_bottom(m):
        return list(reversed(m))

    @cache
    def indices(cell_tup):
        cell = DecIslandTexture.Data(*cell_tup)
        quart_x, quart_y = quarts[cell.quart]
        cell_x = cell_size * cell.col
        cell_y = cell_size * cell.row
        # cell_x = 42 * cell.col
        # cell_y = 42 * cell.row

        xn = [quart_x + cell_x + x for x in range(0, cell_size)]
        yn = [quart_y + cell_y + y for y in range(0, cell_size)]
        offsets = [x + 256 * y for y in yn for x in xn]

        # rot = [1, 2, 3, 4]
        # rotations = rot[cell.ori]
        rotations = cell.ori + 1

        offsets = tuple(tools.chunks(offsets, cell_size))

        for i in range(0, rotations):
            offsets = rotate_counterclockwise(offsets)

        offsets = flip_top_bottom(offsets)

        return tools.flatten(offsets)

    def load_texture(cell: DecIslandTexture.Data, tex_parser: Parser):
        nonlocal tex_pixels
        nonlocal tex_raw

        if cell.base not in tex_raw:
            if cell.base == 0:
                tex_pixels[cell.to_tup()] = [[DecColorList.Data(0, 0, 0, 0).rgb_tup for x in range(0, 42)] for y in range(0, 42)]
                return  # empty cell

            tex_raw[cell.base] = (
                tex_parser.tree.getFirstPath(f'ANNO/TEXTURE@{cell.base}/HEAD/#image-header').decodeToPy().palette_id,
                bytes(tex_parser.tree.getFirstPath(f'ANNO/TEXTURE@{cell.base}/BODY/#image-data').decodeToPy()),
            )

        if cell.to_tup() in tex_pixels:
            return

        pal_id, data = tex_raw[cell.base]
        offsets = indices(cell.to_tup())
        palette = palettes[pal_id]

        # print(f'cell {cell} loaded')
        tex_pixels[cell.to_tup()] = list(tools.chunks([palette.entries[data[offset]].rgb_tup for offset in offsets], cell_size))

    with Parser(args.textures, structure='tex') as tex:
        print("parsing textures...")
        tex.parseFully()

        for pal in tex.tree.getPaths('ANNO/COLOR'):
            ident = pal.findFirstByIdent('#palette-id').decodeToPy()
            body = pal.findFirstByIdent('#palette-entries').decodeToPy()
            palettes[ident] = Palette(uid=ident, entries=body)
            # print(f"palette {ident} loaded")

        print("palettes loaded")

        with Parser(dataFile=args.infile, structure='insel5') as src:
            print("parsing island...")
            src.parsePath('ANNO/INSEL5/#island-width')
            src.parsePath('ANNO/INSEL5/#island-height')
            src.parsePath('ANNO/INSEL5/#texture-chunk/*')

            width = src.tree.findFirstByIdent('#island-width').decodeToPy()
            height = src.tree.findFirstByIdent('#island-height').decodeToPy()
            matrix = read_chunked_data(src.tree.getFirstPath('ANNO/INSEL5'), 'texture')

            print("island body loaded")

        # width = 20
        # height = 20
        # width = 1
        # height = 1
        # start = 10

        start = 0
        matrix = [[cell for cell in row[start:start + width]] for row in matrix[start:start + height]]

        # matrix = [[DecIslandTexture.Data(99, 3, 2, 2, 0)]]
        # # # matrix = [[DecIslandTexture.Data(38, 0, 1, 2, 1)]] # 1123
        # matrix = [[DecIslandTexture.Data(38, 0, 0, 0, 0)]] # 0000
        # matrix = [[DecIslandTexture.Data(99, 0, 0, 0, 0)]] # 0000

        [load_texture(cell, tex) for row in matrix for cell in row]
        print("cell textures loaded")

    # list of rows of r, g, b, a (flat)
    pixel_rows = []

    for y in range(0, height):
        for i in range(0, cell_size):
            row = []

            for x in range(0, width):
                row += tools.flatten(tex_pixels[matrix[x][y].to_tup()][i])

            pixel_rows.append(row)

    png.from_array(pixel_rows, 'RGB').save(args.outfile)
    print(f"image saved to {args.outfile}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='...')
    subparsers = parser.add_subparsers(title='Commands', description='')

    parser_a = subparsers.add_parser('heightmap', help="extract an island's heightmap", aliases=['h'])
    parser_a.add_argument('infile', type=str, help="XML to process ('-' for stdin')", default='-', nargs='?')
    parser_a.add_argument('--as-png', '-P', action='store_true', help='generate a PNG image instead of CSV data')
    parser_a.set_defaults(func=heightmap)

    parser_b = subparsers.add_parser('texture', help="extract an island's terrain texture", aliases=['t'])
    parser_b.add_argument('infile', type=str, help="XML to process ('-' for stdin')", default='-', nargs='?')
    parser_b.add_argument('--as-png', '-P', action='store_true', help='generate a PNG image instead of CSV data')
    parser_b.add_argument('--raw', '-R', action='store_true', help='do not decode texture data')
    parser_b.set_defaults(func=texture)

    parser_c = subparsers.add_parser('vegetation', help="extract an island's vegetation map", aliases=['v'])
    parser_c.add_argument('infile', type=str, help="XML to process ('-' for stdin')", default='-', nargs='?')
    parser_c.add_argument('--as-png', '-P', action='store_true', help='generate a PNG image instead of CSV data')
    parser_c.add_argument('--raw', '-R', action='store_true', help='do not decode texture data')
    parser_c.set_defaults(func=vegetation)

    parser_d = subparsers.add_parser('island-obj', help="convert an island to a 3D object (OBJ)", aliases=['io'])
    parser_d.add_argument('infile', type=str, help="XML to process ('-' for stdin')", default='-', nargs='?')
    parser_d.set_defaults(func=obj_object)

    parser_e = subparsers.add_parser('map', help="create a full texture map of an island", aliases=['m'])
    parser_e.add_argument('infile', type=str, help="source island file")
    parser_e.add_argument('textures', type=str, default="Felder.tex", help="Felder.tex texture file")
    parser_e.add_argument('outfile', type=str, help="output file")
    parser_e.set_defaults(func=mapview)

    parser_f = subparsers.add_parser('terrain', help="extract an island's terrain info")
    parser_f.add_argument('infile', type=str, help="source island file")
    parser_f.set_defaults(func=terrain_info)

    args = parser.parse_args()

    if 'func' in args:
        args.func(args)
    else:
        parser.print_help()
