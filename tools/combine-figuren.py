#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
from amsel.tools import combine_figuren_data_files
from amsel.tools import setup_logging


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Build Figuren.dat from FigurenHeader.dat and FigurenXXX.dat.')
    args = parser.parse_args()

    setup_logging()
    combine_figuren_data_files()
    print("Finished.")
