#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
from amsel.decoders import DecFloat


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert float values to binary and reverse.')
    group_ex = parser.add_mutually_exclusive_group(required=True)
    group_ex.add_argument('--float', '-f', nargs='+', type=float, action="store", help='float value to convert')
    group_ex.add_argument('--bin', '-b', nargs='+', type=bytearray.fromhex, action="store", help='binary float value to convert')

    args = parser.parse_args()

    if args.float:
        for i in args.float:
            print(int(DecFloat.float_to_bin(i)).to_bytes(4, byteorder='little').hex().upper())
    elif args.bin:
        for i in args.bin:
            print(DecFloat.bin_to_float(int.from_bytes(i, byteorder='little', signed=False)))
