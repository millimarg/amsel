#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse

from amsel import parser
from amsel.tools import is_list


if __name__ == '__main__':
    argp = argparse.ArgumentParser(description='Test Path selectors on binary files.')
    argp.add_argument('infile', type=str, help='input file to analyse')
    argp.add_argument('path', type=str, help='path to search')
    group = argp.add_mutually_exclusive_group()
    group.add_argument('--no-count', '-n', action='store_true', help='do not print how many items were found')
    group.add_argument('--only-count', '-c', action='store_true', help='only print how many items were found')
    group = argp.add_mutually_exclusive_group()
    group.add_argument('--value', '-v', action='store_true', default=False,
                       help='print decoded values (must select data nodes)')
    group.add_argument('--value-single', '-V', action='store_true', default=False,
                       help='same as -v, but print list items separately')
    group.add_argument('--xml', '-x', action='store_true', default=False,
                      help='print XML representation of results')

    args = argp.parse_args()

    with parser.Parser(args.infile) as parser:
        parser.parsePath(args.path)
        found = parser.tree.getPaths(args.path)

        if not args.only_count:
            for i in found:
                if args.xml:
                    parser.parseFully(i)
                    [print(x) for x in i.bakeXml()]
                elif args.value and hasattr(i, 'decodeToPy'):
                    print(i.decodeToPy())
                elif args.value_single and hasattr(i, 'decodeToPy'):
                    value = i.decodeToPy()

                    if is_list(value):
                        [print(x) for x in value]
                    else:
                        print(value)
                else:
                    print(i)

        if not args.no_count:
            print(f"-- found {len(found)} items")
