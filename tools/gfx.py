#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
import sys
import logging as log
from pathlib import Path

import amsel.graphics as gfx
from amsel.tools import setup_logging
from amsel.tools import backup_existing_file
from amsel.tools import save_bin
from amsel.tools import save_any
from amsel.parser import Parser
from amsel.parser import Path as ParsePath
from amsel.decoders import DecImageHeader


setup_logging(logfile=None)


def palette_make(args):
    for i, infile in enumerate(args.infiles):
        log.info(f"loading palette from {infile}")
        pal = gfx.Palette()
        pal.from_file(infile)

        path = Path(infile)

        if args.name:
            pal.name = args.name

        if args.outbase:
            if len(args.infiles) > 1:
                pal.to_gpl(f'{args.outbase}_{i:04d}.gpl')
            else:
                if not args.name:
                    pal.name = f'{args.outbase}.gpl'
                pal.to_gpl(f'{args.outbase}.gpl')
        else:
            pal.to_gpl(path.stem + '.gpl')


def palette_merge(args):
    merged = gfx.Palette()
    merged.entries = []

    for i in args.infiles:
        log.info(f"loading palette from {i}")
        pal = gfx.Palette()
        pal.from_file(i)

        for col in pal.entries:
            if col not in merged.entries:
                merged.entries.append(col)
                log.info(f"- appending {col} (at {len(merged.entries)} entries)")

    log.info(f"merged palette contains {len(merged.entries)} entries")

    if len(merged.entries) < 256:
        log.info("padding to 256 entries...")
        merged.entries += [type(merged.entries[0])(0, 0, 0, 0)] * (256 - len(merged.entries))

    if args.name:
        merged.name = args.name
    elif len(args.infiles) > 1:
        merged.name = f'{args.infiles[0]} and {len(args.infiles)-1} others'
    else:
        merged.name = args.infiles[0]

    merged.entries = sorted(merged.entries)

    if args.outfile:
        merged.to_gpl(args.outfile)
    else:
        lines = merged.to_gpl()
        sys.stdout.write('\n'.join(lines))
        print()


def image_png_to_bsh(args):
    log.info("loading palette...")
    pal = gfx.Palette()
    pal.from_file(args.palette if args.palette else args.infile)

    log.info("loading image...")
    img = gfx.Image()
    img.from_png(args.infile, pal)

    if args.outbase:
        outbase = args.outbase
    else:
        outbase = Path(args.infile).stem

    Path(outbase).parent.mkdir(parents=True, exist_ok=True)

    head = f'{outbase}.head.txt'
    body = f'{outbase}.body.bin'
    body_hex = f'{outbase}.body.txt'
    palo = f'{outbase}.palette.gpl'

    backup_existing_file(head)
    backup_existing_file(body)
    backup_existing_file(body_hex)
    backup_existing_file(palo)

    pal.to_gpl(palo)

    with open(head, 'w') as fd:
        fd.write(f'Name: {img.name}\n\n')
        fd.write(f'Head: {img.head}\n\n')
        fd.write(f'Palette: {palo}\n')

    with open(body, 'wb') as fd:
        fd.write(img.data)

    with open(body_hex, 'w') as fd:
        fd.write(img.data.hex())


def image_png_to_txt(args):
    log.info("loading palette...")
    pal = gfx.Palette()
    pal.from_file(args.palette if args.palette else args.infile)

    log.info("loading image...")
    img = gfx.Image()
    img.from_png(args.infile, pal, targetFormatName=args.format)

    outfile = Path(args.outfile)
    Path(outfile).parent.mkdir(parents=True, exist_ok=True)
    img.to_ini(outfile)


def image_bsh_to_png(args):
    log.info("loading palette...")
    pal = gfx.Palette()
    pal.from_file(args.palette)

    log.info("loading body...")
    if args.binary:
        with open(args.bodyfile, 'rb') as fd:
            data = fd.read()
    else:
        with open(args.bodyfile, 'r') as fd:
            data = bytearray.fromhex(fd.read())

    log.info("loading head...")
    head = DecImageHeader.Data(*args.headstr.split(' '), with_shifted_center=False)

    log.info(f"saving {args.outfile}...")
    img = gfx.Image(head, data, pal, args.outfile)
    img.to_png(args.outfile)


def image_txt_to_png(args):
    log.info("loading image...")
    image = gfx.Image()
    image.from_ini(args.source)

    log.info(f"saving {args.outfile}...")
    image.to_png(args.outfile)


def extract_image_by_paths(args):
    parts = [('head', args.head), ('body', args.body), ('palette', args.palette)]

    log.info("parsing input...")

    with Parser(dataFile=args.infile, structure=args.structure[0],
                customSpecFile=args.custom_structure[0], part=args.part) as parser:
        for _, path in parts:
            # parse all so that an index selector actually selects the correct index below
            path = ParsePath(path)
            path.selector.kind = path.Selector.ALL
            parser.parsePath(path)

    loaded = {}

    for key, path in parts:
        log.info(f'loading {key}...')

        if node := parser.tree.getFirstPath(path):
            loaded[key] = node.decodeToPy()
        else:
            raise FileNotFoundError(f"{key} not found")

    log.info(f'building {args.outfile}...')

    head: DecImageHeader.Data = loaded['head']
    body = memoryview(loaded['body'][head.data_offset:head.data_offset + head.data_length])
    palette = gfx.Palette(entries=loaded['palette'])

    img = gfx.Image(head, body, palette, args.outfile)

    try:
        img.to_png(args.outfile)
    except Exception:
        log.info("failed to convert to PNG")

    if args.outfile != '-':
        save_any(args.outfile + ".txt", img.to_ini(), 'w', args.force)


def extract_image(args):
    # example: amsel to gfx x -f0 '56 54 0 0800 BSH_RGBZ 28 46 98693736 2290 00000000' -p pal-f506.lst figuren.dat out.png
    # example: amsel to gfx x -f0 '50 54 0 0800 BSH_RGBV 25 50 0 890 00000000' -p pal-f101.lst figuren.dat out.png

    log.info("loading head...")
    head = DecImageHeader.Data(*args.headstr.split(' '), with_shifted_center=False)

    standard_body_path = f'ANNO/TEXTURES@zoom-level-id:{args.figuren if args.figuren is not None else args.anlagen}/#image-data / 1'

    log.info("loading body...")
    with Parser(args.bodyfile) as parser:
        if args.bodypath:
            parser.parsePath(args.bodypath)
            block = parser.tree.getFirstPath(args.bodypath)
        elif (args.figuren is not None) or (args.anlagen is not None):
            parser.parsePath(standard_body_path)
            block = parser.tree.getFirstPath(standard_body_path)

        body = block.decodeToPy()[head.data_offset:head.data_offset + head.data_length]

    log.info(f"loading palette {args.palette}...")
    palette = gfx.Palette()

    if args.palette:
        palette.from_file(args.palette)
    else:
        log.info("note: using empty (black) palette")

    log.info(f"saving {args.outfile}...")

    # palette.to_gpl(args.outfile + ".gpl")
    img = gfx.Image(head, body, palette, args.outfile)

    try:
        img.to_png(args.outfile)
    except Exception:
        log.info("failed to convert to PNG")

    if args.outfile != '-':
        save_bin(args.outfile + ".dat", body, args.force)
        save_any(args.outfile + ".txt", img.to_ini(), 'w', args.force)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Work with graphics.')
    subparsers = parser.add_subparsers(title='Commands', description='')

    parser_a = subparsers.add_parser('palette-make', help='palette from png', aliases=['pmk'])
    parser_a.add_argument('infiles', nargs='+', type=str, help='Input files')
    parser_a.add_argument('--outbase', '-o', type=str, action="store", nargs='?',
                          help='Output file name base')
    parser_a.add_argument('--name', '-n', type=str, action="store", nargs='?',
                          help='Stored palette name')
    parser_a.set_defaults(func=palette_make)

    parser_b = subparsers.add_parser('palette-merge', help='merge palettes', aliases=['pmr'])
    parser_b.add_argument('infiles', nargs="+", type=str, help='Input files')
    parser_b.add_argument('--name', '-n', type=str, action="store", nargs='?',
                          help='Palette title')
    parser_b.add_argument('--outfile', '-o', type=str, action="store", nargs='?',
                          help='Output file name')
    parser_b.set_defaults(func=palette_merge)

    parser_c = subparsers.add_parser('image-png2bsh', help='convert PNG images to BSH_*', aliases=['ip2b', 'ic'])
    parser_c.add_argument('infile', type=str, help='Input file (PNG)')
    parser_c.add_argument('--palette', '-p', nargs="?", type=str, help='Palette to use')
    parser_c.add_argument('outbase', nargs='?', type=str, help='Output file name base')
    parser_c.set_defaults(func=image_png_to_bsh)

    parser_g = subparsers.add_parser('image-png2txt', help='convert PNG images to BSH_*', aliases=['ip2t'])
    parser_g.add_argument('infile', type=str, help='Input file (PNG)')
    parser_g.add_argument('outfile', type=str, help='Output file name')
    parser_g.add_argument('--format', '-f', type=str, help='Format name, e.g. BSH_RGBAV')
    parser_g.add_argument('--palette', '-p', nargs="?", type=str, help='Palette to use')
    parser_g.set_defaults(func=image_png_to_txt)

    parser_d = subparsers.add_parser('image-bsh2png', help='convert BSH_* and TEX_* images', aliases=['ib2p'])
    parser_d.add_argument('bodyfile', type=str, help='Input file with body data (hex-string)')
    parser_d.add_argument('headstr', type=str, help='Header string')
    parser_d.add_argument('palette', type=str, help='Palette file')
    parser_d.add_argument('--binary', '-b', action='store_true', help='Read body file as binary data')
    parser_d.add_argument('outfile', type=str, help='Output file')
    parser_d.set_defaults(func=image_bsh_to_png)

    parser_d = subparsers.add_parser('image-txt2png', help='convert INI image extracts to PNG', aliases=['it2p'])
    parser_d.add_argument('source', type=str, help='INI input file')
    parser_d.add_argument('outfile', type=str, help='Output file')
    parser_d.set_defaults(func=image_txt_to_png)

    parser_e = subparsers.add_parser('extract', help='extract an image by its header string', aliases=['x'])
    parser_e.add_argument('headstr', type=str, help='Header string (e.g. from XML extract)')
    parser_e.add_argument('bodyfile', type=str, help='Input file with body data')
    parser_e.add_argument('outfile', type=str, help='Output file')
    parser_e.add_argument('--force', '-f', action='store_true', help='Overwrite existing files')
    parser_e.add_argument('--palette', '-p', type=str, nargs='?', help='Palette file (png, xml, lst)')
    group_ex = parser_e.add_mutually_exclusive_group(required=True)
    group_ex.add_argument('--bodypath', '-b', type=str, help='Path to the data block in the data file')
    group_ex.add_argument('--figuren', '-F', choices=[0, 1, 2], type=int, help='Use Figuren data path with given zoom level')
    group_ex.add_argument('--anlagen', '-A', choices=[0, 1, 2], type=int, help='Use Anlagen data path with given zoom level')
    parser_e.set_defaults(func=extract_image)

    parser_f = subparsers.add_parser('extract-paths', help='extract an image by its path', aliases=['xp'])
    # Example:
    # amsel to gfx xp Figuren.dat \
    #   -H 'ANNO/FIGUREN/ENTRY@103/HAEUSER/ENTRY@anim-id:4/TEXTURES@zoom-level-id:0/#image-header / 1' \
    #   -B 'ANNO/TEXTURES@zoom-level-id:0/#image-data' \
    #   -P 'ANNO/FIGUREN/ENTRY@103/COLORS/ENTRY/BODY/#palette-entries' \
    #   -o extracted-image.png -f

    parser_f.add_argument('infile', type=str, help='Input file')
    parser_f.add_argument('--outfile', '-o', type=str, action="store", nargs='?',
                          help='Output file', default='-')
    parser_f.add_argument('--head', '-H', type=str, help='Path to the image header')
    parser_f.add_argument('--body', '-B', type=str, help='Path to the data block in the data file')
    parser_f.add_argument('--palette', '-P', type=str, help='Path to the palette entries')
    parser_f.add_argument('--structure', '-s', nargs=1, type=str, default=[''], action="store",
                          help='structure specification type to follow (optional)')
    parser_f.add_argument('--custom-structure', '-S', nargs=1, type=str, default=[''], action="store",
                          help='custom spec file to use (optional)')
    parser_f.add_argument('--part', '-p', type=str, action="store", nargs='?',
                          help='spec path for the expected part (e.g. "USDAT/PROFIL", requires a known structure)')
    parser_f.add_argument('--force', '-f', action='store_true', help='Overwrite existing files')
    parser_f.set_defaults(func=extract_image_by_paths)

    args = parser.parse_args()

    if 'func' in args:
        args.func(args)
    else:
        parser.print_help()
