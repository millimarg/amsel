#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
import sys
from bs4 import BeautifulSoup

from amsel.tools import save_xml


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Strip pointers and raw data from a generated spec file.')
    parser.add_argument('infile', type=str, help='input file to analyse')
    parser.add_argument('--force', '-f', action='store_true', default=False,
                        help='replace input file with result')

    args = parser.parse_args()

    with open(args.infile, 'r') as fd:
        sp = BeautifulSoup(fd.read(), 'xml')

    to_strip = ['value',
                'original-offset',
                'original-offset-hex',
                'original-length',
                ]

    for node in sp.find_all(True, recursive=True):
        for kill_key in to_strip:
            del node[kill_key]

    if args.force:
        output = args.infile
    else:
        output = sys.stdout

    save_xml(output, sp, args.force)
