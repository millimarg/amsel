#!/usr/bin/env python3
# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import sys
import argparse

from amsel.parser import Parser


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract scripts from scenario files.')
    parser.add_argument('scenario', type=str, help='input file to scan')
    parser.add_argument('outfile', type=str, help='output file to write')
    parser.add_argument('--force', '-f', action="store_true", help='overwrite existing files')
    args = parser.parse_args()

    with Parser(args.scenario) as p:
        try:
            p.parsePath('ANNO/SCENE/BIGBROTHER/*')
            script = p.parsedRoot.getFirstPath('ANNO/SCENE/BIGBROTHER/data')
        except Exception:
            print(f"failed to parse {args.scenario}")
            sys.exit(1)

        if not script:
            print(f"no script found in {args.scenario}")
            sys.exit(1)
        else:
            script = script.saveBodyString(args.outfile, overwrite=args.force)

        print(f"extracted script from {args.scenario}")
