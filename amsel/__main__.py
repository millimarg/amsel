# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import argparse
import sys
# import os
import logging as log
import timeit
from pathlib import Path

if not __package__:
    _msg = "Do not run this script directly. Run it using 'python3 -m amsel'."
    raise RuntimeError(_msg)

from . import tools
from . import spec
from . import docs
from . import filesystem
from .parser import Parser
from .parser import NewDataFactory
from .modding import Modder
from .modding import ValidationFailedError

from .legacy.parser import BinaryAnalyzer


def read_structure(args):
    log.info("reading structure: {}".format(args.infile))
    log.info("parsing...")
    parser = BinaryAnalyzer(args.infile).parse()
    log.info("parsing finished")
    parser.save_xml(sys.stdout, with_pointers=not args.no_pointers, load_all=not args.no_data)

    return 0


def mount_fuse(args):
    filesystem.mount(mountpath=args.mountdir, filename=args.infile,
                     structure=args.structure[0], customSpecFile=args.custom_structure[0],
                     part=args.part)

    return 0


def to_xml(args):
    if args.part and not args.structure:
        log.error("must specify 'structure' for 'part'")
        return 255

    log.info("building XML: {}".format(args.infile))
    log.info("parsing...")

    with Parser(dataFile=args.infile, structure=args.structure[0],
                customSpecFile=args.custom_structure[0], part=args.part) as parser:
        log.info(timeit.timeit(lambda: parser.parseFully(), number=1, setup='gc.enable()'))

    log.info("parsing finished")

    parser.parsedRoot.bakeXmlToFile(args.output[0], overwrite=args.force)

    return 0


def to_bin(args):
    log.info("building binary: {}".format(args.infile))
    log.info("loading xml...")
    root = NewDataFactory.loadDataXmlFile(args.infile)
    log.info("xml loaded")

    log.info("building binary...")
    log.info(timeit.timeit(lambda: root.bakeToFile(args.outfile, overwrite=args.force),
                           number=1, setup='gc.enable()'))
    log.info("done")

    return 0


def apply_mod(args):
    if args.file:
        presets = {x.split(':')[0]: ':'.join(x.split(':')[1:]) for x in args.file}
        log.info(f'using files: {presets}')
    else:
        presets = {}

    if args.var:
        variables = {x.split(':')[0]: ':'.join(x.split(':')[1:]) for x in args.var}
        log.info(f'using variables: {variables}')
    else:
        variables = {}

    with Modder(modfile=args.modfile, file_presets=presets, variables=variables) as mod:
        try:
            if args.save_resolved:
                mod.save_resolved(args.save_resolved, overwrite=args.force)

            if args.save_compiled:
                mod.save_compiled(args.save_compiled, overwrite=args.force, compress=False)

            if not args.no_apply:
                mod.do_apply(dry_run=args.dry_run,
                             save_separately=args.save_separately,
                             save_extracted=args.save_extracted,
                             make_backups=not args.no_backup)
        except ValidationFailedError:
            log.error("Mod validation failed. Please check the logs, fix the mod file, and try again.")
            log.error("Note: line numbers in error messages refer to lines in the resolved mod XML.")

    return 0


def validate_mod(args):
    for moddir in args.moddir:
        log.info(f"=== validating {moddir}")

        modxml = Path(moddir).absolute() / 'main.xml'

        if not modxml.exists():
            log.error(f'main mod XML file could not be found at {modxml}')
            log.error('note: the "mod" command gives you more freedom and more options')

        if args.rnc:
            method = 'rnc'
        elif args.xsd:
            method = 'xsd'
        else:
            method = 'rnc'

        with Modder(modfile=modxml) as mod:
            try:
                mod.validate(method)
            except ValidationFailedError:
                log.error("Mod validation failed. Please check the logs, fix the mod file, and try again.")
                log.error("Note: line numbers in error messages refer to lines in the resolved mod XML.")

    return 0


def resolve_mod(args):
    for moddir in args.moddir:
        log.info(f"=== resolving {moddir}")

        modxml = Path(moddir).absolute() / 'main.xml'
        outfile = Path().cwd() / Path(moddir).with_suffix('.resolved.xml')

        if not modxml.exists():
            log.error(f'main mod XML file could not be found at {modxml}')
            log.error('note: the "mod" command gives you more freedom and more options')

        with Modder(modfile=modxml) as mod:
            mod.save_resolved(outfile, overwrite=args.force)

    return 0


def compile_mod(args):
    for moddir in args.moddir:
        log.info(f"=== compiling {moddir}")

        modxml = Path(moddir).absolute() / 'main.xml'
        outfile = Path().cwd() / (Path(moddir).stem + '.a2x')

        if not modxml.exists():
            log.error(f'main mod XML file could not be found at {modxml}')
            log.error('note: the "mod" command gives you more freedom and more options')

        with Modder(modfile=modxml) as mod:
            try:
                mod.save_compiled(outfile, overwrite=args.force, compress=not args.no_compress)
            except ValidationFailedError:
                log.error("Mod validation failed. Please check the logs, fix the mod file, and try again.")
                log.error("Note: line numbers in error messages refer to lines in the resolved mod XML.")

    return 0


def dump_spec(args):
    loaded = spec.Spec.by_type(args.structure, args.part)
    tools.save_xml('-', loaded.xml)

    return 0


def build_docs(args):
    docs.build_docs(args)

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Amsel - Anno Modding System and Exploration Library',
        epilog='Additional tools are available when using the wrapper script. Call '
               '"amsel tool list" to list available tools. Tool names can be abbreviated.')
    subparsers = parser.add_subparsers(title='subcommands', description='')
    parser.add_argument('--debug', '-d', action='store_true', default=False,
                        help='activate debug info')
    parser.add_argument('--logfile', '-l', type=str, action="store", default="",
                        help='log output file')
    parser.add_argument('--quiet', '-q', action='store_true', default=False,
                        help='be less chatty')

    parser_a = subparsers.add_parser('scan', help='analyse the structure of a file', aliases=['s'])
    # We can't take stdin here because we need the complete file size.
    # parser_a.add_argument('infile', type=str, help='input file to analyse', default='-', nargs='?')
    parser_a.add_argument('infile', type=str, help='input file to analyse')
    parser_a.set_defaults(func=read_structure)
    parser_a.add_argument('--no-pointers', '-P', action='store_true',
                          help='do not include pointers to original sources')
    parser_a.add_argument('--no-data', '-N', action='store_true',
                          help='do not include data in output, print only structure')

    parser_j = subparsers.add_parser('mount', help='mount a file using FUSE', aliases=['m'])
    parser_j.add_argument('mountdir', type=str, help='mount point (must exist)')
    parser_j.add_argument('infile', type=str, help='input file to analyse')
    parser_j.add_argument('--structure', '-s', nargs=1, type=str, default=[''], action="store",
                          help='structure specification type to follow (optional)')
    parser_j.add_argument('--custom-structure', '-S', nargs=1, type=str, default=[''], action="store",
                          help='custom spec file to use (optional)')
    parser_j.add_argument('--part', '-p', type=str, action="store", nargs='?',
                          help='spec path for the expected part (e.g. "USDAT/PROFIL", requires a known structure)')
    parser_j.set_defaults(func=mount_fuse)

    parser_b = subparsers.add_parser('to-xml', help='convert a file to XML', aliases=['x'])
    # We can't take stdin here because we need the complete file size.
    # parser_b.add_argument('infile', type=str, help='input file to convert', default='-', nargs='?')
    parser_b.add_argument('infile', type=str, help='input file to convert')
    parser_b.add_argument('--structure', '-s', nargs=1, type=str, default=[''], action="store",
                          help='structure specification type to follow (optional)')
    parser_b.add_argument('--custom-structure', '-S', nargs=1, type=str, default=[''], action="store",
                          help='custom spec file to use (optional)')
    parser_b.add_argument('--output', '-o', type=str, action="store", nargs=1,
                          help='output file (optional)', default=['-'])
    # parser_b.add_argument('--no-data', '-N', action='store_true', help='do not include raw data in output')
    # parser_b.add_argument('--pointers', '-P', action='store_true', help='include pointers to original sources')
    # parser_b.add_argument('--exclude', '-x', type=str, action="store", nargs='*',
    #                       help='exclude raw data of this list of headers')
    parser_b.add_argument('--part', '-p', type=str, action="store", nargs='?',
                          help='spec path for the expected part (e.g. "USDAT/PROFIL", requires a known structure)')
    parser_b.add_argument('--force', '-f', action='store_true', help='overwrite existing files')
    parser_b.set_defaults(func=to_xml)

    parser_i = subparsers.add_parser('to-bin', help='convert a XML extract to binary', aliases=['b'])
    parser_i.add_argument('infile', type=str, help='input file to convert')
    parser_i.add_argument('outfile', type=str, help='output file name')
    parser_i.add_argument('--force', '-f', action='store_true', help='overwrite existing files')
    parser_i.set_defaults(func=to_bin)

    parser_k = subparsers.add_parser('mod', help='apply a mod')
    parser_k.add_argument('modfile', type=str, help='XML modfile to apply')
    parser_k.add_argument('--save-resolved', '-r', type=str, help='save resolved modfile to this location')
    parser_k.add_argument('--save-compiled', '-c', type=str, help='save compiled modfile to this location')
    parser_k.add_argument('--save-separately', '-s', action='store_true', help='save modded files under a new name')
    parser_k.add_argument('--save-extracted', '-x', action='store_true', help='save files as XML instead of binary')
    parser_k.add_argument('--no-backup', action='store_true', help='do not create backups of existing files when overwriting')
    parser_k.add_argument('--no-apply', '-n', action='store_true', help='do not apply the mod (useful in combination with --save-resolved or --save-compiled)')
    parser_k.add_argument('--dry-run', '-d', action='store_true', help="simulate applying the mod but don't save any output")
    parser_k.add_argument('--file', '-i', type=str, action="store", nargs='*',
                          help='specify files as "id:file" that would be prompted otherwise')
    parser_k.add_argument('--var', '-v', type=str, action="store", nargs='*',
                          help='specify variables as "id:value" that would be prompted otherwise')
    parser_k.add_argument('--force', '-f', action='store_true', help='overwrite existing files')
    parser_k.set_defaults(func=apply_mod)

    parser_n = subparsers.add_parser('mod-validate', help='validate a mod', aliases=['mv'])
    parser_n.add_argument('moddir', type=str, nargs='+', help='base dir of the mod, containing main.xml')
    group = parser_n.add_mutually_exclusive_group()
    group.add_argument('--rnc', '-r', action='store_true', help='use the RelaxNG parser for validation '
                       + '(catches all mistakes but has very unhelpful error messages)')
    group.add_argument('--xsd', '-x', action='store_true', help='use the XSD parser for validation '
                       + '(better error messages but may not catch all errors)')
    parser_n.set_defaults(func=validate_mod)

    parser_m = subparsers.add_parser('mod-resolve', help='resolve a mod', aliases=['mr'])
    parser_m.add_argument('moddir', type=str, nargs='+', help='base dir of the mod, containing main.xml; '
                          + 'the resolved mod will be called "<moddir>.resolved.xml"')
    parser_m.add_argument('--force', '-f', action='store_true', help='overwrite existing files')
    parser_m.set_defaults(func=resolve_mod)

    parser_l = subparsers.add_parser('mod-compile', help='compile a mod', aliases=['mc'])
    parser_l.add_argument('moddir', type=str, nargs='+', help='base dir of the mod, containing main.xml; the compiled mod will be called "<moddir>.a2z"')
    parser_l.add_argument('--no-compress', '-C', action='store_true', help='do not compress to output file')
    parser_l.add_argument('--force', '-f', action='store_true', help='overwrite existing files')
    parser_l.set_defaults(func=compile_mod)

    parser_g = subparsers.add_parser('dump-spec', help='Dump fully resolved spec.', aliases=['p'])
    parser_g.add_argument('structure', type=str, help='structure specification type to follow')
    parser_g.add_argument('--part', '-p', type=str, action="store", nargs='?',
                          help='spec path for the expected part (e.g. "USDAT/PROFIL", requires a known structure)')
    parser_g.set_defaults(func=dump_spec)

    parser_g = subparsers.add_parser('build-docs', help='Build file format documentation.', aliases=['d'])
    parser_g.set_defaults(func=build_docs)

    args = parser.parse_args()

    if not args.logfile:
        logfile = None
    else:
        logfile = args.logfile

    if args.debug:
        tools.setup_logging(log.DEBUG, logfile)
    elif args.quiet:
        tools.setup_logging(log.WARNING, logfile)
    else:
        tools.setup_logging(log.INFO, logfile)

    if 'func' in args:
        exitval = args.func(args)
    else:
        parser.print_help()
        exitval = 0

    sys.exit(exitval)
else:
    tools.setup_logging(log.INFO, None)
