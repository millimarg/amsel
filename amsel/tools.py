# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import sys
import os
import shutil
import logging as log
import random
import string
import gzip
import enum
from pathlib import Path

from typing import Any
from typing import Iterable
from typing import Tuple
from typing import NoReturn
from typing import TypeVar
from typing import Dict
from typing import List

from xdg.BaseDirectory import save_data_path as xdg_data_path
from bs4 import BeautifulSoup
from bs4.formatter import XMLFormatter
import png

# for templates
from jinja2.sandbox import SandboxedEnvironment
from jinja2 import FileSystemLoader


T = TypeVar("T")
LOGFILE = None
DEBUG = log.DEBUG


# ------------------------------------------------------------------------------
# Logging and Exceptions
# ------------------------------------------------------------------------------

def setup_logging(level: int = log.INFO, logfile: str = LOGFILE) -> NoReturn:
    if logfile:
        global LOGFILE
        LOGFILE = logfile
        handlers = [
            log.FileHandler(logfile),
            log.StreamHandler()
        ]
    else:
        handlers = [log.StreamHandler()]

    log.basicConfig(
        format='[%(asctime)s] %(levelname)-8.8s - %(funcName)s @ %(lineno)4d: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S', handlers=handlers,
        level=level,
        force=True
    )


def log_fatal(exception, message: str) -> NoReturn:
    # helper function for logging fatal errors
    log.error(f'fatal: {message}')
    raise exception(message)


# def format_bytes(size):
#     # adapted from https://stackoverflow.com/a/49361727
#     power = 2**10  # 2**10 = 1024
#     n = 0
#     power_labels = {0: '', 1: 'Ki', 2: 'Mi', 3: 'Gi', 4: 'Ti'}
#     while size > power:
#         size /= power
#         n += 1
#     return f"{size:.2f} {power_labels[n]+'B'}"


# ------------------------------------------------------------------------------
# Input/Output
# ------------------------------------------------------------------------------

class _DataPaths:
    class Kind(enum.Enum):
        Xdg = enum.auto()
        File = enum.auto()
        Dir = enum.auto()

    def __init__(self, baseName: str):
        if not baseName:
            raise ValueError('bug: no data base directory defined!')

        self._baseName = baseName.rstrip('/') + '/'
        self._moduleDir = Path(__file__).parent.resolve()
        self._workingDir = Path().resolve()

    def _search(self, name: str, paths: List[Tuple[str, Kind]]):
        for key, kind in paths:
            if kind == self.Kind.Xdg:
                path = Path(xdg_data_path(key)).absolute()
                log.debug(f'found {name} in {path}')
                return path

            path = Path(key).absolute()

            if not path.exists():
                continue
            elif kind == self.Kind.File:
                if path.is_file():
                    log.debug(f'found {name} in {path}')
                    return path
                else:
                    msg = f'expected "{path}" to be a file'
                    raise FileNotFoundError(msg)
            elif kind == self.Kind.Dir:
                if path.is_dir():
                    log.debug(f'found {name} in {path}')
                    return path
                else:
                    msg = f'expected "{path}" to be a directory'
                    raise FileNotFoundError(msg)

        log.debug(f'could not find {name} anywhere, tried {paths}')
        msg = f'could not find data in any searched path: {name} | {paths}'
        raise FileNotFoundError(msg)

    @property
    def spec(self) -> Path:
        return self._search('spec file directory', [
            (self._workingDir / 'spec', self.Kind.Dir),
            (self._moduleDir / '../spec', self.Kind.Dir),
            (self._baseName + 'spec', self.Kind.Xdg),
        ])

    @property
    def mod_templates(self) -> Path:
        return self._search('standard library for modding', [
            (self._workingDir / 'mod-templates', self.Kind.Dir),
            (self._moduleDir / 'mod-templates', self.Kind.Dir),
            (self._baseName + 'mod-templates', self.Kind.Xdg),
        ])


DataPaths = _DataPaths('amsel')


def save_any(path_or_stream, data, openmode='w', force=False):
    if path_or_stream == '-':
        path_or_stream = sys.stdout

    if callable(getattr(path_or_stream, 'write', None)):
        path_or_stream.write(data)
    elif isinstance(path_or_stream, (str, os.PathLike)):
        if Path(path_or_stream).exists() and not force:
            log_fatal(FileExistsError, f"output file '{path_or_stream}' already exists")

        with open(path_or_stream, openmode) as fd:
            fd.write(data)
    else:
        log_fatal(ValueError, f"invalid output file or stream '{path_or_stream}'")


def save_xml(path_or_stream, xml, force=False, pretty=True, compress=False):
    if pretty:
        formatter = XMLFormatter(indent='\t', void_element_close_prefix=' /')
        string = xml.prettify(formatter=formatter)
    else:
        string = str(xml)

    if compress:
        if path_or_stream == '-':
            path_or_stream = sys.stdout

        name = Path(path_or_stream)
        name = name.with_suffix(f'{name.suffix}.gz')

        if name.exists() and not force:
            log_fatal(FileExistsError, f"output file '{name}' already exists")

        with gzip.GzipFile(filename=name, mode='wb', mtime=0) as fd:
            fd.write(bytes(string + '\n', 'utf8'))
    else:
        save_any(path_or_stream, string + "\n", openmode='w', force=force)


def save_bin(path_or_stream, data, force=False):
    save_any(path_or_stream, data, openmode='wb', force=force)


def save_matrix_png(matrix, dataKind: str, outfile, force=False, r=None, g=None, b=None, compact=False):
    if not matrix:
        log_fatal(ValueError, "cannot save empty matrix as png")

    # if r and g and b and hasattr(matrix[0][0], r):
    #     kind = 'attr'
    # elif type(matrix[0][0]) not in [tuple, dict, list, bytearray] and not compact:
    #     kind = 'single'
    # else:
    #     kind = 'dict'

    if dataKind != 'single':
        r = r if r else g if g else b if b else 0
        g = g if g else r if r else b if b else 0
        b = b if b else r if r else g if g else 0

    image = []

    for row in matrix:
        if dataKind == 'single':
            image.append(flatten([[x % 256, x % 256, x % 256] if x is not None else [0, 0, 0] for x in row]))
        elif dataKind == 'dict':
            image.append(flatten([[x[r] % 256, x[g] % 256, x[b] % 256] if x is not None else [0, 0, 0] for x in row]))
        else:
            image.append(flatten([[getattr(x, r, 0) % 256, getattr(x, g, 0) % 256, getattr(x, b, 0) % 256] if x is not None else [0, 0, 0] for x in row]))

    w = png.Writer(len(matrix[0]), len(matrix), greyscale=False)

    if outfile == '-':
        with os.fdopen(sys.stdout.fileno(), 'wb', closefd=False) as fd:
            w.write(fd, image)
            fd.flush()
    else:
        if Path(outfile).exists() and not force:
            log_fatal(FileExistsError, f"output file '{outfile}' already exists")

        with open(outfile, 'wb') as fd:
            w.write(fd, image)


# class InputStreamHelper:
#     def __init__(self, input, options):
#         self.asFile = (input != '-')
#         self.input = (input if self.asFile else sys.stdin)
#         self.name = (input if self.asFile else 'stdin')
#         self.options = (options if options else 'rb')
#         self.stream = None  # use this to access the actual stream
#
#     def __enter__(self, *args):
#         if self.asFile:
#             self.stream = open(self.input, self.options)
#         else:
#             self.stream = self.input
#         return self
#
#     def __exit__(self, *args):
#         if self.asFile:
#             self.stream.close()


def request_file_name(prompt: str, default: str = None) -> str:
    """ Default callback for ModApplier. """
    import readline  # for enhanced editing in input()
    assert readline.__name__  # silence pyflakes

    if default is not None:
        prompt = f'Select a file: {prompt} [default: {default}] --> '
    else:
        prompt = f'Select a file: {prompt} --> '

    while True:
        if filename := input(prompt):
            break

        if default:
            filename = default
            break

    return str(filename).strip()


def request_value(prompt: str, default: str = None, options: List[Tuple[str, str]] = None) -> Any:
    """ Default callback for ModApplier. """
    import readline  # for enhanced editing in input()
    assert readline.__name__  # silence pyflakes

    if options:
        request_prompt = prompt
        prompt = '\n'.join([f'{i+1}. {vk[1]}' if vk[0] == vk[1] else f'{i+1}. {vk[1]} ({vk[0]})'
                            for i, vk in enumerate(options)])

        if default is not None:
            prompt += f'\nChoose a value: {request_prompt} [default: {default}] --> '
        else:
            prompt += f'\nChoose a value: {request_prompt} --> '

        while True:
            if choice := input(prompt):
                try:
                    if int(choice) <= len(options) and int(choice) > 0:
                        value = options[int(choice) - 1][0]
                        break
                except ValueError:
                    continue

            if default:
                value = default
                break
    else:
        if default is not None:
            prompt = f'Define a value: {prompt} [default: {default}] --> '
        else:
            prompt = f'Define a value: {prompt} --> '

        while True:
            if value := input(prompt):
                break

            if default:
                value = default
                break

    return value


def backup_existing_file(infile, suffix: str = None):
    infile = Path(infile)
    suffix = f'{suffix}-' if suffix else ''

    n = 1
    backup = infile

    while backup.exists():
        n += 1
        backup = infile.with_name(f'{infile.name}~{suffix}{n}~')

    if infile.exists():
        infile.rename(backup)


# ------------------------------------------------------------------------------
# Data Wrangling
# ------------------------------------------------------------------------------

# class CustomSandboxedEnvironment(ImmutableSandboxedEnvironment):
class CustomSandboxedEnvironment(SandboxedEnvironment):
    # TODO: https://stackoverflow.com/questions/13355996/how-can-i-prevent-circular-include-calls-in-jinja2-templates

    intercepted_binops = frozenset(["**", "//"])

    def call_binop(self, context, operator, left, right):
        if operator in self.intercepted_binops:
            return self.undefined(f"The operator '{operator}' is unavailable.")

        return super().call_binop(self, context, operator, left, right)


def resolved_xml_templ(sourcepath, extra_search_paths=[], include_cwd=False,
                       custom_filters={}, custom_globals={}) -> BeautifulSoup:
    extra_search_paths = [Path(sourcepath).resolve().parent] + extra_search_paths
    env = _get_jinja_env(extra_search_paths, include_cwd, custom_filters, custom_globals)

    try:
        with gzip.open(sourcepath, 'rb') as fd:
            return BeautifulSoup(env.from_string(fd.read().decode('utf8')).render(), 'xml')
    except gzip.BadGzipFile:
        return BeautifulSoup(env.get_template(Path(sourcepath).name).render(), 'xml')


def resolved_xml_string(string: str, extra_search_paths=[], include_cwd=False,
                        custom_filters={}, custom_globals={}) -> BeautifulSoup:
    env = _get_jinja_env(extra_search_paths, include_cwd, custom_filters, custom_globals)
    return BeautifulSoup(env.from_string(string).render(), 'xml')


def _get_jinja_env(extra_search_paths=[], include_cwd=False,
                   custom_filters={}, custom_globals={}) -> CustomSandboxedEnvironment:
    search_paths = extra_search_paths

    if include_cwd:
        search_paths = [Path('.')] + search_paths

    env = CustomSandboxedEnvironment(loader=FileSystemLoader(search_paths),
                                     trim_blocks=False,
                                     extensions=['jinja2_highlight.HighlightExtension',
                                                 'jinja2.ext.do', 'jinja2.ext.loopcontrols'])

    for k, v in custom_filters.items():
        # ex. [name] = lambda x: str(x)
        env.filters[k] = v

    for k, v in custom_globals.items():
        env.globals[k] = v

    return env


def is_int(value: [str, int, float]):
    """ Jinja filter: check if a value can be converted to int. """

    try:
        drop = int(value)
        del drop
    except Exception:
        return False

    return True


def is_list(value: Any) -> bool:
    """ Jinja filter: check if a value is iterable. """
    if isinstance(value, str):
        return False

    try:
        iter(value)
        return True
    except TypeError:
        return False


def random_str(length):
    """ Jinja function: get a random string with given length. """

    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


class _FigurenFilesCategory(enum.Flag):
    Header = enum.auto()
    DataClose = enum.auto()
    DataMedium = enum.auto()
    DataFar = enum.auto()
    DotAddon = enum.auto()
    DotClassic = enum.auto()
    Combined = enum.auto()

    SeparateData = DataClose | DataMedium | DataFar
    HeaderData = Header | SeparateData
    All = Header | SeparateData | DotAddon | DotClassic | Combined


class _FigurenFiles:
    def __init__(self, base: Path):
        self.base = base
        self.backup_dir = self.base / 'figuren_backup'

        self.expected_files: Dict[_FigurenFilesCategory, List[Path]] = {
            _FigurenFilesCategory.Header: [self.base / 'FigurenHeader.dat'],
            _FigurenFilesCategory.DotAddon: [self.base / 'Figuren.addon'],
            _FigurenFilesCategory.DotClassic: [self.base / 'Figuren.classic'],
            _FigurenFilesCategory.Combined: [self.base / 'Figuren.dat'],
        }

        for category, key, count in [(_FigurenFilesCategory.DataClose, '0', 10),
                                     (_FigurenFilesCategory.DataMedium, '1', 4),
                                     (_FigurenFilesCategory.DataFar, '2', 1)]:
            globbed = sorted(list(self.base.glob(f'Figuren{key}??.dat')))
            expected = [self.base / f'Figuren{key}{x:02d}.dat' for x in range(0, count)]

            if globbed and len(globbed) > count:
                self.expected_files[category] = globbed
            else:
                self.expected_files[category] = expected

    def make_backup(self, category: _FigurenFilesCategory = _FigurenFilesCategory.All):
        backup_existing_file(self.backup_dir)
        self.backup_dir.mkdir(parents=True)

        for file in self.get_existing(category):
            log.info(f'creating backup of {file}...')
            shutil.copyfile(file, self.backup_dir / file.name, follow_symlinks=True)

        log.info(f'original figuren files backed up: {self.backup_dir}')

    def get_files(self, category: _FigurenFilesCategory):
        for file in flatten([self.expected_files[x] for x in category]):
            if file.is_file():
                yield (file, True)
            else:
                yield (file, False)

    def get_first(self, category: _FigurenFilesCategory, must_exist: bool = False) -> Path:
        for file, ok in self.get_files(category):
            if ok or not must_exist:
                return file

        return None

    def get_existing(self, category: _FigurenFilesCategory) -> List[Path]:
        return [file for file, ok in self.get_files(category) if ok]

    def get_missing(self, category: _FigurenFilesCategory) -> List[Path]:
        return [file for file, ok in self.get_files(category) if not ok]

    def exist_all(self, category: _FigurenFilesCategory) -> bool:
        return all([ok for file, ok in self.get_files(category)])

    def exist_none(self, category: _FigurenFilesCategory) -> bool:
        return not any([ok for file, ok in self.get_files(category)])


def autoselect_figuren_data_files() -> Path:
    files = _FigurenFiles(Path('.'))
    selected = None

    # FIXME it seems like the History Edition is still using the same split
    #       Figuren files as the Königsedition. It crashes if any of the extra
    #       files is missing, though. Is this correct? Is DotAddon or HeaderData
    #       actually used?
    # for i in [_FigurenFilesCategory.DotAddon,   # History Edition
    #           _FigurenFilesCategory.Combined]:  # Demo version

    if files.exist_all(_FigurenFilesCategory.DotAddon) and \
            files.exist_all(_FigurenFilesCategory.Combined):
        selected = combine_figuren_data_files()

    if not selected:
        for i in [_FigurenFilesCategory.Combined]:  # Demo version
            if files.exist_all(i):
                files.make_backup(_FigurenFilesCategory.All)
                selected = files.get_first(i)
                break

    if not selected and not files.get_missing(_FigurenFilesCategory.HeaderData):
        # Königsedition, GOG version, Addon, Classic version
        selected = combine_figuren_data_files()

    if selected:
        log.info(f'selected figuren data file: {str(selected)}')
    else:
        log_fatal(FileNotFoundError, "all figuren data files are missing")

    return selected


def combine_figuren_data_files() -> Path:
    files = _FigurenFiles(Path('.'))

    if files.exist_all(_FigurenFilesCategory.Combined) and \
            files.exist_none(_FigurenFilesCategory.HeaderData):
        log.info("figuren data is already prepared")
        return

    if not files.exist_all(_FigurenFilesCategory.HeaderData):
        missing = [str(x) for x in files.get_missing(_FigurenFilesCategory.HeaderData)]
        log_fatal(FileNotFoundError, f'figuren data files were missing: {", ".join(missing)}')

    head = files.get_first(_FigurenFilesCategory.Header)

    log.info(f'reading {head}...')
    with open(head, 'rb') as fd:
        head_raw = fd.read()

    required_blocks = head_raw[-72:]
    expected_block = bytearray('TEXTURES\x00\x00\x00\x00', encoding='utf8')
    combined_output = head_raw[0:-72]

    for pos in (0, 24, 48):
        if required_blocks[pos:pos + 12] != expected_block:
            log.error(f'expected tag TEXTURES not found at -72+{pos} of {head}: {required_blocks}')
            log_fatal(ValueError, f'invalid input file: {head}')

    data_close = files.get_existing(_FigurenFilesCategory.DataClose)
    data_medium = files.get_existing(_FigurenFilesCategory.DataMedium)
    data_far = files.get_existing(_FigurenFilesCategory.DataFar)

    for gi, group in enumerate((data_close, data_medium, data_far)):
        sizes = [os.path.getsize(x) for x in group]
        loaded = bytearray(sum(sizes) + 24)
        loaded[0:24] = required_blocks[(24 * gi):(24 * gi + 24)]

        for i, fs in enumerate(zip(group, sizes)):
            file, size = fs
            log.info(f'reading {file} of zoom level {gi}...')
            with open(file, 'rb') as fd:
                end_offset = 24 + sum(sizes[0:i + 1])
                loaded[end_offset - size:end_offset] = fd.read()

        combined_output += loaded

    files.make_backup(_FigurenFilesCategory.All)
    combined = files.get_first(_FigurenFilesCategory.Combined)

    if combined.exists():
        log.warning(f'an existing combined Figuren data file was ignored ({combined.absolute()})')
        combined.unlink()  # backup_figuren_files() has already created a backup of this file

    log.info(f'writing {combined}...')
    with open(combined, 'wb') as fd:
        fd.write(combined_output)

    for file in files.get_existing(_FigurenFilesCategory.HeaderData):
        file.unlink()  # backup_figuren_files() has already created backups

    return combined


def grouped(iterable: Iterable[T], n=2) -> Iterable[Tuple[T, ...]]:
    """s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), ..."""
    # https://stackoverflow.com/a/5389547
    return zip(*[iter(iterable)] * n)


def chunks(lst, n, maxLength=None):
    """Yield successive n-sized chunks from lst.
       source: https://stackoverflow.com/a/312464 (Ned Batchelder, CC-BY-SA 4.0)

       The last chunk may be shorter than n.
    """
    if maxLength is None:
        for i in range(0, len(lst), n):
            yield lst[i:i + n]
    else:
        for i in range(0, maxLength, n):
            yield lst[i:i + n]


def flatten(lst):
    return [item for sublist in lst for item in sublist]


def combine_matrix(parts: List[Dict[str, Any]], compact: bool = False) -> List[List[Any]]:
    """
    :parts: list of dicts: {x, y, w, h, rows/fields}
    :rows: (in :parts:) list of lists: any data
    :compact: requires :rows: to be a list of lists of dicts with at least {rel_x, rel_y}
    """

    max_w = max([x['w'] for x in parts])
    max_h = max([x['h'] for x in parts])
    max_x = max([x['x'] for x in parts]) + max_w
    max_y = max([x['y'] for x in parts]) + max_h

    matrix = [[None for x in range(max_x)] for y in range(max_y)]

    compact_dict = False
    if compact and isinstance(parts[0]['fields'], dict):
        compact_dict = True

    for part in parts:
        if compact:
            for field in part['fields']:
                # assign field to position
                if compact_dict:
                    matrix[part['y'] + field['rel_y']][part['x'] + field['rel_x']] = field
                else:
                    matrix[part['y'] + getattr(field, 'rel_y')][part['x'] + getattr(field, 'rel_x')] = field
        else:
            for i, row in enumerate(part['rows']):
                # assign row to range
                matrix[part['y'] + i][part['x']:part['x'] + part['w']] = row

    return matrix
