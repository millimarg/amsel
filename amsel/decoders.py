# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import struct
import logging as log
import math
from dataclasses import dataclass
from functools import cache
from collections import namedtuple

from typing import Dict
from typing import List
from typing import Any

from . import parser
from .spec import SpecNode

from .tools import grouped
from .tools import flatten
from .tools import log_fatal
from .tools import chunks


# NOTE See Decoder's docstring.

DEFAULT_CODED_SPLIT_FIELDS: str = '\\x9C'
DEFAULT_CODED_SPLIT_ITEMS: str = '\\x91'
DEFAULT_CODED_SPLIT_LISTS: str = '\\x92'
ENCODING: str = 'windows-1252'  # as used in the original game files
_DECODERS: Dict[str, 'DecBase'] = {}  # name: Class
_LOADED_RAW: List[str] = []  # formats
_REPORTED_MISSING_TRANSCODERS: Dict[str, bool] = {}  # name: True

# def __init....|, ([^,]+)
#               |       self.\1 = \1\n
# def __str.....| = .*$
#               |self.\1 = \1


# ------------------------------------------------------------------------------
# Convenience functions
# ------------------------------------------------------------------------------

@cache
def load_decoders() -> dict[str, 'DecBase']:
    if _DECODERS:
        return _DECODERS

    for k, v in globals().items():
        if type(v) is not type(DecBase) or not issubclass(v, DecBase):
            continue

        xmlname = getattr(v, 'xmlname', None)

        if not xmlname:
            log_fatal(NotImplementedError, f'**bug** decoder {k} has no xml name')
        elif xmlname == '__none__':
            continue

        _DECODERS[xmlname] = v

    return _DECODERS


@cache
def get_transcoder(typeStr: str, raiseIfMissing: bool = False) -> 'DecBase':
    load_decoders()

    if typeStr not in _DECODERS and typeStr not in _LOADED_RAW:
        if raiseIfMissing:
            raise Exception(f"no transcoder for '{typeStr}' found")

        if typeStr not in _REPORTED_MISSING_TRANSCODERS:
            log.warning(f'no transcoder for "{typeStr}" found (only reported once)')
            _REPORTED_MISSING_TRANSCODERS[typeStr] = True
            _LOADED_RAW.append(typeStr)

    if typeStr in _LOADED_RAW:
        return _DECODERS.get('raw')

    return _DECODERS.get(typeStr)


def bin2py(data: memoryview, specNode: SpecNode, subtree: 'parser.BlockNode' = None, **kwargs) -> Any:
    return get_transcoder(specNode.typeStr, raiseIfMissing=False).bin2py(data, specNode, subtree=subtree, **kwargs)


def py2bin(data: Any, specNode: SpecNode, subtree: 'parser.BlockNode' = None, **kwargs) -> memoryview:
    return get_transcoder(specNode.typeStr, raiseIfMissing=True).py2bin(data, specNode, subtree=subtree, **kwargs)


def xml2py(data: str, specNode: SpecNode, subtree: 'parser.BlockNode' = None, **kwargs) -> Any:
    # TODO only apply mods here and not separately in every xml2py implementation?
    return get_transcoder(specNode.typeStr, raiseIfMissing=True).xml2py(data, specNode, subtree=subtree, **kwargs)


def py2xml(data: Any, specNode: SpecNode, subtree: 'parser.BlockNode' = None, **kwargs) -> str:
    return get_transcoder(specNode.typeStr, raiseIfMissing=True).py2xml(data, specNode, subtree=subtree, **kwargs)


# ------------------------------------------------------------------------------
# Exceptions
# ------------------------------------------------------------------------------

class TranscoderError(Exception):
    name = ''

    def __init__(self, message: [str, Exception], specNode: SpecNode):
        if isinstance(message, Exception):
            message = f'[{type(message).__name__}] {message}'

        self.message = message
        self.specNode = specNode

    def __str__(self):
        return f'{self.name+(" " if self.name else "")}recoding failed: {self.message} ' + \
               f'| node: {self.specNode}'


class BinPyError(TranscoderError):
    name = 'bin->py'


class PyBinError(TranscoderError):
    name = 'py->bin'


class XmlPyError(TranscoderError):
    name = 'xml->py'


class PyXmlError(TranscoderError):
    name = 'py->xml'


# ------------------------------------------------------------------------------
# Decoder classes
# ------------------------------------------------------------------------------

String = namedtuple('String', 'string length')


class DecBase():
    """Base class for decoders.

    Docstrings are used for internals, i.e. how to use the functions/classes,
    whereas comments above the class definition are used for describing the data
    format. These comments will be included in the spec documentation. Indented
    paragraphs (>= 2 spaces) will be rendered pre-formatted. Separate paragraphs
    by an empty line. The first line must be a short, one-line summary. Some
    markup is allowed.

    Derived classes must define bin2py, py2bin, xml2py, py2xml, and xmlname.
    """
    xmlname = '__none__'  # data type to be used in spec files

    # very brief description of the XML format
    # prefix: which separators are used (f: fields, i: items, l: lists)
    # use 'x' if the separator is not used: e.g. fxx = only fields
    xmldoc = 'fil:'
    xmlex = ''  # very brief example of the XML format

    CODED_SPLIT_FIELDS = DEFAULT_CODED_SPLIT_FIELDS
    CODED_SPLIT_ITEMS = DEFAULT_CODED_SPLIT_ITEMS
    CODED_SPLIT_LISTS = DEFAULT_CODED_SPLIT_LISTS

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> Any:
        """ Convert binary data to usable Python objects.
            Return the object.
        """
        log_fatal(NotImplementedError, '**bug** bin2py is not implemented for this type')

    @classmethod
    def py2bin(cls, data: Any, specNode: SpecNode, **kwargs) -> memoryview:
        """ Convert Python data to packed binary.
            Return binary data.
        """
        log_fatal(NotImplementedError, '**bug** py2bin is not implemented for this type')

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> Any:
        """ Convert XML data (value attribute) to usable Python objects.
            Return the object.
        """
        log_fatal(NotImplementedError, '**bug** xml2py is not implemented for this type')

    @classmethod
    def py2xml(cls, data: Any, specNode: SpecNode, **kwargs) -> str:
        """ Convert Python data to a string representation.
            Return the string.
        """
        log_fatal(NotImplementedError, '**bug** py2xml is not implemented for this type')

    @classmethod
    def _save_xml(cls, data: Any, stringifyCallback=str) -> str:
        if data is None:
            return ''

        pyType = type(data)

        if pyType is list and len(data) > 0 and type(data[0]) is list:
            string = cls.CODED_SPLIT_LISTS.join([
                cls.CODED_SPLIT_ITEMS.join(map(stringifyCallback, sublist)) for sublist in data
            ])
        elif pyType is list:
            string = cls.CODED_SPLIT_ITEMS.join(map(stringifyCallback, data))
        else:
            string = stringifyCallback(data)

        return string

    # @classmethod
    # def __help_xml2py_impl(cls, data: str, specNode: SpecNode, splitterCallback, converterCallback):
    #     # if data is None or (int(node.attrs.get('length', 0xFF)) > 0 and not xml
    #     #                    and not node.specNode.typeStr == 'cstring'):
    #     #     log_fatal(ValueError, f"no XML data in node {node.ext} (got: {xml})")
    #     return splitterCallback(data, converterCallback)

    @classmethod
    def _help_xml2py_single(cls, data: str, callback: callable) -> Any:
        items = data.split(cls.CODED_SPLIT_FIELDS)
        return callback(*items)

    @classmethod
    def _help_xml2py_list(cls, data: str, callback: callable) -> List[Any]:
        items = data.split(cls.CODED_SPLIT_ITEMS)
        converted = [callback(*x.split(cls.CODED_SPLIT_FIELDS)) for x in items]
        return converted

    @classmethod
    def _help_xml2py_list_of_lists(cls, data: str, callback: callable) -> List[List[Any]]:
        lines = data.split(cls.CODED_SPLIT_LISTS)
        converted = [[callback(*y.split(cls.CODED_SPLIT_FIELDS)) for y in x.split(cls.CODED_SPLIT_ITEMS)] for x in lines]
        return converted

    @classmethod
    def __mod_handle_increase_from(cls, mod, tree, node, data: str, extraData: Dict[str, Any]) -> str:
        ident = mod.getReferencedNode(tree, extraData['increase-from'])

        if not ident:
            raise XmlPyError(f"cannot calculate new value because '{extraData['increase-from']}' was empty - is it parsed?", node)

        if not hasattr(ident, 'decodeToPy'):
            raise XmlPyError(f"cannot calculate new value from non-data node '{ident}' at '{extraData['increase-from']}'", node)

        if ident.specNode.typeStr != 'uint':
            raise XmlPyError(f"cannot calculate new value from non-integer node '{ident}' at '{extraData['increase-from']}' -- {ident.specNode.typeStr}", node)

        return str(ident.decodeToPy() + 1)

    @classmethod
    def __mod_handle_store_offset(cls, mod, tree, node, data: str, extraData: Dict[str, Any]) -> str:
        mod.storeOffset(extraData['store-offset'], node.getNewLocalStartOffset() + int(extraData.get('offset-correction', 0)))
        return data

    @classmethod
    def __mod_handle_use_offsets(cls, mod, tree, node, data: str, extraData: Dict[str, Any]) -> str:
        for i in extraData['use-offsets'].split(' '):
            try:
                offset = mod.getStoredOffset(i) + int(extraData.get('offset-correction', 0))
            except Exception:
                raise XmlPyError(f"cannot use offset '{i}' which is not stored; check for use before definition", node)

            data = data.replace(f'@{i}@', str(offset))
            # print(f"==> @{i}@ {offset}")

        return data

    @classmethod
    def __mod_handle_store_value(cls, mod, tree, node, data: str, extraData: Dict[str, Any]) -> str:
        mod.storeValue(extraData['store-value'], data)
        return data

    @classmethod
    def __mod_handle_use_values(cls, mod, tree, node, data: str, extraData: Dict[str, Any]) -> str:
        for i in extraData['use-values'].strip().split(' '):
            try:
                value = mod.getStoredValue(i)
            except Exception:
                raise XmlPyError(f"cannot use value '{i}' which is not stored; check for use before definition", node)

            data = data.replace(f'@{i}@', str(value))
            # print(f"==> @{i}@ {value} | {data}")

        return data

    @classmethod
    def _apply_mods(cls, data: str, extraData: Dict[str, Any]) -> str:
        try:
            mod = extraData['mod']
        except KeyError:
            return data

        try:
            tree = extraData['tree']
            node = extraData['node']
        except KeyError as e:
            raise XmlPyError(f"cannot apply mods to '{data}' because an error occurred: {e}", node)

        # Note: it is important that modifications are applied in this order
        # because later changes may depend on earlier changes.

        if 'increase-from' in extraData:
            data = cls.__mod_handle_increase_from(mod, tree, node, data, extraData)

        if 'store-offset' in extraData:
            data =  cls.__mod_handle_store_offset(mod, tree, node, data, extraData)

        if 'use-offsets' in extraData:
            data =   cls.__mod_handle_use_offsets(mod, tree, node, data, extraData)

        if 'store-value' in extraData:
            data =   cls.__mod_handle_store_value(mod, tree, node, data, extraData)

        if 'use-values' in extraData:
            data =    cls.__mod_handle_use_values(mod, tree, node, data, extraData)

        return data


class DecIntMixin:
    @classmethod
    def _b2i(cls, raw: bytes) -> int:
        return int.from_bytes(raw, byteorder='little', signed=False)

    @classmethod
    def _i2b(cls, num, width) -> bytes:
        return bytes(int(num).to_bytes(int(width), byteorder='little'))


class DecStringMixin:
    @classmethod
    def _read_cstring(cls, data: bytes, offset: int = 0, decode: bool = True) -> String:
        # read until we reach zero (not included in the result): https://stackoverflow.com/a/46610595
        string = bytes([i for i in iter(lambda x=iter(data[offset:]): next(x), 0)])
        length = len(string) + 1  # +1 to account for skipped trailing zero

        if decode:
            string = string.decode(ENCODING)

        return String._make((string, length, ))

    @classmethod
    def _read_pstring(cls, data: bytes, offset: int = 0, decode: bool = True) -> String:
        print("GOT HERE", data, type(data), offset)
        length = struct.unpack_from('<B', data, offset)[0] + 1  # +1 to account for the length byte
        string = struct.unpack_from(f'{length}p', data, offset)[0]

        if decode:
            string = string.decode(ENCODING)

        return String._make((string, length, ))

    @classmethod
    def _write_pstring(cls, data: str) -> memoryview:
        string = bytes(data, encoding=ENCODING)
        return memoryview(struct.pack(f'{len(string)}p', string))


# Raw data.
#
# Simply any data of any length. This format is used if the structure is not
# known or if no decoder is available yet.
class DecRaw(DecBase):
    xmlname = 'raw'
    xmldoc = 'xxx:hex-encoded bytes, whitespace is ignored'
    xmlex = 'aabbccdd eeff'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> memoryview:
        return memoryview(data)

    @classmethod
    def py2bin(cls, data: memoryview, specNode: SpecNode, **kwargs) -> memoryview:
        return memoryview(data)

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> memoryview:
        data = cls._apply_mods(data, kwargs)

        try:
            return bytes.fromhex(data)
        except Exception as e:
            raise XmlPyError(e, specNode)

    @classmethod
    def py2xml(cls, data: memoryview, specNode: SpecNode, **kwargs) -> str:
        try:
            return data.hex(sep=' ', bytes_per_sep=4)
        except Exception as e:
            raise PyXmlError(e, specNode)


# Zero-terminated string.
#
# A series of non-zero characters terminated by a single "zero" byte. The
# terminator is counted as part of the string's length.
class DecCString(DecBase, DecStringMixin):
    xmlname = 'cstring'
    xmldoc = 'xxx:a simple string'
    xmlex = 'Farbtabellen\\Maeher01.PCX'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> str:
        val = cls._read_cstring(data, 0)

        if val.length != len(data) and sum([x for x in data[val.length:]]) != 0:
            log.error(f"c-string does not cover all non-zero data: read={val.string} left={data[val.length:].hex(' ', 4)}")

        return val.string

    @classmethod
    def py2bin(cls, data: [str, String], specNode: SpecNode, **kwargs) -> memoryview:
        # at least insel5 files write empty c-strings as "\x00"
        # TODO check if this is the case in all files
        if hasattr(data, 'string'):
            data = data.string

        return memoryview(bytes(data + '\x00', encoding=ENCODING))

    @classmethod
    def xml2py(cls, data: [str, String], specNode: SpecNode, **kwargs) -> str:
        if hasattr(data, 'string'):
            data = data.string

        data = cls._apply_mods(data, kwargs)
        return data

    @classmethod
    def py2xml(cls, data: [str, String], specNode: SpecNode, **kwargs) -> str:
        if hasattr(data, 'string'):
            data = data.string

        return data


# Fixed-width string.
#
# A series of characters, expected to be printable.
class DecXString(DecBase, DecStringMixin):
    xmlname = 'xstring'
    xmldoc = 'xxx:a simple string'
    xmlex = 'Bau.wav'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> Any:
        # This string is not null-terminated.
        # We try to decode it in full and hope for the best.
        #
        # Note: specNode.length must not be math.inf, i.e. it must not be length=*
        return bytes(data[:]).decode(ENCODING)

    @classmethod
    def py2bin(cls, data: str, specNode: SpecNode, **kwargs) -> memoryview:
        # TODO how much verification should be done in transcoders?
        #      - verify input data type, i.e. str(...) or isinstance(data, str)
        #      - verify input length
        #      - verify output length
        #      - verify specnode length
        return memoryview(bytes(data, encoding=ENCODING))

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> Any:
        data = cls._apply_mods(data, kwargs)
        return data

    @classmethod
    def py2xml(cls, data: str, specNode: SpecNode, **kwargs) -> str:
        return data


# Length-prefixed string.
#
# A series of characters with their length prefixed. There can be an optional
# zero byte at the end.
class DecPString(DecBase, DecStringMixin):
    xmlname = 'pstring'
    xmldoc = 'xxx:a simple string'
    xmlex = 'TODO'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> String:
        # This string is not null-terminated.
        return cls._read_pstring(data, 0, True)

    @classmethod
    def py2bin(cls, data: str, specNode: SpecNode, **kwargs) -> memoryview:
        return cls._write_pstring(data)

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> str:
        data = cls._apply_mods(data, kwargs)
        return data

    @classmethod
    def py2xml(cls, data: str, specNode: SpecNode, **kwargs) -> str:
        return data


# Fixed-width, zero-padded string.
#
# A series of non-zero characters terminated by a single "zero" byte. The
# rest of the length is expected to be filled with "zero" bytes. In some
# files there are artifacts in the padding which are not part of the string.
#
#   zero = .
#   length = 24
#
#   string...artifacts......
#
# TODO: keep artifacts in all conversions; use String everywhere
class DecFixedCString(DecBase, DecStringMixin):
    xmlname = 'cstring_fixed'
    xmldoc = 'xxx:a simple string'
    xmlex = 'Maeher gehe leer\\leer01_001.tga'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> str:
        """For fixed-width zero-terminated strings. Only relevant for rebuilding files."""
        return DecCString.bin2py(data, specNode)

    @classmethod
    def py2bin(cls, data: str, specNode: SpecNode, **kwargs) -> memoryview:
        encoded = bytes(data, ENCODING)
        ret = bytearray(specNode.length) if specNode.length != math.inf else bytearray(len(data))
        ret[:len(encoded)] = encoded
        return memoryview(ret)

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> str:
        data = cls._apply_mods(data, kwargs)
        return data

    @classmethod
    def py2xml(cls, data: str, specNode: SpecNode, **kwargs) -> str:
        return data


# Series of zero-terminated strings.
#
# A list of series of non-zero characters terminated by a single "zero"
# byte. There is no special separator between entries. Cf. @f:cstring@.
#
#   zero = .
#
#   string.next string.
#
class DecCStringList(DecBase, DecStringMixin):
    xmlname = 'c_cstring_list'
    xmldoc = 'ixx:a list of simple strings'
    xmlex = 'UNUSED || UNUSED'

    CODED_SPLIT_ITEMS = ' || '

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[str]:
        read = 0
        items = []
        length = len(data)

        while read < length:
            val = cls._read_cstring(data, read)
            read += val.length
            items.append(val.string)

        return items

    @classmethod
    def py2bin(cls, data: List[str], specNode: SpecNode, **kwargs) -> memoryview:
        if data:
            data = '\x00'.join(data) + '\x00'
        else:
            # TODO check if empty string lists are '\x00' or ''
            data = ''

        return memoryview(bytes(data, encoding=ENCODING))

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[str]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list(data, lambda x: str(x))

    @classmethod
    def py2xml(cls, data: List[str], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data)


# Series of length-prefixed strings.
#
# A list of series of characters preceded by a byte defining the length of the
# following sequence. There is no special separator between entries. Cf. @f:pstring@.
#
#   AA, BB = length of following string
#
#   AAstringBBnext string
#    6string11next string
#
class DecPStringList(DecBase, DecStringMixin):
    xmlname = 'c_pstring_list'
    xmldoc = 'ixx:a list of simple strings'
    xmlex = 'UNUSED || UNUSED'

    CODED_SPLIT_ITEMS = ' || '

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[str]:
        read = 0
        items = []
        length = len(data)

        while read < length:
            val = cls._read_pstring(data, read, True)
            read += val.length
            items.append(val.string)

        return items

    @classmethod
    def py2bin(cls, data: List[str], specNode: SpecNode, **kwargs) -> memoryview:
        views = [cls._write_pstring(x) for x in data]
        lengths = [len(x) for x in views]
        ret = bytearray(sum(lengths))

        offset = 0
        for length, view in zip(lengths, views):
            ret[offset:offset + length] = view[:]
            offset += length

        return memoryview(ret)

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[str]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list(data, str)

    @classmethod
    def py2xml(cls, data: List[str], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data)


# Series of (id, name) pairs.
#
# A list of IDs with associated strings. The ID takes two bytes (uint),
# immediately followed by a zero-terminated string. There is no special
# separator between entries.
#
#   zero = .
#
#   NNstring.NNstring.
#
class DecIdCStringList(DecBase, DecStringMixin, DecIntMixin):
    xmlname = 'c_id_cstring_list'
    xmldoc = 'fix:a list of id-name pairs'
    xmlex = '10 GOLDERZ | 11 FISCH'

    CODED_SPLIT_FIELDS = ' '
    CODED_SPLIT_ITEMS = ' | '

    @dataclass
    class Data:
        uid: int
        string: str

        def __str__(self):
            return DecIdCStringList.CODED_SPLIT_FIELDS.join([str(self.uid), self.string])

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> Data:
        read = 0
        items = []

        while read < len(data):
            id = cls._b2i(data[read:read + 2])
            read += 2
            val = cls._read_cstring(data, read)
            read += val.length
            items.append(cls.Data(id, val.string))

        return items

    @classmethod
    def py2bin(cls, data: List[Data], specNode: SpecNode, **kwargs) -> memoryview:
        items = bytearray()

        for it in data:
            items += cls._i2b(it.uid, 2) + bytearray(str(it.string) + '\x00', encoding=ENCODING)

        return items

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[Data]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list(data, cls.Data)

    @classmethod
    def py2xml(cls, data: List[Data], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Series of (id, name) pairs.
#
# A list of IDs with associated strings. Name and ID are a zero-terminated
# string, with the ID separated by a comma character (0x2C). Apart from the
# string terminator there is no special separator between entries.
#
#   zero = .
#
#   name,ID.name,ID.
#
# Note: ID is a number in text form, variable width, not padded.
#
class DecCStringListCsv(DecBase, DecStringMixin):
    xmlname = 'c_cstring_list_csv'
    xmldoc = 'fix:a list of id-name pairs'
    xmlex = '10 GOLDERZ | 11 FISCH'

    CODED_SPLIT_FIELDS = ' '
    CODED_SPLIT_ITEMS = ' | '

    @dataclass
    class Data:
        uid: int
        string: str

        def __str__(self):
            return DecIdCStringList.CODED_SPLIT_FIELDS.join([str(self.uid), self.string])

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[Data]:
        read = 0
        items = []

        while read < len(data):
            val = cls._read_cstring(data, read)
            read += val.length
            split = val.string.split(',')
            items.append(cls.Data(uid=split[1], string=split[0]))

        return items

    @classmethod
    def py2bin(cls, data: List[Data], specNode: SpecNode, **kwargs) -> memoryview:
        items = '\x00'.join([f'{x.string},{x.uid}' for x in data])

        if items:
            items += '\x00'  # append a trailing zero as string terminator

        return bytearray(items, encoding=ENCODING)

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[Data]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list(data, cls.Data)

    @classmethod
    def py2xml(cls, data: List[Data], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Series of (b, g, r, a) color values.
#
# A list of color values without any separator. Each value takes one byte
# (uint). "A" appears to be always zero and might as well be interpreted as
# field separator. Some image formats used in the game provide their own
# alpha values.
#
#   zero = .
#
#   bgrabgrabgra
#       (or)
#   bgr.bgr.bgr.
#
# All color palettes used in the game have 256 (16x16) entries, i.e. 1024 bytes.
class DecColorList(DecBase, DecStringMixin):
    xmlname = 'c_color_list'
    xmldoc = 'fix:a list of color values (RGB+A)'
    xmlex = '#010101+00 | #aabbff+00'

    CODED_SPLIT_FIELDS = ' '
    CODED_SPLIT_ITEMS = ' | '

    @dataclass
    class Data:
        r: int
        g: int
        b: int
        a: int = 0

        # if a != 0:
        #     log.warning(f'color found where A != 0: {self} == {self.rgba_str}')

        def __eq__(self, other):
            # NOTE this doesn't compare "alpha" as it is probably not used in the game
            if isinstance(other, DecColorList.Data):
                if self.r == other.r and \
                        self.g == other.g and \
                        self.b == other.b:
                    # self.b == other.b and \
                    # self.a == other.a:
                    return True
            else:
                if self.r == int(other[0]) and \
                        self.g == int(other[1]) and \
                        self.b == int(other[2]):
                    # self.b == int(other[2]) and \
                    # self.a == int(other[3]):
                    return True
            return False

        def __ne__(self, other):
            return not self.__eq__(other)

        def __lt__(self, other):
            return self.num < other.num

        def from_str(self, string):
            self.r = int(string[1:3], 16)
            self.g = int(string[3:5], 16)
            self.b = int(string[5:7], 16)
            self.a = int(string[8:10], 16)
            return self

        def from_tup(self, tup, use_alpha=False):
            self.r = tup[0]
            self.g = tup[1]
            self.b = tup[2]

            if use_alpha:
                self.a = tup[3]
            else:
                self.a = 0

        @property
        def num(self):
            # return int(f'{self.r:02X}{self.g:02X}{self.b:02X}{self.a:02X}', 16)
            return int(f'{self.r:02X}{self.g:02X}{self.b:02X}', 16)

        @property
        def bgr_str(self):
            return f'{self.b} {self.g} {self.r}'

        @property
        def rgb_str(self):
            return f'{self.r} {self.g} {self.b}'

        @property
        def rgb_str_pretty(self):
            return f'{self.r:3d} {self.g:3d} {self.b:3d}'

        @property
        def rgba_str(self):
            return f'{self.r} {self.g} {self.b} {self.a}'

        @property
        def rgba_tup(self):
            return (self.r, self.g, self.b, self.a)

        @property
        def rgb_tup(self):
            return (self.r, self.g, self.b)

        def __str__(self):
            return f'#{self.r:02X}{self.g:02X}{self.b:02X}+{self.a:02}'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[Data]:
        items = [DecColorList.Data(r, g, b, a) for b, g, r, a in grouped(data, 4)]
        return items

    @classmethod
    def py2bin(cls, data: List[Data], specNode: SpecNode, **kwargs) -> memoryview:
        ret = bytearray(len(data) * 4)

        for i, field in enumerate(data):
            fdata = bytearray(4)
            fdata[0] = int(field.b)
            fdata[1] = int(field.g)
            fdata[2] = int(field.r)
            fdata[3] = int(field.a)
            ret[i * 4:(i * 4) + 4] = fdata

        return ret

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[Data]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list(data, lambda x: DecColorList.Data(0, 0, 0, 0).from_str(x))

    @classmethod
    def py2xml(cls, data: List[Data], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Unsigned integer values.
#
# Valid sizes are 1, 2, 3, 4, 6, or 8 bytes. Values are little endian encoded,
# i.e. lowest byte first.
class DecUint(DecBase):
    xmlname = 'uint'
    xmldoc = 'xxx:a single value'
    xmlex = '10'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> Any:
        """Unsigned integer values.

        Valid sizes are 1, 2, 3, 4, 6, or 8 bytes. Values are little endian encoded,
        i.e. lowest byte first.
        """
        valid_lengths = [1, 2, 3, 4, 6, 8]

        if len(data) not in valid_lengths:
            log_fatal(f"invalid data length for type 'uint', must be in {valid_lengths}")

        return int.from_bytes(data, byteorder='little', signed=False)

    @classmethod
    def py2bin(cls, data: int, specNode: SpecNode, **kwargs) -> memoryview:
        return bytearray(data.to_bytes(specNode.length, byteorder='little'))

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> int:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_single(data, int)

    @classmethod
    def py2xml(cls, data: int, specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data)


# Floating point values.
#
# The values are <em>not</em> stored following the
# @l:IEEE 754-1985:https://en.wikipedia.org/wiki/IEEE_754-1985@
# standard that was current in 2003. Instead, they use HCFF, the Horrible
# Custom Float Format.
#
# Only 32 bit single-precision is supported. Like the standard, about 7
# decimal digits are precise, including the integer part.
#
# The format follows the same structure as IEEE 754, i.e. 1-8-(1)-23 bits.
# But instead of an excess of 128, they use 150 (128+22) for the exponent.
# Other than that, the format is standard: <tt>sign * mantissa * 2^exponent</tt>.
#
# <b>Decoding binary to float:</b>
#
#   bits  content   mask
#   1     sign      10000000 00000000 00000000 00000000
#   8     exponent  01111111 10000000 00000000 00000000
#   1+23  mantissa  00000000 X1111111 11111111 11111111
#
# <em>Sign:</em> 1 bit; 1 = negative, 0 = positive
#
# <em>Exponent:</em> 8 bits; @l:excess-150:https://en.wikipedia.org/wiki/Offset_binary@
# (128+22) encoded signed integer, as a power of 2 (<tt>2^exponent</tt>).
# I.e. <tt>exponent = exponent-150</tt>.
#
# <em>Mantissa:</em> 23 remaining bits; with an implied 1 added as most
# significant bit (left).
#
# Note: if the input is <tt>00000000 00000000 00000000 00000000</tt>, the
# value is 0 and not the overflow <tt>5.877471754111438e-39</tt>.
#
# <b>Encoding float to binary:</b>
#
# Integer part and decimal part of the input build the mantissa. The default
# division method has to be used for converting the integer part to binary.
# The decimal part requires the multiplication method.
#
# The radix point of the binary floating point value must be moved to
# normalise the value.
#
#    101.010101...  with exponent = 0 becomes
#    1.01010101...  with exponent = 2
#    ·
#    000.010011...  with exponent =  0 becomes
#    001.0011.....  with exponent = -2
#    ·
#    000.10101....  with exponent = 0 becomes
#    001.0101.....  with exponent = -1
#
# The first bit is always 1 and will be dropped (hidden). This gives the
# exponent, which is then corrected by <tt>-23</tt>, i.e. the number of bits
# in the mantissa excluding the hidden bit.
#
# At last, the exponent has to be biased by 150 (128+22), i.e.
# <tt>exponent += 150</tt>.
#
# TODO: Actually, the bias of 128+22-23 is 127, the same as in IEEE 754.
# There may have been a confusion in the beginning...
#
class DecFloat(DecBase):
    xmlname = 'float'
    xmldoc = 'xxx:a single value'
    xmlex = '10.015'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> float:
        if len(data) != 4:
            log_fatal(ValueError, f"only 32bit floating point numbers are supported (got '{len(data)}' bytes)")

        return cls.bin_to_float(int.from_bytes(data, byteorder='little'))

    @classmethod
    def py2bin(cls, data: float, specNode: SpecNode, **kwargs) -> memoryview:
        return int(cls.float_to_bin(data)).to_bytes(4, byteorder='little')

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> float:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_single(data, float)

    @classmethod
    def py2xml(cls, data: float, specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data)

    @staticmethod
    def bin_to_float(number: int) -> float:
        #
        # See docs on DecFloat in decoders.py for details.
        #

        bits = f"{number:032b}"
        log.debug(f"bin to float: {number} ({bits})")

        if number == 0:
            # when decoded, 0 would yield 5.877471754111438e-39
            log.debug("- value: 0 (shortcut)")
            return 0.0

        if number >= (1 << 32):  # resp. 2^32
            log.error(f"only 32bit real numbers are supported ({number} >= {1<<32})")
            return 0.0

        sign = bits[0]
        exponent = bits[1:9]
        mantissa = '1' + bits[9:]

        _sign = (-1) ** int(sign)  # 1 = negative, 0 = positive
        _exponent = (int(exponent, 2) - (128 + 22))
        _mantissa = int(mantissa, 2)

        value = _sign * _mantissa * (2 ** _exponent)

        log.debug(f"- sign:     {sign}\t\t\t\t\t{_sign}")
        log.debug(f"- exponent: {exponent}\t\t\t\t{_exponent}")
        log.debug(f"- mantissa: (1){mantissa[1:8]} {mantissa[8:16]} {mantissa[16:]}\t{_mantissa}")
        log.debug(f"- value: {value}")

        return value

    @staticmethod
    def _decimal_part_to_bits(num: float, val: str = '', max_rounds=24, rounds=0):
        if num == 0 or rounds == max_rounds:
            return val

        # multiplication method instead of divison!
        #
        # Value *= 2 then take the integer part (1 or 0)
        # and continue with the decimal part (0.*).
        #
        rounds += 1
        frac = (num - int(num)) * 2
        val = f'{val}{int(frac)}'
        return DecFloat._decimal_part_to_bits(frac - int(frac), val, max_rounds, rounds)

    @staticmethod
    def float_to_bin(number: float) -> int:
        log.debug(f"float to bin: {number}")

        if number == 0.0:
            log.debug("- value: 0 (shortcut)")
            return 0

        if number < 0:
            log.debug("- sign: negative (1)")
            sign = '1'
            number = abs(number)
        else:
            sign = '0'
            log.debug("- sign: positive (0)")

        integer = int(number)
        integer = f'{integer:b}' if integer > 0 else ''

        fraction = DecFloat._decimal_part_to_bits(number)
        log.debug(f'{integer}.{fraction}')

        mantissa = integer + fraction
        exponent = 0
        log.debug(f'>{mantissa}')

        if integer:
            # move the radix point so we have
            #    1.010101010111
            # instead of
            #    101.0101010111
            exponent = len(integer) - 1
        elif mantissa[0] != '1':
            # values smaller than abs(dec:0.4) have leading zeros which have to be
            # removed and added to the exponent
            #
            # e.g. 0.3 = 0000.010011... with exponent =  0
            # becomes    0001.0011..... with exponent = -2
            #
            # we solve this uglily by munching and crunching the string of bits
            #
            groups = mantissa.split('1')
            exponent = (len(groups[0]) + 1) * (-1)
            mantissa = '1' + '1'.join(groups[1:])
            log.debug(f'{groups}, {groups[0]}, {mantissa}')
        else:
            # values between abs(dec:1.0) and abs(dec:0.4) must move the radix
            # point by one for normalisation
            exponent = -1

        log.debug(f"- lenint: {len(integer)} ({integer}: {len(integer)-1}), lenfrac: {len(fraction)}")
        exponent -= 23

        # build a string with all bits at the right place, then convert it to int
        # Note: the mantissa is left-aligned and the first 1 is dropped.
        value = int(f'{sign}{exponent+150:08b}{mantissa[1:]:0<23}'[0:32], 2)

        log.debug(f"- fraction: {fraction}")
        log.debug(f"- exponent: {exponent+150:08b}\t\t\t\t{exponent}")
        log.debug(f"- mantissa: (1){mantissa[1:8]} {mantissa[8:16]} {mantissa[16:]}\t= {int(mantissa[0:], 2)} ({len(mantissa)})")
        log.debug(f"- value:    {value}")

        return value

    @staticmethod
    def ieee745_to_float(N):  # ieee-745 bits (max 32 bit)
        # for comparison: the standard IEEE 754-1985 float
        # source: nino_701, https://stackoverflow.com/a/64480938
        a = int(N[0])            # sign,     1 bit
        b = int(N[1:9], 2)       # exponent, 8 bits
        c = int("1" + N[9:], 2)  # fraction, len(N)-9 bits

        return (-1)**a * c / (1 << (len(N) - 9 - (b - 127)))


# Image header.
#
# A header describing an image's properties without holding its data.
# (The zoom level is defined elsewhere.)
#
#   36 bytes:
#    2 B: width
#    2 B: height
#    2 B: palette ID
#    2 B: (unknown)
#   12 B: image format name (cstring)
#    2 B: origin X (point where animations map)
#    2 B: origin Y
#    4 B: data offset (in the respective data block)
#    4 B: data length
#    4 B: zero (unknown)
#
class DecImageHeader(DecBase):
    xmlname = 'c_image_header'
    xmldoc = 'fxx:width - height - palette ID - unknown - image format - origin x - origin y - data offset - data length - unknown (zero)'
    xmlex = '71 33 0 0800 BSH_ALPHA 46 30 167940846 135 00000000'

    CODED_SPLIT_FIELDS = ' '

    class Data(DecStringMixin, DecIntMixin):
        def __init__(self, width=0, height=0, palette_id=0, unknown='08 00', img_format='',
                     origin_x=0, origin_y=0, data_offset=0, data_length=0, zero='00 00 00 00',
                     raw: bytearray = bytearray(),
                     with_shifted_center=False):
            self._with_shifted_center = with_shifted_center

            if raw:
                self.width = self._b2i(raw[0:2])
                self.height = self._b2i(raw[2:4])
                self.palette_id = self._b2i(raw[4:6])

                if self._with_shifted_center:
                    self.unknown = raw[6:10]
                    self.img_format = self._read_cstring(raw[10:10 + 10], decode=True).string
                else:
                    self.unknown = raw[6:8]
                    self.img_format = self._read_cstring(raw[8:8 + 12], decode=True).string

                self.origin_x = self._b2i(raw[20:22])
                self.origin_y = self._b2i(raw[22:24])
                self.data_offset = self._b2i(raw[24:28])
                self.data_length = self._b2i(raw[28:32])
                self.zero = raw[32:36]

                if self.zero != bytearray(4):
                    log.warning(f"image header found where 'zero' != '0': "
                                f"zero = {self.zero.hex()} (raw hex: {raw.hex()})")
                return

            self.width = int(width)
            self.height = int(height)
            self.palette_id = int(palette_id)
            self.unknown = bytearray.fromhex(unknown)
            self.img_format = img_format
            self.origin_x = int(origin_x)
            self.origin_y = int(origin_y)
            self.data_offset = int(data_offset)
            self.data_length = int(data_length)
            self.zero = bytearray.fromhex(zero)

        def set_shifted(self, is_shifted: bool) -> None:
            self._with_shifted_center = is_shifted

        def to_bin(self):
            raw = bytearray(36)
            raw[0:2] = self._i2b(self.width, 2)
            raw[2:4] = self._i2b(self.height, 2)
            raw[4:6] = self._i2b(self.palette_id, 2)

            if self._with_shifted_center:
                raw[6:10] = self.unknown
                raw[10:20] = bytearray(self.img_format, encoding=ENCODING)
            else:
                raw[6:8] = self.unknown
                raw[8:20] = bytearray(self.img_format, encoding=ENCODING)

            raw[20:22] = self._i2b(self.origin_x, 2)
            raw[22:24] = self._i2b(self.origin_y, 2)

            if type(self.data_offset) is str and self.data_offset[0] == '@':
                raw[24:28] = self._i2b(0, 4)  # offset is a reference that can't be resolved
            else:
                raw[24:28] = self._i2b(int(self.data_offset), 4)

            raw[28:32] = self._i2b(self.data_length, 4)
            raw[32:36] = self.zero
            return raw

        def to_xml(self, data_offset_ref: str = None, palette_id_ref: str = None) -> str:
            return DecImageHeader.CODED_SPLIT_FIELDS.join([
                str(self.width),
                str(self.height),
                palette_id_ref if palette_id_ref else str(self.palette_id),
                self.unknown.hex(),
                str(self.img_format),
                str(self.origin_x),
                str(self.origin_y),
                data_offset_ref if data_offset_ref else str(self.data_offset),
                str(self.data_length),
                self.zero.hex(),
            ])

        def __str__(self):
            return self.to_xml()

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> Data:
        return cls.Data(raw=data)

    @classmethod
    def py2bin(cls, data: Data, specNode: SpecNode, **kwargs) -> memoryview:
        return data.to_bin()

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> Data:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_single(data, cls.Data)

    @classmethod
    def py2xml(cls, data: Data, specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Image header with shorter format name.
#
# A header describing an image's properties without holding its data.
# (The zoom level is defined elsewhere.)
#
#   36 bytes:
#    2 B: width
#    2 B: height
#    2 B: palette ID
#    4 B: (unknown)                             <-- difference to default image headers
#   10 B: image format name (cstring)           <-- 2+12 vs. 4+10 bytes
#    2 B: origin X (point where animations map)
#    2 B: origin Y
#    4 B: data offset (in the respective data block)
#    4 B: data length
#    4 B: zero (unknown)
#
class DecImageHeaderShifted(DecImageHeader):
    xmlname = 'c_image_header_shifted'
    xmlex = '71 33 0 08000000 BSH_ALPHA 46 30 167940846 135 00000000'

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> DecImageHeader.Data:
        return cls.Data(raw=data, with_shifted_center=True)

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> DecImageHeader.Data:
        data = cls._apply_mods(data, kwargs)

        def conv(*args):
            return cls.Data(*args, with_shifted_center=True)

        return cls._help_xml2py_single(data, conv)


# Island fertility data.
#
# A list of single-byte unsigned integer values, one value per island square.
# There is no indication for the position of each field.
#
#   example: a 5x5 map
#
#   abcde  from: abcdefghijklmnopqrstuvwxy
#   fghij
#   klmno
#   pqrst
#   uvwxy
#
class DecIslandFertility(DecBase):
    xmlname = 'c_island_fertility'
    xmldoc = 'ilx:a list of lists fertility values (0-10?)'
    xmlex = '0 1 2 3 | 9 8 7 6'

    CODED_SPLIT_ITEMS = ' '
    CODED_SPLIT_LISTS = ' | '

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[List[int]]:
        # width = parsed_node.ext.parser.xml.find(id='island-width').ext.data
        width_f = math.sqrt(len(data))
        width = int(width_f)

        if width != width_f:
            if subtree := kwargs.get('subtree', None):
                widthBlock = subtree.parent.getPath('INSEL4/#island-width / 1')

                if not widthBlock:
                    widthBlock = subtree.getPath('#island-width / 1')

                if widthBlock:
                    width = widthBlock.decodeToPy()
                else:
                    log.warning(f"invalid data: island fertility map size is not a square number ({len(data)} -> {width_f})")
            else:
                log.warning(f"invalid data: island fertility map size is not a square number ({len(data)} -> {width_f})")

        # if not width:
        #     log.warning(f"invalid data: could not find width of soil fertility data "
        #                 f"rows at #island-width ({parsed_node.ext})")
        #     width = len(data)

        matrix = []  # list of lists: int
        for line in chunks(data, width):
            matrix.append([int(x) for x in line])

        return matrix

    @classmethod
    def py2bin(cls, data: List[List[int]], specNode: SpecNode, **kwargs) -> memoryview:
        return bytearray(flatten(data))

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[List[int]]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list_of_lists(data, int)

    @classmethod
    def py2xml(cls, data: List[List[int]], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Island height map data.
#
# Height data is saved in chunks of w*h*n bytes, placed at x and y. The values
# of w, h, x, and y are generally saved right in front of the actual height
# data.
#
# This chunked format is used for other data too, where each field can take
# more than one byte (n=?). Height data is saved as one byte (uint) per field (n=1).
#
#   ...... chunks with position and size
#   ......    x = 3, y = 3
#   ......    w = 3, h = 3
#   ...xxx
#   ...xxx (in general, there can be more than one byte per field)
#   ...xxx
#
# Height data:
#
#   1 byte: height of a field (uint)
#   ...
#
class DecIslandHeightmap(DecBase):
    xmlname = 'c_island_heightmap'
    xmldoc = 'ilx:a list of lists of height values (multiples of 8?)'
    xmlex = '0 8 16 24 24 24 24 24 | 32 32 32 40 ...'

    CODED_SPLIT_ITEMS = ' '
    CODED_SPLIT_LISTS = ' | '

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[List[int]]:
        if subtree := kwargs.get('subtree', None):
            widthBlock = subtree.getPath('#height-chunk-w / 1')

        if widthBlock:
            width = widthBlock.decodeToPy()
        else:
            # width = parsed_node.find_previous_sibling(id='height-chunk-w').ext.data
            width_f = math.sqrt(len(data))
            width = int(width_f)

            if width != width_f:
                log.warning(f"invalid data: island height map size is not a square number ({len(data)} -> {width_f})")

        # if not width:
        #     log.warning(f"invalid data: could not find width of heightmap data "
        #                 f"rows at #height-chunk-w ({parsed_node.ext})")
        #     width = len(data)

        matrix = []  # list of lists: int
        for line in chunks(data, width):
            matrix.append([int(x) for x in line])

        return matrix

    @classmethod
    def py2bin(cls, data: List[List[int]], specNode: SpecNode, **kwargs) -> memoryview:
        return bytearray(flatten(data))

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[List[int]]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list_of_lists(data, int)

    @classmethod
    def py2xml(cls, data: List[List[int]], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Island vegetation map data.
#
# Vegetation data is saved in chunks similar to island height data
# (@f:c_island_heightmap@) but in a more compact format. There are six bytes
# per non-empty field.
#
# Width and height are not relevant for the chunk's size, though they are
# saved right before the chunk.
#
#   ...... chunks with position and size
#   ......    x = 3, y = 3
#   ......    w = 3, h = 3
#   ...... and fields with relative position
#   ...x..    x = 0, y = 0
#   .....x    x = 2, y = 1
#   .....x    x = 2, y = 2
#
# Vegetation data:
#
#   1 B: item id
#   1 B: mish-mash                   mask: 11111111
#       6 bit: unknown, rest of item id?   00111111
#       2 bit: orientation                 11000000
#   1 B: maybe growth?
#   1 B: height?
#   1 B: relative x position
#   1 B: relative y position
#   ...
#
# Still to decode:
#
#   - some forms of vegetation have growth versions (5 images each)
#   - some have all 4 sides (but i.e. trees have only one view!)
#   - what about rocks/wrecks/...?
#
class DecIslandVegetation(DecBase):
    xmlname = 'c_island_vegetation'
    xmldoc = 'fix:a list of items: item id - unknown - orientation - growth - height - relative x - relative y - orientation and unknown'
    xmlex = '228 5 0 0 16 22 16 5 | 151 0 3 0 34 19 17 192 | ...'

    CODED_SPLIT_FIELDS = ' '
    CODED_SPLIT_ITEMS = ' | '

    class Data:
        def __init__(self, item_id, unknown, ori, grow, height, rel_x, rel_y, ori_and_unk):
            self.item_id = int(item_id)
            self.unknown = int(unknown)
            self.ori = int(ori)
            self.grow = int(grow)
            self.height = int(height)
            self.rel_x = int(rel_x)
            self.rel_y = int(rel_y)
            self.ori_and_unk = int(ori_and_unk)

        def __str__(self):
            return DecIslandVegetation.CODED_SPLIT_FIELDS.join([
                str(self.item_id),
                str(self.unknown),
                str(self.ori),
                str(self.grow),
                str(self.height),
                str(self.rel_x),
                str(self.rel_y),
                str(self.ori_and_unk),
            ])

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> Data:
        fields_py = []  # list of items

        for item_id, ori_and_unk, grow, height, rel_x, rel_y in grouped(data, 6):
            orientation = ori_and_unk >> 6 & 0b11
            unknown = ori_and_unk & 0b00111111
            decoded = DecIslandVegetation.Data(
                item_id=int(item_id),
                unknown=int(unknown),
                ori=int(orientation),
                grow=int(grow),
                height=int(height),
                rel_x=int(rel_x),
                rel_y=int(rel_y),
                ori_and_unk=int(ori_and_unk),
            )
            fields_py.append(decoded)

        return fields_py

    @classmethod
    def py2bin(cls, data: Data, specNode: SpecNode, **kwargs) -> memoryview:
        fields = data  # list of items

        data = bytearray(len(fields) * 6)
        for i, field in enumerate(fields):
            fdata = bytearray(6)

            fdata[0] = field.item_id
            fdata[1] = field.ori * 64 + field.unknown
            fdata[2] = field.grow
            fdata[3] = field.height
            fdata[4] = field.rel_x
            fdata[5] = field.rel_y

            data[i * 6:(i * 6) + 6] = fdata
        return data

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> Data:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list(data, DecIslandVegetation.Data)

    @classmethod
    def py2xml(cls, data: Data, specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)


# Island texture data.
#
# Texture data is saved in chunks analogous to island height data (see
# @f:c_island_heightmap@). There are two bytes per field (n=2).
#
#   1 B: encoded terrain info
#   1 B: base texture ID
#   ...
#
# Each base texture is divided into four quarters, which in turn are divided
# into three rows and column. Each of these cells can be rotated up to four
# times (90° counter clockwise). This info is stored in the first byte:
#
#   bits:               11111111
#   rotations:          00000011
#   row and quarter:    00011100
#   column and quarter: 11100000
#
#   -- rotations: lowest 2 bits
#   rotations = encoded % 4
#   encoded = encoded >> 2
#
#   -- row and quarter: next lowest 3 bits
#   -- the value is always between 0 and 5
#   row = encoded % 8 % 3
#   quarter = 2 if encoded % 8 >= 3 else 0
#   encoded = encoded >> 3
#
#   -- column and quarter: last 3 bits
#   -- the value is always between 0 and 5
#   column = encoded % 8 % 3
#   quarter += 1 if encoded % 8 >= 3 else 0
#   encoded = encoded >> 3
#
# The reverse for encoding:
#
#   encoded = 0
#   encoded = encoded << 3
#   encoded += 3 if (quarter == 1 or quarter == 3) else 0
#   encoded += column
#
#   encoded = encoded << 3
#   encoded += 3 if (quarter == 2 or quarter == 3) else 0
#   encoded += row
#
#   encoded = encoded << 2
#   encoded += rotations
#
# Quarters are selected in counter clockwise order:
#
#   ├ ········· 256px ········· ┤
#   ┌─────────────┬─────────────┐   ┬
#   │ 0:   0,   0 │ 3: 128, 128 │   ·
#   ├─────────────┼─────────────┤ 256px
#   │ 1:   0, 128 │ 2: 128,   0 │   ·
#   └─────────────┴─────────────┘   ┴
#
# There are only three rows and columns even though the quarter's size (128px)
# is not cleanly divisible by 3.
#
#   ├ ·················· 128px ················· ┤
#   ├ ··· 42px ··· ┼ ··· 42px ··· ┼ ··· 42px ··· ┤
#   ┌──────────────┬──────────────┬──────────────┐   ┬    ┬
#   │ 0, 0:  0,  0 │ 1, 0: 42,  0 │ 2, 0: 84,  0 │ 42px   ·
#   ├──────────────┼──────────────┼──────────────┤   ┼    ·
#   │ 0, 1:  0, 42 │ 1, 1: 42, 42 │ 2, 1: 84, 42 │ 42px 128px
#   ├──────────────┼──────────────┼──────────────┤   ┼    ·
#   │ 0, 2:  0, 84 │ 1, 2: 42, 84 │ 2, 2: 84, 84 │ 42px   ·
#   └──────────────┴──────────────┴──────────────┘   ┴    ┴
#
# Rotations are applied to the final cell. A cell can be rotated up to four
# times by 90 degrees in counter clockwise direction:
#
#   ┌────────────┐ ┌────────────┐ ┌────────────┐ ┌────────────┐
#   │d          c│ │c          b│ │b          a│ │a          d│
#   │  0:  -90°  │ │  1: -180°  │ │  2: -270°  │ │  3: -360°  │
#   │a          b│ │d          a│ │c          d│ │b          c│
#   └────────────┘ └────────────┘ └────────────┘ └────────────┘
#
class DecIslandTexture(DecBase):
    xmlname = 'c_island_texture'
    xmldoc = 'fil:a list of lists of items: base texture ID - snippet rotations - row - column - quarter'
    xmlex = '... | 23 1 2 0 3 | 0 0 0 0 0 ## 0 0 0 0 0 | ...'

    CODED_SPLIT_FIELDS = ' '
    CODED_SPLIT_ITEMS = ' | '
    CODED_SPLIT_LISTS = ' ## '

    class Data(DecIntMixin):
        def __init__(self, base='', ori='', row='', col='', quart='',
                     raw: bytearray = bytearray()):
            if raw:
                self._decode(int(raw[0]))
                self.base = int(raw[1])
                return

            self.base = int(base)
            self.ori = int(ori)
            self.row = int(row)
            self.col = int(col)
            self.quart = int(quart)

        def _decode(self, encoded):
            self._decoded_from = encoded

            # documented in spec/s_insel.xml
            rotations = encoded % 4
            encoded = encoded >> 2
            row = encoded % 8 % 3
            quarter = 2 if encoded % 8 >= 3 else 0
            encoded = encoded >> 3
            column = encoded % 8 % 3
            quarter += 1 if encoded % 8 >= 3 else 0
            encoded = encoded >> 3

            # TODO check if it is true that 2 and 3 have to be switched, or
            #      if this was a bug in the old bash converter
            if quarter == 2:
                quarter = 3
            elif quarter == 3:
                quarter = 2

            if encoded != 0.0:
                log.error(f"island texture not fully decoded: {encoded} != 0")

            self.ori = rotations
            self.row = row
            self.col = column
            self.quart = quarter

        def _encode(self):
            # documented in spec/s_insel.xml

            # TODO check if it is true that 2 and 3 have to be switched, or
            #      if this was a bug in the old bash converter
            quarter = self.quart
            if quarter == 2:
                quarter = 3
            elif quarter == 3:
                quarter = 2

            encoded = 0
            encoded = encoded << 3
            encoded += 3 if quarter == 1 or quarter == 3 else 0
            encoded += self.col

            encoded = encoded << 3
            encoded += 3 if quarter == 2 or quarter == 3 else 0
            encoded += self.row

            encoded = encoded << 2
            encoded += self.ori

            return encoded

        def to_bin(self):
            return bytearray([self._encode(), self.base])

        def to_tup(self):
            return (self.base, self.ori, self.row, self.col, self.quart)

        def __str__(self):
            return DecIslandTexture.CODED_SPLIT_FIELDS.join([
                str(self.base),
                str(self.ori),
                str(self.row),
                str(self.col),
                str(self.quart),
            ])

    @classmethod
    def bin2py(cls, data: memoryview, specNode: SpecNode, **kwargs) -> List[List[Data]]:
        if subtree := kwargs.get('subtree', None):
            widthBlock = subtree.getFirstPath('#texture-chunk-w')

        if widthBlock:
            width = widthBlock.decodeToPy()
        else:
            # width = parsed_node.find_previous_sibling(id='texture-chunk-w').ext.data
            width_f = math.sqrt(len(data) / 2)
            width = int(width_f)

            if width != width_f:
                log.warning(f"invalid data: island texture map size is not a square number ({len(data)} -> {width_f})")

        if len(data) % 2 != 0:
            log.warning(f"invalid data: length of island texture data is not a multiple of 2 ({specNode})")

        # if not width:
        #     log.warning(f"invalid data: could not find width of texture data "
        #                 f"rows at #texture-chunk-w ({parsed_node.ext})")
        #     width = int(len(data) / 2)

        matrix = []  # list of lists of Data()
        for line in chunks(data, width * 2):
            p_line = []
            for group in grouped(line, 2):
                p_line.append(cls.Data(raw=group))
            matrix.append(p_line)

        return matrix

    @classmethod
    def py2bin(cls, data: List[List[Data]], specNode: SpecNode, **kwargs) -> memoryview:
        fields = flatten(data)
        raw = bytearray(len(fields) * 2)

        for i, field in enumerate(fields):
            raw[(i * 2):(i * 2) + 2] = field.to_bin()

        return raw

    @classmethod
    def xml2py(cls, data: str, specNode: SpecNode, **kwargs) -> List[List[Data]]:
        data = cls._apply_mods(data, kwargs)
        return cls._help_xml2py_list_of_lists(data, cls.Data)

    @classmethod
    def py2xml(cls, data: List[List[Data]], specNode: SpecNode, **kwargs) -> str:
        return cls._save_xml(data, str)
