# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import re
from functools import cache
from functools import cached_property

from typing import TypeVar
from typing import List


TBlockNode = TypeVar('BlockNode')
TDataNode = TypeVar('DataNode')
TGroupNode = TypeVar('GroupNode')
TSpecNode = TypeVar('SpecNode')


class PathError(SyntaxError):
    """ Base class for errors in user defined path strings. """
    pass


class InvalidPathError(PathError):
    pass


class InvalidIdentFieldType(PathError):
    pass


# ANNO/blah/path / *
SELECTOR_RE = re.compile(r'(?P<path>.*) / (?P<select>(-?[1-9][0-9]*)|\*|0)$')

# ANNO/FIGUREN/ENTRY@10/#colors/ENTRY@palette-id:10
ELEM_RE = re.compile(r'^((?P<tag>[_A-Z0-9]+)|(#(?P<tagSelector>[-_a-zA-Z0-9]+))|(?P<kindSelector>data|group))'
                     r'(@((?P<identSelector>[-a-zA-Z0-9]+):)?(?P<ident>[0-9]+))?$')


class Path:
    class Selector:
        ALL = 1
        INDEX = 2

        def __init__(self, kind: int, index: int = None):
            self.kind = kind
            self.index = index

        @property
        def matchAll(self):
            return self.kind == self.ALL

        @cache
        def __repr__(self):
            return f'PathSelector({self})'

        @cache
        def __str__(self):
            if self.kind == self.ALL:
                return ' / *'

            elif self.index >= 0:
                # NOTE the STORED index starts at 0 but the PRINTED index starts at 1
                return f' / {self.index + 1:d}'
            else:
                # NOTE negative indices are kept unchanged
                return f' / {self.index:d}'

    class ElementBase:
        def __init__(self, elem: str = '', matchAll: bool = False):
            elem = elem.strip('/')
            self._source = elem
            self.tagAttr = None
            self.tagValue = None
            self.identChild = None
            self.identValue = None
            self.matchAll = matchAll

            self.isLastElement = False

        @cache
        def __repr__(self):
            return f"PathElement({self})"

        @cache
        def __str__(self):
            return "<Invalid>"

        @cached_property
        def css(self):
            raise InvalidPathError(f"cannot convert '{self}' to CSS")

        def matchTag(self, node: TSpecNode) -> bool:
            return self.matchAll

        def matchIdent(self, node: TDataNode) -> bool:
            return self.matchAll

        def matchIdent2(self, node: [TBlockNode, TGroupNode, TSpecNode]) -> bool:
            return self.matchAll

        @cached_property
        def matchAllTags(self) -> bool:
            return self.matchAll

        @cached_property
        def matchAllIdents(self) -> bool:
            return self.matchAll or not self.identChild

        @cached_property
        def identElement(self) -> ['Path.Element', None]:
            if self.identChild:
                return Path.Element(f'#{self.identChild}')
            return None

    class Element(ElementBase):
        UINT_STR = {1: '<B', 2: '<H', 4: '<L', 8: '<Q'}
        UINT_VAL = UINT_STR.keys()

        def __init__(self, elem: str):
            super(Path.Element, self).__init__(elem=elem)
            self._parsed = ELEM_RE.match(self._source)

            if not self._parsed:
                raise InvalidPathError(f"'{elem}' is not a valid path element")

            if self._parsed.group('kindSelector'):
                self.tagAttr = 'kind'
                self.tagValue = self._parsed.group('kindSelector')
            elif self._parsed.group('tagSelector'):
                self.tagAttr = 'ident'
                self.tagValue = self._parsed.group('tagSelector')
            else:
                self.tagAttr = 'title'
                self.tagValue = self._parsed.group('tag')

            if self._parsed.group('ident'):
                self.identValue = int(self._parsed.group('ident'))

                if self._parsed.group('identSelector'):
                    self.identChild = self._parsed.group('identSelector')
                else:
                    self.identChild = 'entry-id'

        @cache
        def matchTag(self, node: [TSpecNode, TBlockNode, TDataNode, TGroupNode]) -> bool:
            if hasattr(node, 'specNode'):
                specNode = node.specNode
            else:
                specNode = node

            if getattr(specNode, self.tagAttr, '') == self.tagValue:
                return True

            return False

        @cache
        def matchIdent(self, node: TDataNode) -> bool:
            # WARNING: must match on a PARSED DATA NODE which is a CHILD of
            # the actual node that should match.

            if not self.identChild:
                return True

            if node.specNode.kind != 'data':
                return False

            if node.specNode.ident == self.identChild:
                length = node.specNode.length

                if node.specNode.type_ != 'uint' or length not in self.UINT_VAL:
                    raise InvalidIdentFieldType(f"{self}: id fields must be of type uint, got "
                                                f"'{node.specNode.type_}' ('{length}' bytes)")

                if node.decodeToPy() == self.identValue:
                    # print("MATCHED", self.identValue, node)
                    return True
                # else:
                #     print("NO MATCH", self.identValue,
                #           struct.unpack(self.UINT_STR[length], node.dataView[:length])[0],
                #           self.identChild == node.specNode.ident)

            return False

        @cache
        def matchIdent2(self, node: [TBlockNode, TGroupNode, TSpecNode]) -> bool:
            # NOTE: Matches on the id attribute are done in matchTag.
            # WARNING: This looks at THE NODE'S CHILDREN to check if one of them matches.

            if not self.identChild:
                return True

            if hasattr(node, 'specNode'):
                if not node.specNode.isHead and not node.specNode.isGroup:
                    return False

                isSpecNode = False
            else:
                if not node.isHead and not node.isGroup:
                    return False

                isSpecNode = True

            for i in node.children:
                if isSpecNode:
                    if i.isData and i.ident == self.identChild:
                        if i.typeStr != 'uint' or i.length not in self.UINT_VAL:
                            raise InvalidIdentFieldType(f"{self}: id fields must be of type uint, got "
                                                        f"'{i.typeStr}' ('{i.length}' bytes)")

                        # We are operating on spec nodes, so we only check if there
                        # is a possibility that the node (if parsed) could have the
                        # required value.
                        return True
                else:
                    if i.specNode.isData and i.specNode.ident == self.identChild:
                        if i.specNode.typeStr != 'uint' or i.specNode.length not in self.UINT_VAL:
                            raise InvalidIdentFieldType(f"{self}: id fields must be of type uint, got "
                                                        f"'{i.specNode.typeStr}' ('{i.specNode.length}' bytes)")

                        if i.decodeToPy() == self.identValue:
                            return True

            return False

        @cache
        def __str__(self):
            return ('#' if self.tagAttr == 'ident' else '') + self.tagValue + \
                (f"@{self.identChild}:{self.identValue}" if self.identChild else "")

        @cached_property
        def css(self):
            if self.tagAttr == 'ident':
                tag = f'#{self.tagValue}'
            elif self.tagAttr == 'kind':
                tag = f'{self.tagValue}'
            elif self.tagAttr == 'title':
                tag = f'head[title="{self.tagValue}"]'
            else:
                raise InvalidPathError(f"cannot convert '{self}' to CSS")

            if self.identChild:
                ident = f':has(> #{self.identChild}[value="{self.identValue}"])'
            else:
                ident = ''

            return f'{tag}{ident}'

    class GlobElement(ElementBase):
        def __init__(self):
            super(Path.GlobElement, self).__init__(elem='', matchAll=True)

        @cache
        def __repr__(self):
            return "<Anything>"

        @cache
        def __str__(self):
            return "*"

    class LevelElement(ElementBase):
        def __init__(self):
            super(Path.LevelElement, self).__init__(elem='', matchAll=True)

        @cache
        def __repr__(self):
            return "<Here>"

        @cache
        def __str__(self):
            return "."

    class NoneElement(ElementBase):
        def __init__(self):
            super(Path.NoneElement, self).__init__(elem='', matchAll=False)

        @cache
        def __str__(self):
            return "<Nothing>"

    def __init__(self, path: str):
        if not path:
            raise InvalidPathError(f"path is empty: {path}")

        if select := SELECTOR_RE.match(path):
            path = select.group('path')
            select = select.group('select')

            if select == '*':
                self.selector = self.Selector(self.Selector.ALL, None)
            else:
                select = int(select)

                if select > 0:
                    select -= 1
                elif select == 0:
                    raise InvalidPathError(f"item selectors use indexes starting at 1, got {select} in {path}")

                self.selector = self.Selector(self.Selector.INDEX, select)
        else:
            self.selector = self.Selector(self.Selector.ALL, None)

        path = path.strip('/')

        if path.endswith('/*') or path == '*':
            path = path[:-2]
            self._elements = [self.GlobElement()]
        elif path.endswith('/.') or path == '.':
            path = path[:-2]
            self._elements = [self.LevelElement()]
        else:
            self._elements = []

        self._source = path

        if path:
            self._elements = [self.Element(x) for x in path.split('/') if x.strip('/')] + self._elements

        if not self._elements:
            pass
            # self._elements = [self.LevelElement()]
        else:
            self._elements[-1].isLastElement = True

    def __iter__(self):
        return self.elements.__iter__()

    def __len__(self):
        return len(self.elements)

    @cache
    def __str__(self):
        if self.selector and self.selector.kind != self.selector.ALL:
            selector = str(self.selector)
        else:
            selector = ''

        if len(self._elements) == 0:
            return '/' + selector

        return ("/".join([str(x) for x in self._elements]) if self._elements else '/') + selector

    @cache
    def __repr__(self):
        return f"Path({self})"

    @cached_property
    def css(self):
        # TODO add self.selector
        return " > ".join(x.css for x in self._elements) if self._elements else ""

    @cached_property
    def elements(self) -> List[ElementBase]:
        return self._elements

    def __contains__(self, node: [TBlockNode, TDataNode, TGroupNode, TSpecNode]) -> bool:
        return self.match(node, matchSubset=True)

    def match(self, node: [TBlockNode, TDataNode, TGroupNode, TSpecNode], matchSubset: bool = False) -> bool:
        otherPath: Path = node.specPath
        treePath: List[[TBlockNode, TDataNode, TGroupNode]] = node.path

        if treePath and treePath[0].specNode.isFileRoot:
            if len(self) == 1 and isinstance(self.elements[0], self.NoneElement):
                return True

            treePath = treePath[1:]

        if not treePath and len(otherPath) == 1 and \
                isinstance(otherPath.elements[0], self.NoneElement):
            # 'node' is a sole root node, e.g. <file>.

            if self.elements[0].matchAll:
                return True
            elif matchSubset:
                return True
            else:
                return False

        if len(otherPath) != len(treePath):
            raise InvalidPathError(f'The calculated path {otherPath} is not a match for {node} at {treePath} | matching against {self}')

        # Abort if our path is shorter than their path and we don't have a GlobElement
        # as our last element. Example:
        #   we:     ANNO/FIGUREN/ENTRY          -> no match
        #   other:  ANNO/FIGUREN/ENTRY/HAEUSER
        #
        #   we:     ANNO/FIGUREN/ENTRY/*        -> match
        #   other:  ANNO/FIGUREN/ENTRY/HAEUSER
        if len(otherPath) > len(self) and not self.elements[-1].matchAll:
            return False

        # If matching subsets is allowed and our path is longer than their path,
        # we only compare up to the shared length.
        #
        # If complete matching is required, we abort if our path is longer (i.e.
        # more specific) than their path.
        #
        # Example with matchSubset == True:
        #   we:     ANNO/FIGUREN/ENTRY/ |< match up to here <| HAEUSER/ENTRY/TEXTURES/#zoom-level
        #   they:   ANNO/FIGUREN/ENTRY   -> match
        #
        #   they:   ANNO/FIGUREN/WOOF    -> no match
        #
        # Example with matchSubset == False:
        #   we:     ANNO/FIGUREN/ENTRY/HAEUSER/ENTRY/TEXTURES/#zoom-level
        #   they:   ANNO/FIGUREN/ENTRY   -> no match
        #   they:   ANNO/FIGUREN/WOOF    -> no match
        if matchSubset and len(self) > len(otherPath):
            activeElements = self.elements[0:len(otherPath)]
        else:
            # Abort early if one path is more specific than the other...
            if len(self) > len(otherPath):
                return False

            activeElements = self.elements

        # print("---")
        # print(activeElements)
        # print(self)
        # print(otherPath)

        for we, other in zip(activeElements, treePath):
            if not we.matchTag(other):
                # print("tag mismatch:", we, other)
                return False

            if not we.matchIdent2(other):
                # print("ident mismatch:", we, other)
                return False

        # NOTE it's not possible to check the index selector here because we don't know
        # how many other nodes are matching as well. The caller has to check that!

        return True

    def matchResultIndex(self, index: int) -> bool:
        if self.selector.matchAll:
            return True

        if self.selector.index == index:
            return True

        return False
