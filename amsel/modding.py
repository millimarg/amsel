# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import contextlib
import copy
import tempfile
import os
import io
import re
import glob
import logging as log

from dataclasses import dataclass
from collections import defaultdict
from pathlib import Path as FilePath
from typing import Dict
from typing import Callable
from typing import List
from typing import Tuple
from typing import Any

from bs4 import BeautifulSoup
from lxml import etree

from .tools import DataPaths
from .tools import request_file_name
from .tools import request_value
from .tools import resolved_xml_templ
from .tools import save_xml
from .tools import is_int
from .tools import flatten
from .tools import combine_figuren_data_files
from .tools import autoselect_figuren_data_files
from .tools import backup_existing_file
# from .tools import random_str
from .spec import Spec
from .parser import ParseMode
from .parser import Parser
from .parser import ParsedNode
from .parser import NewDataFactory
from .parser import ModCache
from .path import Path as ObjectPath
from .path import PathError as ObjectPathError
from .graphics import Palette
from .graphics import Image
from .decoders import load_decoders
from .decoders import get_transcoder
from .decoders import DecCString
from .decoders import DecColorList
# from .path import Path as AddPath


@dataclass(frozen=True)
class _LoaderInfo:
    reference: str
    attributes: dict
    loaded: Any


@dataclass(frozen=True)
class _FileSelect:
    name: FilePath
    kind: str


_id_counter = 0
_regexes = {}


def _make_id(prefix):
    global _id_counter
    _id_counter += 1
    return (str(prefix) + "-" if prefix else "") + f'{_id_counter:06d}'


def _replace_regex(string, fromRe, toRe):
    global _regexes

    if fromRe not in _regexes:
        _regexes[fromRe] = re.compile(fromRe)

    # if toRe not in _regexes:
    #     _regexes[toRe] = re.compile(toRe)

    # return _regexes[fromRe].sub(_regexes[toRe], string)
    return _regexes[fromRe].sub(toRe, string)


@dataclass
class _Shorthand:
    expanded: str
    ident: str = ''


_path_shorthands = {
    'figuren': _Shorthand('ANNO/FIGUREN/ENTRY{ident}', 'entry-id'),
    'figuren_palette': _Shorthand('COLORS/ENTRY{ident}', 'palette-id'),

    'anlagen': _Shorthand('ANNO/ANLAGEN/ENTRY{ident}', 'entry-id'),
    'anlagen_palette': _Shorthand('ANNO/COLORS/ENTRY{ident}', 'palette-id'),

    'textures': _Shorthand('TEXTURES{ident}', 'zoom-level-id'),
    'animation': _Shorthand('HAEUSER/ENTRY{ident}', 'anim-id'),
}


def _expand_abbreviated_path(input_path) -> str:
    """ Expand abbreviations in object paths.

    Example:
        abbreviated: @my-reference/$figuren@103/$animation@4/$textures@0 / 1
        expanded:    @my-reference/ANNO/FIGUREN/ENTRY@entry-id:103/HAEUSER/ENTRY@anim-id:4/TEXTURES@zoom-level-id:0 / 1

    Paths are automatically expanded when a mod file is resolved. Paths that are
    not found during that step can be expanded manually using the "|path" filter.

    Invalid object paths raise a ModError exception.
    """

    original_path = input_path

    for key, shorthand in _path_shorthands.items():
        replaced = ''
        start = 0

        for match in re.finditer(r'\${key}(@(?P<ident>\d+))?\b'.format(key=key), input_path):
            ident = match.group('ident')

            if ident:
                expanded = shorthand.expanded.format(ident=f'@{shorthand.ident}:{ident}')
            else:
                expanded = shorthand.expanded.format(ident='')

            end, start_new = match.span()
            replaced += input_path[start:end]
            replaced += expanded
            start = start_new

        replaced += input_path[start:]
        input_path = replaced

    try:
        without_reference = re.sub(r'^@[\-A-Za-z0-9_]+?/', '', input_path)
        ObjectPath(without_reference)
    except ObjectPathError as e:
        if original_path != input_path:
            msg = f'invalid object path "{original_path}" ({input_path}): {e}'
        else:
            msg = f'invalid object path "{input_path}": {e}'

        raise ModError(msg)

    return input_path


def _glob_resources(pattern, basePath):
    if FilePath(basePath).resolve() not in (FilePath(basePath) / pattern).resolve().absolute().parents:
        raise ValueError(f'resources must not be outside of {basePath}, got {pattern}')

    found = [str(FilePath(x).relative_to(basePath))
             for x in glob.glob(str(FilePath(basePath) / pattern), recursive=False)]

    if not found:
        raise ValueError(f'glob pattern {pattern} matched no files')

    return found


def _xml(node) -> str:
    """ Helper function to format nodes for logging.
        Returns a tag's name and attributes but skips children.
    """

    ret = f'<{node.name}'
    attrs = [f'{k}="{v}"' for k, v in node.attrs.items()]

    if attrs:
        ret += ' ' + ' '.join(attrs)

    return ret + '>'


class ModError(Exception):
    pass


class ValidationFailedError(ModError):
    pass


class DuplicateIdentError(ModError):
    pass


class InvalidDataTypeError(ModError):
    """ A change tries to apply data in an invalid format, or tries to change an unsupported spec node. """
    pass


class _Blob:
    def __init__(self):
        self.length = 0
        self.string = ''

    def read_file(self, infile):
        with open(infile, 'rb') as fd:
            fd.seek(0, os.SEEK_END)
            self.length = fd.tell()
            fd.seek(0, os.SEEK_SET)
            self.string = fd.read().hex(sep=' ', bytes_per_sep=4)


class Modder:
    EXTRACT_SPEC_NAMES = ('xml', 'data')

    def close(self):
        self._close_parsers()

    def __enter__(self):
        return contextlib.closing(self).__enter__()

    def __exit__(self, *args):
        self._close_parsers()

    def __init__(self, modfile: str,
                 fileselect_callback: Callable = request_file_name,
                 file_presets: Dict[str, str] = {},
                 variables: Dict[str, str] = {},
                 variable_callback: Callable = request_value):
        self._target_path = FilePath().cwd().resolve().absolute()
        self._resource_path = FilePath(modfile).resolve().parent.absolute()

        self._modfile: str = modfile
        self._modxml_resolved: BeautifulSoup = None
        self._modxml_compiled: BeautifulSoup = None

        self._fileselect_callback: Callable = fileselect_callback
        self._file_presets: Dict[str, str] = {f'@{k.lstrip("@")}': v for k, v in file_presets.items()}
        self._files_cache: Dict[FilePath, _FileSelect] = {}
        self._files_cache_aliases: Dict[str, FilePath] = {}

        self._variable_callback: Callable = variable_callback
        self._variables: Dict[str, str] = variables

        self._parsers: Dict[FilePath, Parser] = {}
        self._modified_targets: Dict[FilePath, bool] = defaultdict(lambda: False)
        self._placeholder_files: List[FilePath] = []

        # outer dict keys are value groups (palette, image...)
        # inner dict keys are user defined identifiers
        self._loaded_cache: Dict[str, Dict[str, _LoaderInfo]] = defaultdict(dict)

        self._actions_count: int = 0
        self._failed_actions_count: int = 0
        self._current_selects: Dict[str, [List[ParsedNode], str]] = {}
        self._current_selects_delayed_ref: List[str] = []
        # self._current_selects_delayed_parse: Dict[str, Tuple[str, BeautifulSoup]] = defaultdict(list)
        self._clipboard: Dict[str, BeautifulSoup] = {}
        self._global_mod_cache = ModCache()

        self._templ_filters: Dict[str, Callable] = {
            'as_int': int,
            'replace': _replace_regex,
            'path': _expand_abbreviated_path,
        }
        self._templ_globals: Dict[str, Callable] = {
            'is_int': is_int,
            'make_id': _make_id,
            'glob_res': lambda x: _glob_resources(x, self._resource_path),
        }

        self._save_separately: bool = False
        self._save_extracted: bool = False
        self._make_backups: bool = True

    def save_resolved(self, outfile: [str, FilePath], overwrite: bool = False) -> None:
        self._do_resolve()
        save_xml(outfile, self._modxml_resolved, force=overwrite)

    def save_compiled(self, outfile: [str, FilePath], overwrite: bool = False, compress=True) -> None:
        self._do_compile()
        save_xml(outfile, self._modxml_compiled, force=overwrite, compress=compress)

    def validate(self, method: str = 'rnc') -> None:
        self._do_resolve()
        self._do_validate(method)

    def do_apply(self, dry_run: bool = False, save_separately: bool = False, save_extracted: bool = False, make_backups: bool = True):
        self._save_separately = save_separately
        self._save_extracted = save_extracted
        self._make_backups = make_backups
        self._do_apply(dry_run)

    def _do_resolve(self):
        if self._modxml_resolved is not None:
            return

        log.info("resolving mod xml...")
        self._modxml_resolved = resolved_xml_templ(self._modfile, [DataPaths.mod_templates],
                                                   custom_filters=self._templ_filters,
                                                   custom_globals=self._templ_globals)

        def fix(node, attribute):
            node[attribute] = _expand_abbreviated_path(node[attribute])

        for node in self._modxml_resolved.find_all(self._object_paths_finder, recursive=True):
            if node.name == 'select':
                fix(node, 'select')
            elif node.name == 'change':
                fix(node, 'select')
            elif node.name == 'load-palette':
                fix(node, 'id-source')
            elif node.name == 'data':
                fix(node, 'x-increase-from')
            elif node.name == 'clipboard':
                continue

    def _do_validate(self, method: str = 'rnc'):
        self._do_resolve()

        log.info("validating mod...")

        if method == 'rnc':
            schema = etree.RelaxNG(file=str(FilePath(Spec.get_spec_dir()) / 'schema/schema.rnc'))
        elif method == 'xsd':
            schema = etree.XMLSchema(file=str(FilePath(Spec.get_spec_dir()) / 'schema/schema.xsd'))
        else:
            raise ValueError(f'unknown schema validation method "{method}"')

        string = io.BytesIO(str(self._modxml_resolved).encode('utf8'))
        doc = etree.parse(string)

        if not schema.validate(doc):
            log.error("validation failed:")

            for i in schema.error_log:
                log.error(i)

            raise ValidationFailedError("Validation failed.")

    def _do_compile(self) -> None:
        if self._modxml_compiled is not None:
            return

        log.info("compiling mod xml...")

        self._do_resolve()
        self._do_validate()
        self._modxml_compiled = copy.copy(self._modxml_resolved)

        groups = [('palette', 'load-palette', Palette),
                  ('image', 'load-image', Image),
                  ('blob', 'load-blob', _Blob)]

        for key, tag, data_cls in groups:
            for node in self._modxml_compiled.find_all(tag):
                if node['id'] in self._loaded_cache['key']:
                    raise DuplicateIdentError(f'id "{node["id"]}" is used more than once for loading a "{key}"')

                self._loaded_cache[key][node['id']] = _LoaderInfo(node['id'], node.attrs, data_cls() if data_cls else None)
                node.extract()

        for info in self._loaded_cache['palette'].values():
            self._compile_palette(info)

        for info in self._loaded_cache['image'].values():
            # note: images require palettes to be loaded first
            self._compile_image(info)

        for info in self._loaded_cache['blob'].values():
            self._compile_blob(info)

        for node in self._modxml_compiled.find_all(name='change', attrs={'action': re.compile(r'clone:.*')}, recursive=True):
            copy_path = node['copy']
            paste_path = node['paste']
            paste_action = node['action'][6:]
            copy_id = _make_id('clone')

            copy_tag = self._modxml_compiled.new_tag('change', action='copy', select=copy_path, id=copy_id)
            paste_tag = self._modxml_compiled.new_tag('change', action=paste_action, select=paste_path)
            clip_tag = self._modxml_compiled.new_tag('clipboard', slot=f'@{copy_id}', id=node.get('id', _make_id('pasted')))

            node.insert_before(copy_tag)
            paste_tag.append(clip_tag)
            node.insert_before(paste_tag)
            node.extract()

    def _compile_blob(self, info: _LoaderInfo) -> None:
        log.info(f'loading data blob: {info.attributes}')

        # --- load the object

        infile = self._get_validated_path(info.attributes['source'],
                                          requiredParent=self._resource_path, allowAbsolute=False)
        info.loaded.read_file(infile)

        # --- compile data nodes

        # <blob-data> => <data>
        for node in self._search_all_ref(self._modxml_compiled, 'blob-data', info.reference):
            data_node = self._modxml_compiled.new_tag('data')
            data_node['x-mod-id'] = _make_id(f'{info.reference}-data')

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            data_node['type'] = 'raw'
            data_node['length'] = info.loaded.length
            data_node['value'] = info.loaded.string

            node.replace_with(data_node)

    def _compile_palette(self, info: _LoaderInfo) -> None:
        log.info(f'loading palette: {info.reference} {info.attributes} -> {info.loaded}')

        # --- load the object

        infile = self._get_validated_path(info.attributes['source'],
                                          requiredParent=self._resource_path, allowAbsolute=False)
        info.loaded.from_file(infile)

        # --- compile cache seeds for static values

        if 'id-value' in info.attributes:
            cache_node = self._modxml_compiled.new_tag('seed-cache')
            cache_node['key'] = f'{info.reference}-id'
            cache_node['value'] = info.attributes.get('id-value')
            self._modxml_compiled.file.meta.insert_after(cache_node)

        # --- compile data nodes

        # <palette-id> => <data>
        for node in self._search_all_ref(self._modxml_compiled, 'palette-id', info.reference):
            data_node = self._modxml_compiled.new_tag('data')

            # Note: palette-id does not get a new unique id because it is used in
            # image headers via use-values.
            data_node['x-mod-id'] = f'{info.reference}-id'

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            data_node['type'] = 'uint'
            data_node['length'] = '4'
            data_node['value'] = info.attributes.get('id-value', '0')
            data_node['x-store-value'] = data_node['x-mod-id']

            if 'id-source' in info.attributes:
                data_node['x-increase-from'] = info.attributes['id-source']

            node.replace_with(data_node)

        # <palette-name> => <data>
        for node in self._search_all_ref(self._modxml_compiled, 'palette-name', info.reference):
            data_node = self._modxml_compiled.new_tag('data')
            data_node['x-mod-id'] = _make_id(f'{info.reference}-name')

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            if 'fixed-length' in node.attrs:
                data_node['type'] = 'cstring_fixed'
                data_node['length'] = node['fixed-length']
                data_node['value'] = info.loaded.name[-int(node['fixed-length']):]
            else:
                data_node['type'] = 'cstring'
                data_node['length'] = len(DecCString.py2bin(info.loaded.name, specNode=None))
                data_node['value'] = info.loaded.name

            node.replace_with(data_node)

        # <palette-body> => <data>
        for node in self._search_all_ref(self._modxml_compiled, 'palette-body', info.reference):
            data_node = self._modxml_compiled.new_tag('data')

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            data_node['x-mod-id'] = _make_id(f'{info.reference}-body')
            data_node['type'] = 'c_color_list'
            data_node['length'] = '1024'
            data_node['value'] = DecColorList.py2xml(info.loaded.entries, None)
            node.replace_with(data_node)

    def _compile_image(self, info: _LoaderInfo) -> None:
        log.info(f'loading image: {info.reference} {info.attributes} -> {info.loaded}')

        # --- load the object

        if 'palette' not in info.attributes:
            # It is not possible to allow a default value here because the
            # palette has to be saved somewhere. This has to be done explicitly
            # by the mod author. Also, generating a palette is slow and should be avoided.
            raise NotImplementedError(f'invalid mod: palette required for loading an image ({info.attributes})')

        palref = info.attributes['palette']

        if palref.startswith('@'):
            # The leading '@' is not required as this field is always interpreted
            # as a reference and never as a file.
            palref = palref[1:]

        # Note: palettes must be defined separately in a <load-palette> element
        # because palettes must have valid ID numbers.

        if not palref:
            raise ModError(f'no palette defined in {info.attributes}')

        if palref not in self._loaded_cache['palette']:
            raise ModError(f'palette "{palref}" must be loaded '
                        f'before it can be used in an image ({info.attributes})')
        pal = self._loaded_cache['palette'][palref].loaded

        infile = self._get_validated_path(info.attributes['source'],
                                          requiredParent=self._resource_path, allowAbsolute=False)

        info.loaded.from_png(infile, palette=pal, targetFormatName=info.attributes.get('format', 'BSH_RGBV'))
        info.loaded.head.origin_x = int(info.attributes.get('hotspot', '0x0').split('x')[0])
        info.loaded.head.origin_y = int(info.attributes.get('hotspot', '0x0').split('x')[-1])
        info.loaded.unknown = bytes([8, 0])
        bodyOffsetCorrection = info.attributes.get('body-offset-correction', '0')

        # --- compile data nodes

        # <image-name> => <data>
        for node in self._search_all_ref(self._modxml_compiled, 'image-name', info.reference):
            data_node = self._modxml_compiled.new_tag('data')
            data_node['x-mod-id'] = _make_id(f'{info.reference}-name')

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            if 'fixed-length' in node.attrs:
                data_node['type'] = 'cstring_fixed'
                data_node['length'] = node['fixed-length']
                data_node['value'] = info.loaded.name[-int(node['fixed-length']):]
            else:
                data_node['type'] = 'cstring'
                data_node['length'] = len(DecCString.py2bin(info.loaded.name, specNode=None))
                data_node['value'] = info.loaded.name

            node.replace_with(data_node)

        # <image-body> => <data>
        # <data type="raw" value="01 02 ..." x-store-offset="img" />
        for node in self._search_all_ref(self._modxml_compiled, 'image-body', info.reference):
            image_body_id = _make_id(f'{info.reference}-body')
            data_node = self._modxml_compiled.new_tag('data')

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            data_node['x-mod-id'] = image_body_id
            data_node['x-store-offset'] = image_body_id
            data_node['x-offset-correction'] = node.get('offset-correction', None) or bodyOffsetCorrection

            data_node['type'] = 'raw'
            data_node['value'] = info.loaded.data.hex(sep=' ', bytes_per_sep=4)
            data_node['length'] = len(info.loaded.data)

            node.replace_with(data_node)

        # <image-header> => <data>
        # <data type="c_image_header" value="aaaa @img@ bbbb" x-use-offsets="img" />
        for node in self._search_all_ref(self._modxml_compiled, 'image-header', info.reference):
            data_node = self._modxml_compiled.new_tag('data')
            data_node['x-mod-id'] = _make_id(f'{info.reference}-header')

            if ident := node.get('set-id', None):
                data_node['id'] = ident

            # all headers refer to the last body instance
            # note: normally, there is only one instance of the header and one instance
            #       of the body for a single image
            data_node['x-use-offsets'] = image_body_id
            data_node['x-use-values'] = f"{node.get('x-use-values', '')} {palref}-id".strip()
            data_node['x-offset-correction'] = node.get('body-offset-correction', None) or bodyOffsetCorrection

            if node.get('shifted', 'false') == 'true':
                data_node['type'] = 'c_image_header_shifted'
            else:
                data_node['type'] = 'c_image_header'

            oldHotspots = (info.loaded.head.origin_x, info.loaded.head.origin_y)

            if 'hotspot' in node.attrs:
                info.loaded.head.origin_x = int(node.get('hotspot', '0x0').split('x')[0])
                info.loaded.head.origin_y = int(node.get('hotspot', '0x0').split('x')[-1])

            data_node['length'] = '36'
            data_node['value'] = info.loaded.head.to_xml(data_offset_ref=f'@{image_body_id}@', palette_id_ref=f'@{palref}-id@')
            node.replace_with(data_node)

            info.loaded.head.origin_x, info.loaded.head.origin_y = oldHotspots

    def _search_all_ref(self, xml: BeautifulSoup, tag: str, ref: str):
        """ Search for all nodes of type <tag> with attribute ref='ref'. """
        for node in xml.find_all(lambda x: x.name == tag and x['ref'] == ref, recursive=True):
            yield node

    def _enforce_reference_format(self, node, title, attribute) -> str:
        value = node.get(attribute, '')

        if not value:
            return ''
        else:
            value = str(value)

        if value[0] == '@':
            value = value[1:]

            if not value:
                raise ValueError(f'invalid mod: empty reference in "{attribute}" of {title} {_xml(node)}')
        else:
            raise ValueError(f'invalid mod: references must start with an "@" in "{attribute}" of {title} {_xml(node)}')

        return value

    def _do_apply(self, dry_run: bool) -> None:
        self._do_resolve()
        self._do_validate()
        self._select_files()
        self._select_variables()
        self._select_choices()
        self._select_active_changesets()
        self._run_preparation_actions()
        self._gather_changeset_files()

        self._do_compile()
        self._seed_caches()
        self._setup_parsers()

        for changeset in self._modxml_compiled.select('file > changeset'):
            self._handle_changeset(changeset)

        if self._failed_actions_count > 0 and self._failed_actions_count >= self._actions_count:
            log.error(f'mod failed to apply, nothing saved ({self._failed_actions_count} out of {self._actions_count} failed)')
        else:
            log.info(f'{self._actions_count} actions successfully applied')
            self._save_files(dry_run)

        self._close_parsers()

    def _select_variables(self):
        self._do_resolve()

        log.info('loading constants...')

        for k, v in [(x['id'], x['value']) for x in self._modxml_resolved.select('file > constant')]:
            log.info(f'- setting variable: {k} = {v}')
            self._global_mod_cache.storeValue(k, v)

        log.info('loading variables...')

        supported_variable_types = load_decoders().keys()

        for node in self._modxml_resolved.select('file > variable'):
            key = node['id']
            default = node.get('default', None)
            value = self._variables.get(key)
            kind = node.get('type', None)
            prompt = node.get('prompt', key)

            if node.get('options', None) is not None:
                field_sep = node.get('field-sep', ':')
                entry_sep = node.get('entry-sep', ',')
                options = [[x.strip(), x.strip()] if len(x.split(field_sep)) == 1 else [x.split(field_sep)[0].strip(), x.split(field_sep)[1].strip()]
                        for x in node.get('options', '').split(entry_sep)]
                log.info(f'variables: {value} {self._variables} {key} | {options}')
            else:
                options = []
                log.info(f'variables: {value} {self._variables} {key}')

            if value is None:
                value = self._variable_callback(prompt, default, options)

                if value is None:
                    raise ValueError(f'variable {key} is not defined and has no default value')

            if kind not in supported_variable_types:
                raise ValueError(f'variable {key} has unknown type {kind}')

            # TODO: verify variables to make sure type constraints are respected

            log.info(f'- setting variable: {key} = {value}')
            self._global_mod_cache.storeValue(key, value)

    def _select_choices(self):
        self._do_resolve()

        log.info('loading choices...')

        for node in self._modxml_resolved.select('file > choice'):
            key = node['id']
            default = self._enforce_reference_format(node, 'choice', 'default')
            prompt = node.get('prompt', key)
            options = [(x['id'], x['name']) for x in node.select('option')]
            value = self._variables.get(key)

            # TODO verify that 'default' points to a valid option
            # TODO verify that 'value' points to a valid option

            if not value:
                value = self._variable_callback(prompt, default, options)

            log.info(f'selected "{value}" for {_xml(node)}')

            for k, v in [(f'choice-{key}-{value}', 'true'), (f'choice-{key}', f'@{value}')] + \
                    [(x['id'], x['value']) for x in node.find('option', attrs={"id": value}).find_all('constant')]:
                log.info(f'- setting variable: {k} = {v}')
                self._global_mod_cache.storeValue(k, v)

    def _select_active_changesets(self):
        self._do_resolve()

        log.info('loading enabled changesets...')
        found = 0
        dropped = 0

        for node in self._modxml_resolved.select('file > changeset'):
            found += 1

            enable_flag = node.get('enable-if-defined', '')
            enable_flag_exclusion = node.get('enable-if-undefined', '')

            if not enable_flag and not enable_flag_exclusion:
                continue

            if enable_flag:
                if enable_flag[0] == '@':
                    enable_flag = enable_flag[1:]

                    if not enable_flag:
                        raise ValueError(f'invalid mod: empty reference in enable-if-defined of changeset {_xml(node)}')
                else:
                    raise ValueError(f'invalid mod: references must start with an "@" in enable-if-defined of changeset {_xml(node)}')

                try:
                    self._global_mod_cache.getStoredValue(enable_flag)
                except KeyError:
                    log.info(f'dropping disabled changeset {_xml(node)}')
                    node.extract()
                    del node
                    dropped += 1
            else:
                if enable_flag_exclusion[0] == '@':
                    enable_flag_exclusion = enable_flag_exclusion[1:]

                    if not enable_flag_exclusion:
                        raise ValueError(f'invalid mod: empty reference in enable-if-undefined of changeset {_xml(node)}')
                else:
                    raise ValueError(f'invalid mod: references must start with an "@" in enable-if-undefined of changeset {_xml(node)}')

                try:
                    self._global_mod_cache.getStoredValue(enable_flag_exclusion)
                    log.info(f'dropping disabled changeset {_xml(node)}')
                    node.extract()
                    del node
                    dropped += 1
                except KeyError:
                    pass  # variable is undefined, keep the changeset

        log.info(f'dropped {dropped} of {found} changesets')

    def _select_files(self):
        self._do_resolve()

        log.info("selecting files...")

        for sel in self._modxml_resolved.select('file > select-file'):
            fid = sel.get('id', None)

            if not fid:
                raise ValueError(f'invalid mod: missing ID on {_xml(sel)}')
            else:
                fid = f'@{fid}'

            if fid in self._files_cache_aliases:
                raise ValueError(f'invalid mod: non-unique ID on {_xml(sel)}')

            # When selecting an XML extract, the spec attribute must be one of
            # EXTRACT_SPEC_NAMES instead of the selected file's binary format name.
            selspec = sel.get('spec', None)
            prompt = sel.get('prompt', None)
            must_exist = False if sel.get('exists', 'true').strip().lower() == 'false' else True

            if not selspec:
                raise ValueError(f'invalid mod: no spec attribute on {_xml(sel)}, use "data" for extracts or new files')
            else:
                if selspec in self.EXTRACT_SPEC_NAMES and not prompt:
                    raise ValueError(f'invalid mod: no or empty prompt on selected extract file {_xml(sel)}')
                elif not prompt:
                    prompt = sel.get('prompt', Spec().by_type(selspec).title)

            # Presets and prompt results may contain absolute paths, as they are
            # consciously defined by the user. Relative paths are relative to the
            # current working directory, not the mod directory.
            if fid in self._file_presets:
                selected_file = self._file_presets[fid]
            else:
                defaultPath = sel.get('default', None)

                if defaultPath:
                    defaultPath = self._target_path / defaultPath
                    defaultPath = defaultPath.relative_to(self._target_path)

                selected_file = self._fileselect_callback(prompt, default=defaultPath)

            self._cache_file(fid, selected_file, selspec, allowAbsolute=True, mustExist=must_exist)

    def _get_validated_path(self, inputFile: str, requiredParent: FilePath = None, allowAbsolute: bool = False, mustExist: bool = True) -> FilePath:
        if not requiredParent and not allowAbsolute:
            raise ValueError(f'bug: one of "requiredParent" and "allowAbsolute" must be defined, got {requiredParent} and {allowAbsolute}')

        inputPath = FilePath(inputFile)

        if inputPath.is_absolute():
            if not allowAbsolute:
                raise ValueError(f'input file must not be specified as absolute path, got {inputPath}')
        else:
            if requiredParent:
                inputPath = requiredParent / inputPath

            inputPath = inputPath.absolute().resolve()

        if not allowAbsolute and requiredParent not in inputPath.parents:
            raise ValueError(f'input file must be below {requiredParent}, got {inputPath}')

        if inputPath.exists() and not inputPath.is_file():
            raise FileExistsError(f'element found at {inputPath} is not a file')

        if mustExist and not inputPath.is_file():
            raise FileNotFoundError(f'input file not found at "{inputPath}"')

        return inputPath

    def _cache_file(self, key: str, inputFile: str, spec: str, requiredParent: FilePath = None,
                    allowAbsolute: bool = False, mustExist: bool = True):
        inputPath = self._get_validated_path(str(inputFile), requiredParent, allowAbsolute, mustExist=mustExist)

        if not mustExist and not FilePath(inputPath).exists():
            with open(inputPath, 'wb') as fd:
                fd.write(b'\x00')

            # NOTE: The placeholder file is necessary because a Parser() needs
            #       an existing file to operate on, and mmap doesn't support empty
            #       files.
            self._placeholder_files.append(FilePath(inputPath).absolute())

        self._files_cache[inputPath] = _FileSelect(inputPath, spec)
        self._files_cache_aliases[key] = inputPath
        self._files_cache_aliases[str(inputPath)] = inputPath
        self._files_cache_aliases[inputFile] = inputPath

    def _get_cached_file(self, key) -> _FileSelect:
        if key not in self._files_cache_aliases:
            raise FileNotFoundError(f'file {key} is not cached, this is probably a bug')

        key = self._files_cache_aliases[key]
        return self._files_cache[key]

    def _get_parser(self, key) -> Parser:
        cache = self._get_cached_file(key)
        absolute = FilePath(cache.name).absolute()
        key = absolute.relative_to(self._target_path)

        if key not in self._parsers:
            log.info(f'loading parser for {key}...')
            self._parsers[key] = Parser(dataFile=absolute, structure=cache.kind)

        return self._parsers[key]

    def _gather_changeset_files(self):
        self._do_resolve()

        log.info("gathering changeset files...")

        for node in self._modxml_resolved.select('file > changeset'):
            target = node.attrs.get('target', None)

            if not target:
                raise ValueError(f'invalid mod: no target on {_xml(node)}')

            if target[0] == '@':
                try:
                    self._get_cached_file(target)
                except FileNotFoundError:
                    raise ValueError(f'invalid mod: undefined reference to "{target}" on {_xml(node)}')
            else:
                must_exist = False if node.get('exists', 'true').strip().lower() == 'false' else True
                self._cache_file(target, target, node.attrs.get('spec', ''),
                                 requiredParent=self._target_path, allowAbsolute=False,
                                 mustExist=must_exist)

    def _seed_caches(self):
        self._do_compile()

        log.info('loading caches...')

        for node in self._modxml_compiled.find_all('seed-cache', recursive=True):
            log.info(f"- setting variable: {node['key']} = {node['value']}")
            self._global_mod_cache.storeValue(node['key'], node['value'])
            node.extract()
            del node

    def _setup_parsers(self):
        # log.info("loading parsers...")
        #
        # for k, v in self._files_cache.items():
        #     log.info(f'loading parser for {k} ({v.name})')
        #     self._parsers[k] = Parser(dataFile=v.name, structure=v.kind)
        pass

    def _close_parsers(self):
        for k, v in self._parsers.items():
            v.close()

        self._parsers = {}

    def _run_preparation_actions(self):
        log.info("running preparation actions...")

        for prep in self._modxml_resolved.select('file > prepare'):
            action = prep['action']

            log.info(f'preparing: {action}')
            log.debug(f'{prep}')

            if action == 'combine-figuren' or action == 'autoselect-figuren':
                fid = prep.get('id', None)

                if not fid:
                    raise ValueError(f'invalid mod: missing ID on {_xml(prep)}')
                else:
                    fid = f'@{fid}'

                if fid in self._files_cache_aliases:
                    raise ValueError(f'invalid mod: non-unique ID on {_xml(prep)}')

                # Presets and prompt results may contain absolute paths, as they are
                # consciously defined by the user. Relative paths are relative to the
                # current working directory, not the mod directory.
                if fid in self._file_presets:
                    combined_file = self._file_presets[fid]
                    self._cache_file(fid, combined_file, 'figuren', allowAbsolute=True, mustExist=True)
                else:
                    if action == 'combine-figuren':
                        # files are expected to be in the current working directory
                        combined_file = combine_figuren_data_files()
                    elif action == 'autoselect-figuren':
                        combined_file = autoselect_figuren_data_files()

                    self._cache_file(fid, combined_file, 'figuren', allowAbsolute=True, mustExist=True)
            else:
                raise ValueError(f'invalid mod: unknown preparation action "{action}" ({prep})')

    def _handle_changeset(self, changeset):
        log.info("handling changeset...")
        self._parse_changeset_paths(changeset)

        target = changeset['target']
        parser = self._get_parser(target)
        modcache = ModCache(tree=parser.tree,
                            offsets=self._global_mod_cache._offsets,
                            values=self._global_mod_cache._values)

        log.info("applying changes...")
        for node in changeset.select('change'):
            self._actions_count += 1
            self._handle_change(node, parser, modcache)

    def _cache_selected_path(self, reference: str, logNode: BeautifulSoup, parser: Parser) -> None:
        if reference[0] == '@':
            ref = reference.split('/')[0]
            path = '/'.join(reference.split('/')[1:])

            if ref in self._current_selects_delayed_ref:
                log.info(f'caching delayed path: {reference}')
                self._current_selects_delayed_parse[ref] += [(reference, logNode)]
                return

            if ref not in self._current_selects or not self._current_selects[ref]:
                raise ValueError(f'invalid mod: undefined reference to "{ref}" ({reference}) in {_xml(logNode)}')

            if path:
                # path = path
                parents = self._current_selects[ref]
            else:
                return  # ok, reference is cached
        else:
            path = reference
            parents = [parser.parsedRoot]

        log.info(f'caching path: {reference}')

        for p in parents:
            parser.parsePath(path, p)
            # parser.tree.printTree()  # DEBUG

        self._current_selects[reference] = reference

    def _object_paths_finder(self, node: BeautifulSoup) -> bool:
        if node.name in ('select', 'change') \
                or (node.name == 'load-palette' and node.get('id-source', None)) \
                or (node.name == 'data' and node.get('x-increase-from', None)) \
                or (node.name == 'clipboard'):
            return True
        else:
            return False

    def _parse_changeset_paths(self, changeset):
        log.info("parsing changeset paths...")

        target = changeset['target']
        parser = self._get_parser(target)

        self._current_selects: Dict[str, [List[ParsedNode], str]] = {}
        self._current_selects_delayed_ref: List[str] = []
        self._current_selects_delayed_parse: Dict[str, Tuple[str, BeautifulSoup]] = defaultdict(list)

        for node in changeset.find_all(self._object_paths_finder):
            if node.name == 'select':
                paths = [node['select']]

                if node['select'][0] == '@':
                    raise ModError(f'references are not allowed in <select> statements, got {node["select"]} in {_xml(node)}')
            elif node.name == 'change':
                if node['action'] == 'copy':
                    if not node.get('id', None):
                        node['id'] = _make_id('clipboard-slot')

                    paths = [node['select']]
                else:
                    paths = [node['select']]
            elif node.name == 'load-palette':
                paths = [node['id-source']]
            elif node.name == 'data':
                paths = [node['x-increase-from']]
            elif node.name == 'clipboard':
                if ref := node.get('id', None):
                    ref = f'@{node["id"]}'
                    paths = [ref]
                    self._current_selects_delayed_ref.append(ref)
                else:
                    paths = []

            for p in paths:
                self._cache_selected_path(p, node, parser)

            if node.name == 'select':
                # parse and store parsed nodes only in <select> statements!
                self._current_selects[f'@{node["id"]}'] = parser.tree.getPaths(paths[0])

    def _fail_action(self, action, message):
        self._failed_actions_count += 1
        log.warning(f'failed action {action}: {message}')

    def _get_change_targets(self, change, parser) -> ['ParsedNode', List['ParsedNode']]:
        action = change['action']
        target_paths = [self._current_selects.get(change['select'], None)]

        targets = []

        for path in target_paths:
            if isinstance(path, list):
                target = path  # already parsed
                log.debug("paths are already parsed: %s", path)
            elif path:
                log.debug("paths have to be parsed: %s", path)

                if path[0] == '@':
                    ref = path.split('/')[0]
                    path = '/'.join(path.split('/')[1:])
                    parent = self._current_selects[ref][0]
                else:
                    parent = parser.tree

                log.debug("- path: %s | parent: %s", path, parent)
                # log.debug("- parent xml: %s", list(parent.bakeXml('')))
                target = parent.getPaths(path)
                log.debug("- got: %s", target)
            else:
                log.debug("no paths to be parsed: %s", path)
                target = []

            if target:
                targets += [target]
            if not target and action != 'remove':
                self._fail_action(change, "no target selected")
                raise Exception(f'no target selected in {_xml(change)}')

        if len(targets[0]) != 1 and (
            action in ['insert-before', 'insert-after', 'append', 'prepend', 'copy']
            or action.startswith('counter:')
            or action.startswith('clipboard:')
            or action.startswith('list:')
        ):
            self._fail_action(change, "more than one target selected but this action only works with single items")
            return

        if action in ('remove', 'replace'):
            target = flatten(targets)
        else:
            target = target[0]

        return target

    def _handle_inserts(self, action: str, target: ParsedNode, change: BeautifulSoup, modCache: ModCache) -> [None, List[ParsedNode]]:
        if action not in ('insert-before', 'insert-after', 'prepend', 'append'):
            return None

        def nodes(offset):
            return self._build_change_tree(change, modCache, offset).children

        with ParseMode(parseStatus=ParseMode.State.Disable, dynamicGroups=ParseMode.State.Enable):
            if action == 'insert-before':
                added = nodes(target.getNewLocalStartOffset())
                target.insertBefore(added)
            elif action == 'prepend':
                added = nodes(target.getNewLocalStartOffset())
                target.prependNodes(added)
            elif action == 'insert-after':
                added = nodes(target.getNewLocalStartOffset() + target.getFinalFullLength())
                target.insertAfter(added)
            elif action == 'append':
                added = nodes(target.getNewLocalStartOffset() + target.getFinalFullLength())
                target.appendNodes(added)
            else:
                raise ModError(f'unknown action: {action}')

        return added

    def _build_change_tree(self, change: BeautifulSoup, modCache: ModCache, baseOffset: int) -> ParsedNode:
        change_xml = BeautifulSoup('<file type="xml"></file>', 'xml')

        # insert clipboard
        for node in change.find_all('clipboard'):
            log.info(f"inserting contents of clipboard slot {node['ref']}")
            clipboard_xml = self._clipboard[node['ref']]
            clipboard_xml = str(clipboard_xml)

            pasted_id = node.get('id', _make_id('clipboard-pasted'))
            clipboard_xml = BeautifulSoup(clipboard_xml, 'xml').clone
            children = list(clipboard_xml.children)

            if len(children) > 1:
                raise ModError('clipboard can only hold single entries, got more')
            elif not children:
                pass
            else:
                pasted_node = children[0]
                pasted_node['x-clipboard-is-pasted'] = 'true'
                pasted_node['x-clipboard-pasted-id'] = pasted_id
                node.insert_after(pasted_node)

            node.extract()

        # handle actual data
        valid_children = change.find_all(['head', 'data', 'group'], recursive=False)

        for i in valid_children:
            change_xml.file.append(copy.copy(i))

        oldBaseOffset = modCache.baseOffset
        oldSelects = modCache.currentSelects

        modCache.baseOffset = baseOffset
        modCache.currentSelects = self._current_selects

        change_tree = NewDataFactory.makeExtractedNodes(str(change_xml), modCache=modCache)

        modCache.baseOffset = oldBaseOffset
        modCache.currentSelects = oldSelects

        return change_tree

    def _handle_change(self, change: BeautifulSoup, parser: Parser, modCache: ModCache):
        log.info(f'applying change to {FilePath(parser._dataFile).relative_to(self._target_path)}: {change["action"]}')
        target = self._get_change_targets(change, parser)
        action = change['action']

        if inserted := self._handle_inserts(action, target, change, modCache):
            for node in change.find_all(attrs={'x-clipboard-is-pasted': 'true'}, recursive=True):
                pasted_id = node.get('x-clipboard-pasted-id', None)

                if not pasted_id:
                    continue

                ident = f'@{pasted_id}'
                self._current_selects[ident] = inserted
                del self._current_selects_delayed_ref[self._current_selects_delayed_ref.index(ident)]

                for path, logNode in self._current_selects_delayed_parse[ident]:
                    self._cache_selected_path(path, logNode, parser)
        elif action == 'copy':
            log.debug(f'copying {_xml(change)} to clipboard slot "{change["id"]}"')
            parser.parseFully(target)
            self._clipboard[change['id']] = BeautifulSoup(f'<clone>{"".join(list(target.bakeXml("")))}</clone>', 'xml').clone
        elif action.startswith('list:'):
            list_types = [
                'c_cstring_list', 'c_pstring_list', 'c_id_string_list',
                'c_cstring_list_csv', 'c_color_list']

            if target.specNode.typeStr not in list_types:
                raise InvalidDataTypeError(f'cannot use list actions with spec nodes of type "{target.specNode.typeStr}" '
                                           f'in {_xml(change)}; allowed types are {", ".join(list_types)}')

            target_list = target.decodeToPy()
            transcoder = get_transcoder(target.specNode.typeStr, raiseIfMissing=True)

            if at_value := change.get('at-value', None):
                try:
                    at_value_py = transcoder.xml2py(at_value, specNode=None)[0]
                except (TypeError, ValueError, IndexError):
                    raise InvalidDataTypeError(f'selector value "{at_value}" cannot be parsed as "{target.specNode.typeStr}" in {_xml(change)}')

                log.debug(f"converted value '{at_value}' to: {at_value_py}")

                try:
                    at_index = target_list.index(at_value_py)
                    log.debug(f'found index of {at_value} at {at_index}')
                except ValueError:
                    raise ModError(f'value "{at_value}" is not in data selected in {_xml(change)}')
            elif at_index := change.get('at-index', None):
                try:
                    at_index = int(at_index)
                except ValueError:
                    raise InvalidDataTypeError(f'index value "{at_index}" is not an integer in {_xml(change)}')

                try:
                    at_value = target_list[at_index]
                    log.debug(f"got value at {at_index}: {at_value}")
                except IndexError:
                    raise ModError(f'index value "{at_index}" is out of range for data selected in {_xml(change)}')
            else:
                raise ModError(f'no index or value selected in {_xml(change)}')

            if action != 'list:remove':
                new_value = change['value']

                for i in change.get('x-use-values', '').strip().split(' '):
                    if not i:
                        continue

                    try:
                        variable = self._global_mod_cache.getStoredValue(i)
                    except Exception:
                        raise ModError(f'cannot use value "{i}" which is not stored; check for use before definition in {_xml(change)}')

                    new_value = new_value.replace(f'@{i}@', str(variable))

                try:
                    new_value_py = transcoder.xml2py(new_value, specNode=None)[0]
                    log.debug(f'converted new value {new_value} to: {new_value_py}')
                except KeyError:
                    raise ModError(f'no new value defined in {_xml(change)}')
                except (TypeError, ValueError, IndexError):
                    raise InvalidDataTypeError(f'new value "{new_value}" cannot be parsed as "{target.specNode.typeStr}" in {_xml(change)}')

            if action == 'list:insert-after':
                if at_index == -1:
                    target_list.append(new_value_py)
                else:
                    target_list.insert(at_index + 1, new_value_py)
            elif action == 'list:insert-before':
                target_list.insert(at_index, new_value_py)
            elif action == 'list:replace':
                target_list[at_index] = new_value_py
            elif action == 'list:remove':
                del target_list[at_index]

            target.encodeFromPy(target_list)
        elif action.startswith('counter:'):
            if action != 'counter:store':
                value = change['value']

                for i in change.get('x-use-values', '').strip().split(' '):
                    if not i:
                        continue

                    try:
                        variable = self._global_mod_cache.getStoredValue(i)
                    except Exception:
                        raise ModError(f'cannot use value "{i}" which is not stored; check for use before definition in {_xml(change)}')

                    value = value.replace(f'@{i}@', str(variable))

                if '.' in value:
                    value = float(value)
                else:
                    value = int(value)

                if action == 'counter:increase':
                    value = target.decodeToPy() + value
                elif action == 'counter:decrease':
                    value = target.decodeToPy() - value
                elif action == 'counter:set':
                    pass
                else:
                    raise ModError(f'unknown counter action "{action}" in {_xml(change)}')

                target.encodeFromPy(value)
            else:
                value = target.decodeToPy()

            if store_id := change.get('id', None):
                self._global_mod_cache.storeValue(store_id, value)
        elif action == 'replace':
            with ParseMode(parseStatus=ParseMode.State.Disable, dynamicGroups=ParseMode.State.Enable):
                change_nodes = self._build_change_tree(
                    change, modCache, target[0].getNewLocalStartOffset())
                target[0].insertBefore(change_nodes.children)

                for i in target:
                    i.delete()
        elif action == 'remove':
            with ParseMode(parseStatus=ParseMode.State.Disable, dynamicGroups=ParseMode.State.Enable):
                for i in target:
                    i.delete()

        if action in ['counter:store', 'copy']:
            pass  # nothing to do because the file was not changed here
        else:
            self._modified_targets[FilePath(parser._dataFile).absolute()] = True

    def _save_files(self, dry_run: bool):
        for key, parser in self._parsers.items():
            if not self._modified_targets[FilePath(key).absolute()]:
                log.info(f"skipping unchanged input file {key}...")

                if FilePath(key).absolute() in self._placeholder_files:
                    FilePath(key).unlink()

                continue

            # NOTE if the source file is a symbolic link, new files should end
            #      up next to the link itself and not next to its target
            filepath = FilePath(parser._dataFile).absolute()

            if self._save_extracted:
                suffix = '.xml'
            elif filepath.suffix == '.xml':
                suffix = '.bin'
            else:
                suffix = filepath.suffix

            if self._save_separately:
                filepath = filepath.parent / f'{filepath.stem}.modded{suffix}'
            else:
                filepath = filepath.parent / f'{filepath.stem}{suffix}'

            if self._make_backups:
                if dry_run:
                    log.info(f'dry run: would backup existing files at {filepath}')
                else:
                    backup_existing_file(filepath)
            else:
                log.warn("skipping backups due to configuration")

            if self._save_separately or self._save_extracted:
                if dry_run:
                    log.info(f'dry run: would save {key} as {filepath.name}...')
                else:
                    log.info(f'saving {key} as {filepath.name}...')
            else:
                if dry_run:
                    log.info(f'dry run: would save {key}...')
                else:
                    log.info(f'saving {key}...')

            if not dry_run:
                temp = tempfile.mkstemp(dir=filepath.parent)
                os.close(temp[0])

                if self._save_extracted:
                    parser.tree.bakeXmlToFile(temp[1], overwrite=True)
                else:
                    parser.tree.bakeToFile(temp[1], overwrite=True)

                FilePath(temp[1]).rename(filepath)
