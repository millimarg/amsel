# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import logging as log
import re
import itertools
import configparser
# import math
import enum
from textwrap import dedent
from enum import auto
# from enum import STRICT  # since py 3.11
from functools import cached_property
from functools import cache
from collections import defaultdict
from pathlib import Path as FilePath
from typing import List
from typing import Tuple

import png
# import apng
from bs4 import BeautifulSoup

from .decoders import DecColorList, DecImageHeader
from .tools import log_fatal
from .tools import chunks
from .tools import flatten
from .tools import random_str


class Palette:
    def __init__(self, uid=0, name='', entries=None):
        self.uid = uid
        self.name = name

        if entries:
            self.entries = entries
        else:
            self.entries = [DecColorList.Data(0, 0, 0, 0) for x in range(0, 256)]

    def __contains__(self, item):
        return item in self.entries

    def __getitem__(self, key):
        if isinstance(key, int):
            raise LookupError('access palette entries through Palette.entries[index], use Palette[color] for looking up values')

        if ret := next((x for x in self.entries if key == x), None):
            return ret
        raise LookupError(f'palette has no entry "{key}"')

    def from_file(self, source):
        if isinstance(source, FilePath):
            source = str(source)

        if source.endswith('.png'):
            return self.from_png(source)
        elif source.endswith('.lst'):
            return self.from_plain_extract(source)
        elif source.endswith('.xml'):
            return self.from_extract(source)
        else:
            return self.from_gpl(source)

    def from_png(self, source, keepAlpha=False):
        self.name = source + ' (generated)'

        reader = png.Reader(filename=source)
        spng = reader.asRGBA8()  # rows of RGBA, 8bit (converted if necessary)
        rows = spng[2]

        color_entries = 0
        for y, row in enumerate(rows):
            for x, rgba in enumerate(chunks(row, 4)):
                if color_entries >= 256:
                    log.error(f'PNG image {source} has more than 256 colors')
                    log.info('Colors can be reduced using Imagemagick:')
                    log.info(f'mogrify -channel RGBA -matte -colorspace transparent -colors 256 "{source}"')
                    return self

                if not keepAlpha:
                    rgba = list(rgba[:3]) + [0]

                if rgba not in self.entries:
                    self.entries[color_entries].from_tup(rgba)
                    color_entries += 1

        return self

    def from_plain_extract(self, source: str, inputIsFile=True) -> 'Palette':
        if inputIsFile:
            with open(source, 'r') as fd:
                data = fd.read()
        else:
            data = source

        self.entries = DecColorList.xml2py(data, None)
        self.uid = 0
        self.name = source

        return self

    def from_extract(self, source):
        with open(source, 'r') as fd:
            sp = BeautifulSoup(fd.read(), 'xml')
        sp.ext.from_xml(force=True)

        found_pals = sp.select('#palette-entries')
        if len(found_pals) > 1:
            log.warning(f'found {len(found_pals)} palettes, loading only the first one')

        for uid_node in sp.select('#palette-id'):
            uid_id = 'palette-id-' + random_str(8)
            uid_node['id'] = uid_id
            uid = uid_node.ext.data

            if name := uid_node.select_one(':scope + #palette-name'):
                name = name.ext.data
            elif name := uid_node.parent.select_one(':scope #palette-name'):
                name = name.ext.data
            else:
                name = source

            if data := uid_node.select_one(':scope + #palette-entries'):
                data = data.ext.data
            elif data := uid_node.parent.select_one(':scope #palette-entries'):
                data = data.ext.data
            else:
                log.error(f'no palette found in {source} (expected #palette-entries next to or below #palette-id)')
                return self

            # we can't load more than one palette...
            break
        else:
            if data := sp.select_one('#palette-entries'):
                data = data.ext.data
                name = source
                uid = 0
            else:
                log.error(f'no palette found in {source} (expected #palette-entries anywhere)')
                return self

        self.entries = data
        self.name = name
        self.uid = uid
        log.info(f'loaded {len(self.entries)} entries for {self.name} with ID {self.uid}')

        return self

    def from_gpl(self, source):
        with open(source, 'r') as fd:
            entry_index = 0
            for i, line in enumerate(fd):
                # --- Header:
                # GIMP Palette
                # Name: test
                # Columns: 16
                # #
                # --- Body:
                # r g b
                # r g b <tab> color name

                if i == 0:
                    if not line.startswith('GIMP Palette'):
                        log_fatal(ValueError, f'missing magic header "GIMP Palette" in {source}')
                    else:
                        continue

                if line[0] == '#':
                    # very first line must not be a comment
                    continue

                if i == 1:
                    if line.startswith('Name:'):
                        self.name = line[5:].strip()
                        continue
                    else:
                        self.name = source

                if i == 2:
                    if line.startswith('Columns:'):
                        continue

                if entry_index > 255:
                    log.error(f'palette truncated: more than 256 entries in {source}')
                    break

                values = re.sub(r'[\t ]+', ' ', line.strip()).split(' ')
                self.entries[entry_index].r = int(values[0])
                self.entries[entry_index].g = int(values[1])
                self.entries[entry_index].b = int(values[2])
                entry_index += 1

        return self

    def to_gpl(self, outfile=None):
        lines = [''] * (len(self.entries) + 4)

        lines[0] = 'GIMP Palette'
        lines[1] = f'Name: {self.name}'
        lines[2] = 'Columns: 16'
        lines[3] = '#'

        for i, color in enumerate(self.entries):
            lines[3 + i + 1] = f'{color.rgb_str_pretty}\t{color}'

        if outfile:
            with open(outfile, 'w') as fd:
                fd.write('\n'.join(lines))

        return lines

    def to_plain_extract(self) -> str:
        return DecColorList.py2xml(self.entries, specNode=None)


# Image formats:
#
# Figuren.dat:
# BSH_ALPHA
# BSH_RGBAV
# BSH_RGBAZ
# BSH_RGBH
# BSH_RGBV
# BSH_RGBZ
#
# Anlagen.dat:
# BSH_ALPHA
# BSH_RGBAH
# BSH_RGBAV
# BSH_RGBAZ
# BSH_RGBH
# BSH_RGBV
# BSH_RGBWZ
# BSH_RGBZ
# BSH_RGBZM
#
#


class SingleFeatures(enum.Enum):
    BlackWhite = auto()
    ShadowOnly = auto()
    PlainPixelList = auto()

    @classmethod
    def isVerbose(self, feature):
        return False


# class CombiFeatures(enum.Flag, boundary=enum.STRICT):  # since py 3.11
class CombiFeatures(enum.Flag):
    Color = auto()
    Alpha = auto()
    ZBuffer = auto()
    WBuffer = auto()
    Verbose = auto()


Formats = {
    "BSH_RGBAZ": CombiFeatures.Color | CombiFeatures.Alpha | CombiFeatures.ZBuffer,
    "BSH_RGBAV": CombiFeatures.Color | CombiFeatures.Alpha,
    "BSH_RGBAH": CombiFeatures.Color | CombiFeatures.Alpha,
    "BSH_RGBWZ": CombiFeatures.Color |                       CombiFeatures.WBuffer,
    "BSH_RGBZ":  CombiFeatures.Color |                       CombiFeatures.ZBuffer,
    "BSH_RGBV":  CombiFeatures.Color,
    "BSH_RGBH":  CombiFeatures.Color,
    "BSH_ALPHA":    SingleFeatures.ShadowOnly,
    "BSH_OUTLN":    SingleFeatures.BlackWhite,
    "BSH_MASK":     SingleFeatures.BlackWhite,                                                              # unclear
    "BSH_RGBW":  CombiFeatures.Color,                                                                       # unclear
    "BSH_RGBZM": CombiFeatures.Color |                       CombiFeatures.ZBuffer,                         # unclear
    "BSH_RGB":   CombiFeatures.Color,                                                                       # unclear
    "TEX_RGB":      SingleFeatures.PlainPixelList,
    "TEX_RGBA":  CombiFeatures.Color | CombiFeatures.Alpha |                       CombiFeatures.Verbose,   # unclear
}

AlphaValueByLevel = {
    0: 63,   # 75% transparency
    1: 126,  # 50%
    2: 189,  # 25%
    3: 255,  # 0% transparency = opaque
}

AlphaLevelByValue = {}
AlphaLevelByValue |= {x: 0 for x in range(0, 63 + 1)}
AlphaLevelByValue |= {x: 1 for x in range(64, 126 + 1)}
AlphaLevelByValue |= {x: 2 for x in range(127, 189 + 1)}
AlphaLevelByValue |= {x: 3 for x in range(190, 255 + 1)}

TransparentPixel = (0, 0, 0, 0)

ShadowPixel = (255, 255, 255, 100)  # alpha value of 100 seems to be right

BlackWhitePixels = defaultdict(lambda: (255, 0, 255, 255))  # invalid (magenta)
BlackWhitePixels[1] = (0, 0, 0, 255)                        # black
BlackWhitePixels[2] = (255, 255, 255, 255)                  # white
BlackWhitePixels[3] = (50, 50, 50, 255)                     # drop shadow (TODO should have alpha=127)

BlackWhitePalette = defaultdict(lambda: 4)                # invalid
BlackWhitePalette[(0, 0, 0)] = 1                          # black
BlackWhitePalette[(255, 255, 255)] = 2                    # white
BlackWhitePalette[(50, 50, 50)] = 3                       # drop shadow


class Image:
    _INI_LINES_SPLIT = re.compile(r'(.*? fe[ ]?)')
    _INI_TEMPLATE = dedent('''\
            [image]
            name = {name}
            head = {head}
            body = {body}
            palette_id = {pal_id:d}
            palette = {pal}

        ''')

    # Alpha value of a fully non-transparent pixel.
    _OPAQUE_ALPHA = 255

    # Groups must not be longer than 63 pixels if transparency is enabled
    # because the length must be encoded in 6 bits.
    _MAX_PIXELS_PER_GROUP_ALPHA = 63

    # The number of consecutive color pixels is encoded in one byte, of which the
    # values 254 and 255 are reserved. That means longer lines must be split into
    # chunks of 253 bytes.
    _MAX_PIXELS_PER_GROUP = 253

    def __init__(self, head=None, data=None, palette=None, name=None):
        self.head = head if head else DecImageHeader.Data()
        self.data = data if data else bytearray()
        self.name = str(name) if name else ''

        if isinstance(palette, Palette):
            self.palette = palette
        else:
            self.palette = Palette()
            if palette:
                self.palette.from_file(palette)

    @property
    def width(self):
        return self.head.width

    @property
    def height(self):
        return self.head.height

    @width.setter
    def width(self, new):
        self.head.width = new

    @height.setter
    def height(self, new):
        self.head.height = new

    def from_png(self, source, palette=None, targetFormatName='BSH_RGBAV') -> 'Image':
        if isinstance(source, FilePath):
            source = str(source)

        targetFormat = Formats.get(targetFormatName)
        self.head.img_format = targetFormatName
        self.name = source

        reader = png.Reader(filename=source)
        spng = reader.asRGBA8()  # rows of RGBA, 8bit (converted if necessary)

        self.width = spng[0]
        self.height = spng[1]
        rows = spng[2]
        # info = spng[3]

        if isinstance(palette, Palette):
            # always pass a valid palette (with valid ID!) from mods
            self.palette = palette
        elif palette:
            self.palette.from_file(palette)
        else:
            self.palette.from_png(source)

        self.head.palette_id = self.palette.uid

        prepared = [[] for x in range(0, self.height)]

        if isinstance(targetFormat, CombiFeatures) and targetFormat & CombiFeatures.Verbose or \
                isinstance(targetFormat, SingleFeatures) and SingleFeatures.isVerbose(targetFormat):
            verbose = True
        else:
            verbose = False

        for y, row in enumerate(rows):
            if isinstance(targetFormat, SingleFeatures) and targetFormat == SingleFeatures.PlainPixelList:
                # Some image formats don't use fancy stuff like line end markers, and simply
                # store a list of color references.

                for pixel in chunks(row, 4):
                    prepared[y] += [self.palette.entries.index(list(pixel)[:3] + [0])]

                continue

            # group by "pixel is transparent?"
            groups: List[Tuple[bool, List[bytearray]]] = itertools.groupby(chunks(row, 4), key=lambda x: x[3] == 0)
            # print([f'{x}: {[z for z in y]}' for x, y in groups])

            for i, group in enumerate(groups):
                isTransparent, pixels = group
                pixels = list(pixels)
                pixelCount = len(pixels)

                if i == 0 and not isTransparent:  # line starts opaque
                    prepared[y].append(0)  # "line starts with zero transparent pixels"
                elif isTransparent:  # transparent
                    if pixelCount == self.width and not verbose:
                        # Do nothing when the whole line is transparent and the image
                        # format supports elided transparency.
                        break
                    elif pixelCount > self._MAX_PIXELS_PER_GROUP:
                        # len(x) transparent pixels, followed by 0 color pixels,
                        # then drop the last "0 color pixels" indicator
                        prepared[y] += flatten([[len(x), 0] for x in chunks(bytes(pixelCount), self._MAX_PIXELS_PER_GROUP)])[:-1]
                    else:
                        prepared[y] += [pixelCount]  # "pixelCount" transparent pixels

                    continue

                if isinstance(targetFormat, CombiFeatures) and targetFormat & CombiFeatures.Alpha:
                    chunkSize = self._MAX_PIXELS_PER_GROUP_ALPHA
                    pixelsByAlpha: List[Tuple[int, List[bytearray]]] = itertools.groupby(pixels, key=lambda x: x[3])
                else:
                    chunkSize = self._MAX_PIXELS_PER_GROUP
                    pixelsByAlpha: List[Tuple[int, List[bytearray]]] = [(self._OPAQUE_ALPHA, pixels)]

                for alphaValue, alphaPixels in pixelsByAlpha:
                    for pixelChunk in chunks(list(alphaPixels), chunkSize):
                        localPixelCount = len(pixelChunk)

                        if isinstance(targetFormat, CombiFeatures) and targetFormat & CombiFeatures.Alpha:
                            alphaLevel = AlphaLevelByValue[alphaValue]
                            encodedAlpha = alphaLevel << 6
                            encodedCount = encodedAlpha + localPixelCount
                        else:
                            encodedCount = localPixelCount

                        prepared[y] += [encodedCount]

                        for pixel in pixelChunk:
                            if isinstance(targetFormat, SingleFeatures):
                                if targetFormat == SingleFeatures.BlackWhite:
                                    prepared[y] += [BlackWhitePalette[list(pixel)[:3]]]
                                elif targetFormat == SingleFeatures.ShadowOnly:
                                    pass  # shadow pixels are only implied by the count
                            else:
                                if targetFormat & CombiFeatures.ZBuffer:
                                    prepared[y] += [0]  # TODO not implemented yet
                                elif targetFormat & CombiFeatures.WBuffer:
                                    prepared[y] += [0]  # TODO not implemented yet

                                if targetFormat & CombiFeatures.Color:
                                    prepared[y] += [self.palette.entries.index(list(pixel)[:3] + [0])]

                        prepared[y] += [0]  # "zero transparent pixels follow this chunk"

                prepared[y] = prepared[y][:-1]  # drop the last state byte

            if isTransparent:  # ended with transparency
                if verbose:
                    prepared[y] += [0]  # "line ends with zero regular pixels"
                else:
                    prepared[y] = prepared[y][:-1]  # drop last state byte

            prepared[y].append(254)  # line-end marker

        if not isinstance(targetFormat, SingleFeatures) or not targetFormat == SingleFeatures.PlainPixelList:
            prepared[-1][-1] = 255  # replace last line-end marker with EOF marker

        self.data = bytearray(flatten(prepared))

        self.head.data_length = len(self.data)

        return self

    def to_png(self, outfile):
        self.get_png().save(outfile)

    class BytesReader:
        def __init__(self, data: bytes):
            self.data = memoryview(data)
            self.pos = 0

        def read(self) -> memoryview:
            if self.pos < self.length:
                self.pos += 1
                return self.data[self.pos - 1]
            else:
                return None

        @cached_property
        def length(self):
            return len(self.data)

        def __len__(self):
            return self.length

    @cache
    def alpha_level_to_byte(self, level: int) -> int:
        pass

    @cache
    def alpha_byte_to_level(self, byte: int) -> int:
        pass

    def get_png(self) -> png.Image:
        rows = [[] for x in range(0, self.height)]
        data = self.BytesReader(self.data)
        imgFormat = Formats.get(self.head.img_format, None)

        if not imgFormat:
            raise Exception()  # TODO

        if len(data) != self.head.data_length and self.head.data_length > 0:
            log.warn(f"got {len(data)} bytes of image data, but header reports {self.head.data_length} bytes for '{self.name}'")

        currentRow = 0
        while True:
            state = data.read()

            if state is None:
                break

            if isinstance(imgFormat, SingleFeatures) and imgFormat == SingleFeatures.PlainPixelList:
                # Some image formats don't use fancy stuff like line end markers, and simply
                # store a list of color references.

                color = self.palette.entries[state]
                rows[currentRow] += list(color.rgb_tup) + [255]

                if int(len(rows[currentRow]) / 4) == self.width:
                    currentRow += 1

                if currentRow >= self.height:
                    break

                continue

            if state == 254:  # line-end marker
                currentRow += 1
                continue
            elif state == 255:  # EOF marker
                if data.pos != len(data):
                    log.warn(f'not all image data consumed: {data.pos} of {len(data)} bytes')
                break
            else:  # number of transparent pixels
                rows[currentRow] += list(TransparentPixel) * state

            pixelCount = data.read()

            if isinstance(imgFormat, CombiFeatures) and imgFormat & CombiFeatures.Alpha:
                alphaLevel = (pixelCount >> 6) & ((1 << 2) - 1)  # lowest 2 bits (last)
                pixelCount = (pixelCount >> 0) & ((1 << 6) - 1)  # highest 6 bits (first)
            else:
                alphaLevel = 3  # opaque

            for i in range(0, pixelCount):
                if isinstance(imgFormat, SingleFeatures):
                    if imgFormat == SingleFeatures.BlackWhite:
                        rows[currentRow] += BlackWhitePixels[data.read()]
                    elif imgFormat == SingleFeatures.ShadowOnly:
                        rows[currentRow] += list(ShadowPixel)
                else:
                    if imgFormat & CombiFeatures.ZBuffer:
                        data.read()  # TODO not implemented yet
                    elif imgFormat & CombiFeatures.WBuffer:
                        data.read()  # TODO not implemented yet

                    if imgFormat & CombiFeatures.Color:
                        colorEntry = data.read()
                        color = self.palette.entries[colorEntry]

                        rows[currentRow] += color.rgb_tup
                        rows[currentRow] += [AlphaValueByLevel[alphaLevel]]

        # fill incomplete rows with transparency
        rows = [x + list(TransparentPixel) * (self.width - int(len(x) / 4)) for x in rows]

        # list of rows of r, g, b, a (flat)
        return png.from_array(rows, 'RGBA')

    def to_ini(self, outfile=None, getBytes=False) -> str:
        imgFormat = Formats.get(self.head.img_format)
        hexlified = self.data.hex(bytes_per_sep=1, sep=' ')

        if (isinstance(imgFormat, SingleFeatures) and imgFormat == SingleFeatures.PlainPixelList) or \
                len(self.data) > 256 * 256:
            bodystr = '\n\t'.join([x.strip() for x in chunks(hexlified, 120)])
        else:
            bodystr = '\n\t'.join([x.strip() for x in self._INI_LINES_SPLIT.split(hexlified) if x.strip()])

        formatted = self._INI_TEMPLATE.format(
            name=self.name,
            head=self.head.to_xml(),
            body=bodystr,
            pal_id=self.palette.uid,
            pal=self.palette.to_plain_extract()
        )

        if outfile:
            with open(outfile, 'w') as fd:
                fd.write(formatted)

        if getBytes:
            return bytes(formatted, 'utf8')
        else:
            return formatted

    def from_ini(self, sourceFile: str) -> 'Image':
        ini = configparser.ConfigParser()
        ini.read(sourceFile)
        image = ini['image']

        self.name = image['name']
        self.head = DecImageHeader.xml2py(image['head'], specNode=None)
        self.data = bytes.fromhex(image['body'])
        self.palette = Palette(image['palette_id'], '')
        self.palette.from_plain_extract(image['palette'], inputIsFile=False)

        return self


class Animation:
    def __init__(self, images: List[Image]):
        self._images = images
        self._import()

    def _import(self):
        self.width = max([x.width for x in self._images])
        self.height = max([x.height for x in self._images])

        for img in self._images:
            offset_x = 0  # head.origin_x
            # TODO port implementation from libaf-legacy

    def to_png(self, outfile):
        raise NotImplementedError()


class Spritesheet:
    def __init__(self, images: List[Image]):
        self._images = images

    def to_png(self, outfile, columns=8):
        raise NotImplementedError()
