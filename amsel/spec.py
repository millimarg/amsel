# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import re
import os
import enum
import math
import textwrap
from pathlib import Path
from collections import namedtuple
from functools import cache
from functools import cached_property
from typing import List

import json
import xmltodict

from anytree import AnyNode
from anytree import RenderTree
from anytree.importer import DictImporter
from bs4 import BeautifulSoup

from .path import Path as SpecPath
from .tools import DataPaths
from .tools import log_fatal
from .tools import random_str
from .tools import resolved_xml_templ
from .tools import grouped


class SpecError(SyntaxError):
    """ Base class for errors caused by broken spec files. """
    pass


class EmptySpecError(SpecError):
    pass


class UnknownBlockTypeError(SpecError):
    pass


class UnknownSpecTypeError(SpecError):
    pass


class InvalidCountError(SpecError):
    pass


class InvalidLengthError(SpecError):
    pass


class InvalidGroupMemberError(SpecError):
    pass


class InvalidMagicError(SpecError):
    pass


class InvalidRootParentError(SpecError):
    pass


class VariableGroupLengthError(SpecError):
    pass


class SpecNode(AnyNode):
    Interval = namedtuple('Interval', 'min max')

    class BlockType(enum.Enum):
        Short = 1
        Long = 2

    def __init__(self, parent=None, children=None, **kwargs):
        super(SpecNode, self).__init__(parent=parent, children=children, **kwargs)

    def __repr__(self):
        return str(self)

    @cache
    @staticmethod
    def parseCount(count: str) -> Interval:
        if not count:
            countMin = 1
            countMax = 1
        elif count == '?':
            countMin = 0
            countMax = 1
        elif count == '+':
            countMin = 1
            countMax = math.inf
        elif count == '*':
            countMin = 0
            countMax = math.inf
        else:
            try:
                count = int(count)
                countMin = count
                countMax = count
            except ValueError:
                raise InvalidCountError(f"invalid count '{count}'")

        return SpecNode.Interval._make([countMin, countMax])

    @cache
    @staticmethod
    def parseLength(length: str) -> int:
        if length == '*':
            length = math.inf
        else:
            try:
                length = int(length)
            except ValueError:
                raise InvalidLengthError(f"invalid length '{length}'")

        return length

    @cached_property
    def isFileRoot(self) -> bool:
        return self.kind == 'file' or self.kind == 'spec' or self.kind == 'root'

    @cached_property
    def isData(self) -> bool:
        return self.kind == 'data'

    @cached_property
    def isGroup(self) -> bool:
        return self.kind == 'group'

    @cached_property
    def isHead(self) -> bool:
        return self.kind == 'head'

    @cached_property
    def kind(self) -> str:
        return getattr(self, '$kind')

    @cached_property
    def title(self) -> str:
        return getattr(self, '@title', '')

    @cached_property
    def length(self) -> int:
        return getattr(self, '@length', 0)

    @cached_property
    def lengthStr(self) -> str:
        return getattr(self, '@length-str', '')

    @cached_property
    def type_(self) -> [BlockType, str]:
        if self.kind == 'head':
            if getattr(self, '@type', '') == 'long':
                return self.BlockType.Long
            else:
                return self.BlockType.Short
        else:
            return getattr(self, '@type', '')

    @cached_property
    def typeStr(self) -> str:
        return getattr(self, '@type', '')

    @cached_property
    def count(self) -> Interval:
        return getattr(self, '@count')

    @cached_property
    def countStr(self) -> str:
        return getattr(self, '@count-str', '')

    @cached_property
    def ident(self) -> str:
        return getattr(self, '@id', '')

    @cached_property
    def identMark(self) -> str:
        if self.ident:
            return f'#{self.ident}'
        return ''

    @cached_property
    def allowEmpty(self) -> bool:
        return getattr(self, '@allow-empty', 'false') == 'true'

    @cached_property
    def allowMangled(self) -> bool:
        return getattr(self, '@allow-mangled', 'false') == 'true'

    @cached_property
    def value(self) -> str:
        return getattr(self, '@value', '')

    @cached_property
    def extraData(self) -> dict:
        return getattr(self, '@extra-data', {})

    @cached_property
    def docWhat(self) -> str:
        return getattr(self, 'doc:what', '')

    @cached_property
    def docDetails(self) -> str:
        return getattr(self, 'doc:details', '')

    @cached_property
    def specPath(self) -> SpecPath:
        # NOTE: this property must not be called 'path' because it conflicts with
        # the anytree node path.

        if self.isFileRoot:
            return SpecPath('/')

        ancestor = self.parent
        reverse_path = [self.identMark or self.title or self.kind]

        while ancestor:
            if ancestor.isFileRoot:
                break

            reverse_path.append(ancestor.identMark or ancestor.title or ancestor.kind)
            ancestor = ancestor.parent

        # FIXME The index is calculated only from siblings even though a path
        # like ANNO/data/data / 2 could match cousins as well:
        #
        # ANNO - data - data
        #               data     << would match
        #        data - data
        #               data     << would match too
        #               data     << ***
        #        data - data
        #
        # In the above example, the path ANNO/data/data / 5 would only match the
        # item marked with "***".
        #
        # The index selector selects from all matching results, not only from
        # siblings of the first match.
        #
        # TEMPORARY SOLUTION: skip the index. This makes paths less unique but
        # that's something the user has to deal with.

        # index = 0
        #
        # for i in self.parent.children:
        #     if self.ident:
        #         if i.ident == self.ident:
        #             index += 1
        #     elif self.title:
        #         if i.title == self.title or i.kind == self.title:
        #             index += 1
        #     elif self.kind:
        #         if i.title == self.kind or i.kind == self.kind:
        #             index += 1
        #
        #     if i is self:
        #         break

        # return SpecPath('/'.join(reversed(reverse_path)) + f' / {index:d}')
        return SpecPath('/'.join(reversed(reverse_path)))

    @cache
    def __str__(self):
        ident = f"{self.identMark}, " if self.ident else ""

        if self.kind == 'data':
            return f"Data({ident}{self.typeStr}, c: {self.countStr}, l: {self.lengthStr})"
        elif self.kind == 'head':
            return f"Block({ident}{self.title}, c: {self.countStr}, l: {self.typeStr})"
        elif self.kind == 'group':
            return f"Group({ident}c: {self.countStr})"
        else:
            return f"{self.kind}({self.title}, {self.typeStr}, c: {self.countStr}, l: {self.lengthStr}{ident})"


class _Definition:
    def __init__(self, file: str, part: str = None, string: str = ''):
        """
        Load a spec definition.

        Pass a XML string to 'string' to load the definition from memory. In that
        case, you MUST pass a unique identifier for the spec file in the 'file' argument.

        Pass a file path to 'file' to load the definition from disk.
        """

        if string:
            self._string = string
            self.file = file
            match = re.search(r'(?P<basename>.*)$', str(file), flags=re.I)
        else:
            match = re.search(r'(.*/)?(?P<basename>.*?)\.xml$', str(file), flags=re.I)

            if not match:
                log_fatal(ValueError, f'invalid spec file name "{file}"')

            self._string = None
            self.file = Path(file)
            if not self.file.is_file():
                log_fatal(FileNotFoundError, f'spec file "{file}" not found')
            self.file = self.file.resolve()  # make absolute

        self.ident = match.group('basename') + ('@' + part if part else '')
        self.part = part
        self.partPath = SpecPath(part) if part else None
        self._real_xml = None
        self._real_xml_is_enriched = False
        self._real_title = None
        self._real_magic: [None, List[[int, None]]] = None
        self._real_magic_offset = None
        self._real_priority = None
        self._real_node: SpecNode = None

        self._templ_filters = {
            'dedent_pre': lambda x: f'<pre>{textwrap.dedent(x)}</pre>',
            'dedent': lambda x: textwrap.dedent(x),
            'bitmask1': lambda x: f'<tt>{int(x):08b}</tt>',
            'bitmask2': lambda x: f'<tt>{int(x):016b}</tt>',
            'bitmask4': lambda x: f'<tt>{int(x):032b}</tt>',
        }
        self._templ_globals = {}

    @property
    def node(self) -> SpecNode:
        self._load()

        if not self._real_node:
            self._real_node = SpecNodeFactory.makeSpecNode(self.xml, debug=False)

        return self._real_node

    @property
    def magic(self) -> [None, List[[int, None]]]:
        self._load()
        return self._real_magic

    @property
    def magic_encoded(self) -> str:
        return ' '.join([bytes([x]).hex() if x is not None else '*' + ' ' for x in self.magic])

    @property
    def magic_readable(self) -> str:
        readable = ''

        for i in self.magic:
            char = '?'

            if i is None:
                char = '*'
            elif i == 0:
                char = '.'
            else:
                char = bytes([i]).decode('ascii', 'replace').replace('�', '?')

            readable += char

        return readable

    @property
    def magic_offset(self):
        self._load()
        return self._real_magic_offset

    @property
    def priority(self):
        self._load()
        return self._real_priority

    @property
    def title(self):
        self._load()
        return self._real_title

    @property
    def xml(self):
        self._load()
        self._enrich()
        return self._real_xml

    def _load(self):
        if self._real_xml:
            return

        if self._string:
            self._real_xml = BeautifulSoup(self._string, 'xml')
        else:
            with open(self.file, 'r') as fd:
                self._real_xml = BeautifulSoup(fd.read(), 'xml')

        if self.part:
            self._enrich()
            part_xml = BeautifulSoup('', 'xml')
            main_tag = part_xml.new_tag('file', attrs=self._real_xml.file.attrs)
            main_tag['part'] = self.part
            part_xml.append(main_tag)

            selection = self._real_xml.select_one('file > ' + self.partPath.css)

            if not selection:
                log_fatal(ValueError, f'invalid spec part selection: {self.part} not available in {self.file}')

            main_tag.append(selection)
            self._real_xml = part_xml

        filenode = self._real_xml.find('file')
        self._real_title = filenode.get('title', '')

        if self.part:
            self._real_title += f' ({self.part})'

        try:
            offset, magic = filenode.get('magic', '0|').split('|')
        except ValueError:
            msg = f'magic "{filenode.get("magic", "0|")}" cannot be parsed'
            raise InvalidMagicError(msg)

        self._real_magic_offset = int(offset)

        magic = magic.replace(' ', '').replace('*', '**')
        self._real_magic = [None if x == ('*', '*') else bytes.fromhex(x[0] + x[1])[0] for x in grouped(magic, 2)] if magic else None

        priorities = {'low': 0, 'medium': 1, 'high': 2}
        self._real_priority = priorities[filenode.get('priority', 'medium')]

    def _enrich(self):
        # this method assumes it is called by xml() after the main xml has
        # been loaded and saved in self._real_xml
        if self._real_xml_is_enriched:
            return

        self._real_xml = resolved_xml_templ(self.file, [DataPaths.spec],
                                            custom_filters=self._templ_filters,
                                            custom_globals=self._templ_globals)
        self._real_xml_is_enriched = True


class Spec:
    _DEFINITIONS = {}
    _SPEC_DIR = None

    def __init__(self):
        pass

    @classmethod
    def get_spec_dir(cls):
        if cls._SPEC_DIR:
            return cls._SPEC_DIR

        sdir = Path('./spec')

        if not sdir.is_dir():
            sdir = DataPaths.spec

            if not sdir.is_dir():
                log_fatal(FileNotFoundError, f"spec directory not found at '{sdir}' nor './spec")

        cls._SPEC_DIR = sdir
        return cls._SPEC_DIR

    @classmethod
    def reload_definitions(cls):
        options = list(cls.get_spec_dir().glob('*.xml'))

        if not options:
            log_fatal(ValueError, f"no spec files found at '{cls.get_spec_dir()}'")

        cls._DEFINITIONS = {}
        for opt in options:
            definition = _Definition(opt)
            cls._DEFINITIONS[definition.ident] = definition

    @classmethod
    def _load(cls):
        if not cls._DEFINITIONS:
            cls.reload_definitions()

    @classmethod
    def get_definitions(cls):
        cls._load()
        return cls._DEFINITIONS

    @classmethod
    def from_xml_string(cls, xmlstring, part=None):
        definition = _Definition(f'inline.{random_str(8)}', part)
        cls._DEFINITIONS[definition.ident] = definition
        return definition

    @classmethod
    def from_custom_file(cls, spec_file, part=None):
        definition = _Definition(spec_file, part)
        cls._DEFINITIONS[definition.ident] = definition
        return definition

    @classmethod
    def by_type(cls, spec_type, part=None):
        cls._load()

        if spec_type not in cls._DEFINITIONS:
            log_fatal(FileNotFoundError, f"no spec file found for type '{spec_type}'")

        actual_type = spec_type + ('@' + part if part else '')
        if actual_type not in cls._DEFINITIONS:
            cls._DEFINITIONS[actual_type] = _Definition(cls._DEFINITIONS[spec_type].file, part)

        return cls._DEFINITIONS[actual_type]

    @classmethod
    def by_magic(cls, raw_file) -> [_Definition, None]:
        """ Not possible to load a custom part from an unknown file. """
        cls._load()
        file_length = os.path.getsize(raw_file)

        with open(raw_file, 'rb') as fd:
            # 32 = 12+4 + 12+4 -> for two headers with sizes
            cached = fd.read(32) if file_length >= 32 else bytearray()

            candidates = []

            for definition in cls._DEFINITIONS.values():
                offset = definition.magic_offset
                magic = definition.magic
                from_file = cached

                if not magic:
                    continue

                if offset + len(magic) > file_length:
                    continue

                if offset + len(magic) > len(cached):
                    fd.seek(offset)
                    from_file = fd.read(len(magic))
                else:
                    from_file = cached[offset:offset + len(magic)]

                if all([True if x == y or y is None else False for x, y in zip(from_file, magic)]):
                    candidates.append(definition)

            candidates.sort(key=lambda x: x.priority, reverse=True)  # high to low

            if candidates:
                return candidates[0]

        log_fatal(FileNotFoundError, f"no possible spec file found for file '{raw_file}'")


class SpecNodeFactory:
    @staticmethod
    def _postProcessXmlNode(path, key, value):
        """ Post-process XML nodes as dict.

        1. Ensure node order
        --------------------

        Dicts cannot have duplicate keys. Thus, the following loses order when
        converting it using the default converter:

            <head>
                <head />
                <data />
                <head />
                <head />
            </head>

            would lead to: { head: { head: [1,2,3], data: {} } }

        To fix this, we save all children in a list and store the type
        separately in the "$kind" attribute (as data/head/group).

        This way, the above example leads to: { children: [ {head}, {data}, {head}, {head} ] }

        2. Parse length and count
        -------------------------

        Length and count strings are immediately parsed into int for length and
        SpecNode.Interval for count.
        """

        if key in ['head', 'data', 'group']:
            if not value:
                raise ValueError(f'invalid spec: no value for "{key}" at path {path}; are all XML tags properly closed?')

            value['$kind'] = key
            key = 'children'

            # # # # remove documentation nodes
            # # # # They are not needed for parsing.
            if 'details' in value:
                value['doc:details'] = xmltodict.unparse({'p': value['details']}, full_document=False)
                del value['details']

            # keep short description nodes
            # This could be useful when debugging.
            if 'what' in value:
                value['doc:what'] = value['what']
                del value['what']

            # convert inline documentation to description
            if '#text' in value and 'what' not in value:
                value['doc:what'] = str(value['#text'])
                del value['#text']

            if '@length' in value:
                value['@length-str'] = value['@length']
                value['@length'] = SpecNode.parseLength(value['@length'])

            if '@count' in value:
                value['@count-str'] = value['@count']
                value['@count'] = SpecNode.parseCount(value['@count'])
            else:
                value['@count-str'] = '1'
                value['@count'] = SpecNode.parseCount('1')

            value['@extra-data'] = {k.replace('@x-', ''): v for k, v in value.items() if k.startswith('@x-')}

        return (key, value)

    @staticmethod
    def loadSpecFile(specFile) -> SpecNode:
        with open(specFile, "r") as f:
            root = SpecNodeFactory.makeSpecNode(f.read(), debug=False)
        return root

    @staticmethod
    def makeSpecNode(specXml: str, debug: bool = False) -> SpecNode:
        schema = xmltodict.parse(str(specXml), postprocessor=SpecNodeFactory._postProcessXmlNode, force_list=['children'])
        root = None

        if 'file' not in schema:
            if 'children' in schema:
                if not schema['children']:
                    raise EmptySpecError(f"spec is empty: {specXml}")
                elif len(schema['children']) > 1:
                    schema = {'file': schema}
                else:
                    schema = schema['children'][0]
                    root = schema
            else:
                raise EmptySpecError(f"spec is empty: {specXml}")

        if 'file' in schema:
            schema['file']['$kind'] = 'file'
            root = schema['file']

        if debug:
            print(json.dumps(schema, indent=2))

        # convert dict to tree
        importer = DictImporter(nodecls=SpecNode)
        root = importer.import_(root)

        if debug:
            print(RenderTree(root))

        return root
