# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import mmap
import struct
import sys
import contextlib
import contextvars
import enum
import os
import random
import string
import math
import pathlib
import logging as log
from collections import namedtuple
from functools import cached_property
from functools import cache
from functools import partial
from pathlib import Path as FilePath

from typing import Tuple
from typing import List
from typing import Dict
from typing import Any
from typing import Iterator
from typing import Callable

from anytree import Node
from anytree import RenderTree
from anytree import LevelOrderIter

from . import decoders
# from . import tools
from .spec import Spec
from .spec import SpecNode
from .spec import SpecNodeFactory
from .spec import InvalidGroupMemberError
# from .spec import VariableGroupLengthError
from .spec import InvalidRootParentError
from .spec import UnknownSpecTypeError
from .path import Path


XML_INDENT_STR = '\t'


# type ProgressCallback = Callable[[float, int, int], None]  # 'type' keyword requires Python 3.12
ProgressCallback = Callable[[float, int, int], None]


def noOpProgressCallback(percent: float, level: int = 1, level_count: int = 1) -> None:
    pass


ctxCurrentlyParsing = contextvars.ContextVar('ctxCurrentlyParsing', default=False)
ctxCurrentlyParsedBytes = contextvars.ContextVar('ctxCurrentlyParsedBytes', default=0)
ctxCurrentlyParsedTotal = contextvars.ContextVar('ctxCurrentlyParsedTotal', default=0)
ctxDynamicGroupsEnabled = contextvars.ContextVar('ctxDynamicGroupsEnabled', default=False)
ctxProgressCallback = contextvars.ContextVar('ctxProgressCallback', default=noOpProgressCallback)


class BinaryError(MemoryError):
    """ Base class for errors caused by mishandling of binary data. """
    pass


class OutOfBoundsError(BinaryError):
    pass


class InvalidSizeError(BinaryError):
    pass


class MissingBlockError(BinaryError):
    pass


class MissingDataError(BinaryError):
    pass


class EditWhileParsingError(BinaryError):
    pass


class BlockMismatchError(BinaryError):
    pass


class TreeError(ValueError):
    """ Base class for errors caused by mishandling of the parsed tree. """
    pass


class ImmutableNodeError(TreeError):
    pass


class UninitializedNodeError(TreeError):
    pass


class UnrelatedNodeError(TreeError):
    pass


class InvalidMutationError(TreeError):
    pass


class ParseMode:
    class State(enum.Enum):
        Keep = 1
        Enable = 2
        Disable = 3

    def __enter__(self):
        return contextlib.closing(self).__enter__()

    def __exit__(self, *args):
        self.close()

    def __init__(self, parseStatus=State.Enable, dynamicGroups=State.Enable,
                 progressCallback: ProgressCallback = None,
                 resetProgress: bool = True):
        self._parseStatusToken = None
        self._groupStatusToken = None
        self._progresserToken = None
        self._resetProgress = resetProgress

        if resetProgress:
            self._bytesToken = ctxCurrentlyParsedBytes.set(0)
            self._totalToken = ctxCurrentlyParsedTotal.set(0)

        if parseStatus != self.State.Keep:
            self._parseStatusToken = ctxCurrentlyParsing.set(True if parseStatus == self.State.Enable else False)

        if dynamicGroups != self.State.Keep:
            self._groupStatusToken = ctxDynamicGroupsEnabled.set(True if dynamicGroups == self.State.Enable else False)

        if progressCallback:
            self._progresserToken = ctxProgressCallback.set(progressCallback)

    def close(self):
        if self._resetProgress:
            ctxCurrentlyParsedBytes.reset(self._bytesToken)
            ctxCurrentlyParsedTotal.reset(self._totalToken)

        if self._parseStatusToken:
            ctxCurrentlyParsing.reset(self._parseStatusToken)

        if self._groupStatusToken:
            ctxDynamicGroupsEnabled.reset(self._groupStatusToken)

        ctxProgressCallback.get()(100.0)

        if self._progresserToken:
            ctxProgressCallback.reset(self._progresserToken)


class ParsedNode(Node):
    _dataStore: dict = {}
    _ReadResult = namedtuple('ReadResult', 'ok headLength bodyLength customPadding')

    def __init__(self, name: str, specNode: SpecNode, dataLength: int,
                 parent: 'ParsedNode' = None,
                 children: List['ParsedNode'] = None):
        # Whether this node is user-generated or parsed from a file.
        self.isMemoryBacked = True

        self.specNode: SpecNode = specNode
        self.dataLength = dataLength  # does not include possible headers

        # Nodes that have a direct uint data child can be addressed by the value
        # of said child. This property is a shorthand for detecting "entry id"
        # nodes directly. If a node has a ID that ends with '-id', it will replace
        # its parent's current entryIdNode in the _pre_attach handler. The property
        # is reset when that node is detached again, in the _pre_detach handler.
        self.entryIdNode: ParsedNode = None

        # IMPORTANT The data buffer will be set to the parent's data buffer when the
        #           node is attached to a tree.
        #           It will not be reset when the node is detached, neither will
        #           it reset when the mmap is closed, i.e. when the parser object is closed.
        self._dataBuffer: mmap = None

        super(ParsedNode, self).__init__(name, parent=parent, children=children)

        self.localStartOffset = 0

        self._changed = False
        self._deleted = False
        self._newDataKeyCache = None  # access through _newDataKey
        self._newDataLength = 0

        self._readPosition = 0

    def findNodesByIdent(self, ident: str, recursive: bool = True) -> Iterator['ParsedNode']:
        if ident.startswith('#'):
            ident = ident[1:]

        for node in LevelOrderIter(self, maxlevel=None if recursive else 1):
            if node.specNode.ident == ident:
                yield node

    def findFirstByIdent(self, ident: str, recursive: bool = True) -> [None, 'ParsedNode']:
        if ident.startswith('#'):
            ident = ident[1:]

        for node in LevelOrderIter(self, maxlevel=None if recursive else 1):
            if node.specNode.ident == ident:
                return node

        return None

    def getFirstPath(self, path: str) -> [None, 'ParsedNode']:
        if not isinstance(path, Path):
            path = Path(path)

        if path.selector.kind == path.Selector.ALL:
            path.selector = Path.Selector(Path.Selector.INDEX, 0)

        # found = self.getPath(path, maxN=path.selector.INDEX + 1)
        found = self.getPath(path)  # FIXME maxN seems to be broken (again? still?)
        return found if found else None

    def getPaths(self, path: [str, Path], maxN: int = math.inf) -> List['ParsedNode']:
        found = self.getPath(path, maxN)

        if not isinstance(found, list):
            return [found]
        return found

    def _pickPartialPathMatches(self, path, nodes):
        for i in nodes:
            if i.isDeleted:
                continue

            if path.match(i, True):
                yield i

                if i.children:
                    for i in self._pickPartialPathMatches(path, i.children):
                        yield i

    def _pickFullPathMatchesWithLimit(self, path, partialMatches, maxN) -> List['ParsedNode']:
        count = 0

        if maxN == 0:
            return

        for node in partialMatches:
            if path.match(node, matchSubset=False):
                count += 1
                yield node

                if count >= maxN:
                    break

    def _pickFullPathMatchesByIndex(self, path, partialMatches, index) -> List['ParsedNode']:
        if index >= 0:
            current_index = 0

            for node in partialMatches:
                if path.match(node, matchSubset=False):
                    if current_index == index:
                        yield node
                        break

                    current_index += 1
        else:
            all_results = [x for x in partialMatches if path.match(x, matchSubset=False)]

            try:
                yield all_results[index]
            except IndexError:
                return

    def getPath(self, path: [str, Path], maxN: int = math.inf) -> ['ParsedNode', List['ParsedNode']]:
        if not isinstance(path, Path):
            path = Path(path)

        if len(path) == 0:
            return self

        path = Path(f'{self.specPath}/{path}')

        if not path.match(self, matchSubset=True):
            return []

        pick = partial(self._pickPartialPathMatches, path, self.children)

        if maxN == math.inf and path.selector.matchAll:
            return [x for x in pick() if path.match(x, matchSubset=False)]
        elif path.selector.matchAll:
            return list(self._pickFullPathMatchesWithLimit(path, pick(), maxN))
        else:
            found = list(self._pickFullPathMatchesByIndex(path, pick(), path.selector.index))

            if found:
                return found[0]

            return []

    # def printTree(self, specOnly: bool = False) -> None:
    #     TODO: print only the spec nodes that are actually instantiated
    #     if specOnly:
    #         print(RenderTree(self.specNode))
    #     else:
    #         print(RenderTree(self))

    def printTree(self, asXML: bool = False) -> None:
        if asXML:
            for i in self.bakeXml(includeUnparsedData=False, indent=' '):
                print(i)
        else:
            print(RenderTree(self))

    @staticmethod
    def _ensureIterable(variable) -> List:
        try:
            iter(variable)
            return variable
        except TypeError:
            return [variable]

    @classmethod
    def _resetChildren(cls, parent: 'ParsedNode', newOffset: int,
                       childrenBefore: Tuple['ParsedNode'],
                       newNodes: List['ParsedNode'],
                       childrenAfter: Tuple['ParsedNode']):
        """ Set children of 'parent' to a new list.

            Anytree detaches and then attaches all nodes while the children list
            is being reset. This triggers signal handlers that notify the parent
            of length changes which should not happen for existing nodes.

            Attaching a node updates its parent's length while detaching does not.
            When nodes are temporarily detached, the length is over-counted.

            To prevent this, we have to set a special attribute on existing nodes
            that disables signal handlers, then enable them after the children
            list has been updated.
        """

        newNodes = ParsedNode._ensureIterable(newNodes)

        # Set the start offset of new nodes to the given value. This is necessary
        # to make sure the nodes are sorted correctly when baking the file.
        # Python guarantees that sorting is stable, i.e. that nodes with the same
        # offset stay in the same order when sorting.
        # Cf. https://stackoverflow.com/a/1915418
        [setattr(x, 'localStartOffset', newOffset) for x in newNodes]

        [setattr(x, '__disableTreeSignals', True) for x in childrenBefore + childrenAfter]
        newChildren = childrenBefore + newNodes + childrenAfter
        parent.children = newChildren
        [delattr(x, '__disableTreeSignals') for x in childrenBefore + childrenAfter]

    def insertBefore(self, nodes: List['ParsedNode']) -> None:
        if not self.parent:
            raise InvalidMutationError(f"cannot insert nodes before root node {self}")

        self._resetChildren(
            parent=self.parent,
            newOffset=self.localStartOffset,
            childrenBefore=self.parent.children[:self.index],
            newNodes=nodes,
            childrenAfter=self.parent.children[self.index:],
        )

    def insertAfter(self, nodes: List['ParsedNode']) -> None:
        if not self.parent:
            raise InvalidMutationError(f"cannot insert nodes after root node {self}")

        self._resetChildren(
            parent=self.parent,
            newOffset=self.localStartOffset,
            childrenBefore=self.parent.children[:self.index + 1],
            newNodes=nodes,
            childrenAfter=self.parent.children[self.index + 1:],
        )

    def appendNodes(self, nodes: List['ParsedNode']) -> None:
        self._resetChildren(
            parent=self,
            newOffset=self.children[-1].localStartOffset if self.children else 0,
            childrenBefore=self.children,
            newNodes=nodes,
            childrenAfter=tuple(),
        )

    def prependNodes(self, nodes: List['ParsedNode']) -> None:
        self._resetChildren(
            parent=self,
            newOffset=self.children[0].localStartOffset if self.children else 0,
            childrenBefore=tuple(),
            newNodes=nodes,
            childrenAfter=self.children,
        )

    def delete(self) -> None:
        # Mark this node as deleted and do not include its data when bakeBinary()
        # is called by an ancestor.
        if ctxCurrentlyParsing.get():
            raise EditWhileParsingError(f"attempted to delete '{self}' while parsing, "
                                        "which is not supported")

        if hasattr(self.parent, '_lengthChanged'):
            self.parent._lengthChanged(-self._newDataLength - self.ownDataOffset if self._changed else -self.fullLength)

        if self._changed:
            if self._newDataKey in self._dataStore:
                del self._dataStore[self._newDataKey]

            self._newDataKeyCache = None
            self._changed = False
            self._newDataLength = 0

        self._readPosition = 0
        self._deleted = True

    @property
    def isDeleted(self):
        return self._deleted

    @property
    def isChanged(self):
        return self._changed

    def getNewDataLength(self):
        return self._newDataLength  # 0 if the node is unmodified/immutable

    @property
    def specPath(self) -> Path:
        # NOTE: this property must not be called 'path' because it conflicts with
        # the anytree node path.

        plain_path, root = self._build_spec_path()

        if root is self:
            return Path('/')

        return Path(plain_path)

    @property
    def specPathWithIndex(self) -> Path:
        plain_path, root = self._build_spec_path()

        if root is self:
            return Path('/')
        elif root:
            results = root.getPaths(plain_path)
            index = None

            for i, node in enumerate(results):
                if node is self:
                    index = i
                    break

            if index is not None:
                plain_path += f' / {index+1:d}'

        return Path(plain_path)

    def _build_spec_path(self) -> Tuple[str, 'ParsedNode']:
        if self.specNode.isFileRoot:
            return ('/', self)

        ancestor = self
        root = None
        reverse_path = []

        while ancestor:
            root = ancestor
            aspec = ancestor.specNode

            if aspec.isFileRoot:
                break

            tag = aspec.identMark or aspec.title or aspec.kind

            if ancestor.entryIdNode:
                tag += f'@{ancestor.entryIdNode.specNode.ident}:{ancestor.entryIdNode.decodeToPy():d}'

            reverse_path.append(tag)
            ancestor = ancestor.parent

        plain_path = '/'.join(reversed(reverse_path))

        return (plain_path, root)

    @property
    def index(self):
        """ Return own index in parent's children list. """
        return self._getOwnIndex(self.parent, self.parent.children if self.parent else tuple(), self._changed)

    @cache
    def _getOwnIndex(self, parent, siblings, changed):
        """ Return own index in parent's children list. """
        if not self.parent:
            return 0
        return self.parent.children.index(self)

    @property
    def fullLength(self):
        # return length including headers, if there are any
        # (ownDataOffset must be defined by subclasses)
        if not self.isMemoryBacked:   # TODO properly determine if a node has been created without being parsed
            return 0

        return self.ownDataOffset + self.dataLength

    def getFinalFullLength(self):
        if self._changed:
            return self.ownDataOffset + self._newDataLength
        else:
            return self.fullLength

    def seek(self, pos, whence=os.SEEK_SET) -> None:
        # NOTE: seek() and tell() cannot be generalized in a custom memoryview
        #       class because deriving from memoryview is not possible.
        #       ("TypeError: type 'memoryview' is not an acceptable base type")

        currentGlobal = self.globalStartOffset + self.ownDataOffset + self._readPosition
        currentLocal = self._readPosition
        globalMax = self.globalStartOffset + self.fullLength
        localMax = self.dataLength

        if whence == os.SEEK_SET:
            globalTarget = self.globalStartOffset + self.ownDataOffset + pos
            localTarget = pos
        elif whence == os.SEEK_CUR:
            globalTarget = self.globalStartOffset + self.ownDataOffset + self._readPosition + pos
            localTarget = self._readPosition + pos
        elif whence == os.SEEK_END:
            globalTarget = globalMax + pos
            localTarget = self.dataLength + pos

        if localTarget < 0 or localTarget > localMax:
            raise OutOfBoundsError(f"cannot seek from l:{currentLocal} / g:{currentGlobal} "
                                   f"to position l:{localTarget} / g:{globalTarget} "
                                   f"(maximum: l:{localMax} / g:{globalMax})")

        self._readPosition = localTarget
        self._dataBuffer.seek(globalTarget, os.SEEK_SET)

    def tell(self) -> int:
        return self._readPosition

    def bakeBinary(self) -> bytearray:
        # must be implemented by subclasses as well!

        if self.isDeleted:
            yield bytes()
            return

        yieldedLocalOffset = 0

        if self.children:
            for node in sorted(self.children, key=lambda x: x.localStartOffset):
                if node.localStartOffset > yieldedLocalOffset and self.dataLength > 0:
                    yield self.dataView[yieldedLocalOffset:node.localStartOffset]
                    yieldedLocalOffset = node.localStartOffset

                for x in node.bakeBinary():
                    yield x

                yieldedLocalOffset += node.fullLength

        yield self.dataView[yieldedLocalOffset:]

    def bakeToFile(self, fileOut: str, overwrite=False) -> None:
        self._doBakeFile(fileOut, self.bakeBinary, 'wb', overwrite=overwrite)

    def bakeXml(self, indent: str = XML_INDENT_STR, includeUnparsedData: bool = True, _depth: int = 0) -> str:
        # must be implemented by subclasses as well!

        if self.parent:
            # NOTE: a subtree must be detached from its parent before baking it
            #       to XML, otherwise the resulting XML will not be wrapped in <file> tags
            raise NotImplementedError(f"The baseclass bakeXml method must not be called from {self}.")

        if self.isDeleted:
            return

        yield self._wrapXmlOpen()

        yieldedLocalOffset = 0

        if self.children:
            for node in sorted(self.children, key=lambda x: x.localStartOffset):
                if node.localStartOffset > yieldedLocalOffset and self.dataLength > 0:
                    if includeUnparsedData:
                        yield self._xmlRawDataLine(indent, _depth + 1, node.localStartOffset - yieldedLocalOffset,
                                                   self.dataView[yieldedLocalOffset:node.localStartOffset])
                    yieldedLocalOffset = node.localStartOffset

                for x in node.bakeXml(indent=indent, _depth=_depth + 1, includeUnparsedData=includeUnparsedData):
                    yield x

                yieldedLocalOffset += node.fullLength

        # if self._changed and yieldedLocalOffset < self.getNewDataLength():
        #     yield self._xmlRawDataLine(indent, _depth + 1, self.getNewDataLength() - yieldedLocalOffset)
        # elif not self._changed and yieldedLocalOffset < self.dataLength:
        #     yield self._xmlRawDataLine(indent, _depth + 1, self.dataLength - yieldedLocalOffset, '')

        if yieldedLocalOffset < self.dataLength:
            if includeUnparsedData:
                yield self._xmlRawDataLine(indent, _depth + 1, self.dataLength - yieldedLocalOffset,
                                           self.dataView[yieldedLocalOffset:])

        yield self._wrapXmlClose(indent)

    def _wrapXmlOpen(self):
        return '''<?xml version="1.0" encoding="utf-8"?>\n<file type="data">'''

    def _wrapXmlClose(self, indent):
        return ParsedNode._xmlCloseLine(indent, 0, 'file')

    @cached_property
    def _xmlIdAttr(self) -> str:
        if self.specNode.ident:
            return f' id="{self.specNode.ident}"'
        return ''

    @cache
    @staticmethod
    def _xmlIndent(indent, depth: int) -> str:
        return indent * depth

    # not cached because it can get very large
    def _xmlRawDataLine(self, indent, depth: int, length: int, value: bytes) -> str:
        return f'''{ParsedNode._xmlIndent(indent, depth)}<data length="{length}" type="raw" value="{bytes(value).hex(' ', -4)}" />'''

    def _xmlDataLine(self, indent, depth: int, length: int, kind: str, value: str) -> str:
        return f'''{ParsedNode._xmlIndent(indent, depth)}<data{self._xmlIdAttr} length="{length}" type="{kind}" value="{value}" />'''

    @cache
    def _xmlBlockLine(self, indent, depth: int, title: str, kind: str) -> str:
        if pad := getattr(self, 'customPadding', None):
            return f'{ParsedNode._xmlIndent(indent, depth)}<head title="{title}"{self._xmlIdAttr} type="{kind}" ' + \
                   f'padding="{pad.hex(bytes_per_sep=1, sep=" ")}">'

        return f'''{ParsedNode._xmlIndent(indent, depth)}<head title="{title}"{self._xmlIdAttr} type="{kind}">'''

    @cache
    def _xmlGroupLine(self, indent, depth: int) -> str:
        return f'''{ParsedNode._xmlIndent(indent, depth)}<group{self._xmlIdAttr}>'''

    @cache
    @staticmethod
    def _xmlCloseLine(indent, depth: int, tag: str) -> str:
        return f'''{ParsedNode._xmlIndent(indent, depth)}</{tag}>'''

    def bakeXmlToFile(self, fileOut: str, overwrite=False) -> None:
        self._doBakeFile(fileOut, self.bakeXml, 'w', extra=lambda f: f.write('\n'), overwrite=overwrite)

    def _doBakeFile(self, pathOrStream, baker, openMode, extra=None, overwrite=False) -> None:
        if pathOrStream == '-':
            pathOrStream = sys.stdout

        if callable(getattr(pathOrStream, 'write', None)):
            for x in baker():
                if x is not None:
                    pathOrStream.write(x)

                    if extra:
                        extra(pathOrStream)

        elif isinstance(pathOrStream, (str, os.PathLike)):
            if pathlib.Path(pathOrStream).exists() and not overwrite:
                raise FileExistsError(f"output file '{pathOrStream}' already exists")

            with open(pathOrStream, openMode) as f:
                for x in baker():
                    if x is not None:
                        f.write(x)

                        if extra:
                            extra(f)
        else:
            raise ValueError(f"invalid output file or stream '{pathOrStream}'")

    def checkAhead(self, expectedSpecNode: SpecNode):
        back = self.tell()
        tagLength = BlockNode.TagLength[expectedSpecNode.type_]
        headLength = tagLength + BlockNode.LenLength[expectedSpecNode.type_]

        if back + headLength > self.dataLength:
            return self._ReadResult(False, headLength, 0, bytes())

        read = BlockNode.Header._make(struct.unpack(BlockNode.HeaderString[expectedSpecNode.type_],
                                                    memoryview(self.dataView)[back:back + headLength]))

        expected = expectedSpecNode.title
        ok = read.tag == ParsedNode._padded(bytes(expected, 'utf8'), tagLength)
        customPadding = bytes()

        if not ok and expectedSpecNode.allowMangled:
            tag = read.tag.split(b'\x00')[0]
            customPadding = read.tag[len(tag):]
            ok = tag == bytes(expected, 'utf8')

        return self._ReadResult(ok, headLength, read.length, customPadding)

    @staticmethod
    @cache
    def _padded(constant: str, length: int, padding: bytes = b'\x00') -> bytes:
        assert len(constant) <= length, f"Constant '{constant.decode('windows-1252')}' must not be larger than available width '{length}'."
        return constant + (length - len(constant)) * padding

    @property
    def dataView(self):
        if self.parent:
            return self._getDataView(self.parent.dataView, self.localDataOffset, self.dataLength)
        else:
            return self._getDataView(None, self.localDataOffset, self.dataLength)

    # caching is not possible because it makes it impossible to close the input file
    # @lru_cache(maxsize=1)
    def _getDataView(self, parentView, offset, length):
        if parentView:
            return memoryview(parentView).toreadonly()[offset:offset + length]
        elif self._dataBuffer:
            return memoryview(self._dataBuffer).toreadonly()[offset:offset + length]
        else:
            return memoryview(bytes())

    @cached_property
    def ownDataOffset(self):
        return 0

    @cached_property
    def localDataOffset(self):
        return self.localStartOffset

    def getNewLocalStartOffset(self):
        previousSiblings = self.parent.children[:self.index]
        newLocalStartOffset = 0

        # FIXME this is wrong if not all siblings have been parsed!
        # FIXME this is wrong if not all data is covered by siblings!
        for i in previousSiblings:
            newLocalStartOffset += i.getFinalFullLength()

        return newLocalStartOffset

    @property
    def globalStartOffset(self):
        return self._getGlobalStartOffset(self.parent)

    @cache
    def _getGlobalStartOffset(self, parent):
        if self.parent:
            return self.parent.globalStartOffset + self.parent.ownDataOffset + self.localStartOffset
        else:
            return self.localStartOffset

    @cached_property
    def _newDataKey(self):
        if not self._newDataKeyCache:
            self._newDataKeyCache = ''.join(random.choices(string.ascii_lowercase, k=8))
        return self._newDataKeyCache

    def _pre_detach(self, parent):
        # NOTE This method doesn't call parent._lengthChanged() because it only removes
        #      the node from the tree. It does not actually delete its data.
        #
        #      Detaching a node does NOT delete it from the baked binary. Call delete()
        #      to actually delete the node from the output. If the node is only
        #      detached, it will be treated as if it hadn't been parsed.

        if hasattr(self, '__disableTreeSignals'):
            return

        if not ctxCurrentlyParsing.get():
            if isinstance(parent, DataNode):
                raise ImmutableNodeError(f"data nodes cannot have children (in '{self.specNode}')")

        if isinstance(parent, GroupNode) and not ctxDynamicGroupsEnabled.get():
            raise ImmutableNodeError(f"groups cannot change their children after construction (in '{self.specNode}')")

        if parent and parent.entryIdNode is self:
            parent.entryIdNode = None

    def _pre_attach(self, parent):
        if hasattr(self, '__disableTreeSignals'):
            return

        if not ctxCurrentlyParsing.get():
            if not hasattr(self, '_changed'):
                raise UninitializedNodeError(
                    "when creating new nodes, make sure to set the parent property immediately "
                    "after creation, as well as before changing data through setData()")

            if isinstance(parent, DataNode):
                raise ImmutableNodeError(f"data nodes cannot have children (in '{self.specNode}')")

        if isinstance(parent, GroupNode) and not ctxDynamicGroupsEnabled.get():
            raise ImmutableNodeError(f"groups cannot change their children after construction (in '{self.specNode}')")

    def _post_attach(self, parent):
        if hasattr(self, '__disableTreeSignals'):
            return

        if not ctxCurrentlyParsing.get() and hasattr(parent, '_lengthChanged'):
            parent._lengthChanged(+self._newDataLength + self.ownDataOffset if self._changed else +self.fullLength)

        # IMPORTANT The data buffer will not be reset when the node is detached.
        #           Neither will it reset when the mmap is closed, i.e. when the parser
        #           object is closed.
        if self._dataBuffer is None and hasattr(parent, '_dataBuffer'):
            self._dataBuffer = parent._dataBuffer

        if parent and self.specNode.ident.endswith('-id'):
            parent.entryIdNode = self


class BlockNode(ParsedNode):
    Header = namedtuple('Header', 'tag length')
    HeaderString = {SpecNode.BlockType.Long: '<12sL', SpecNode.BlockType.Short: '<6sH'}
    TagLength = {SpecNode.BlockType.Long: 12, SpecNode.BlockType.Short: 6}
    LenLength = {SpecNode.BlockType.Long: 4, SpecNode.BlockType.Short: 2}

    TagBytesCache: Dict[Tuple[str, int, bytes], bytes] = {}

    def __init__(self, name: str, specNode: SpecNode, dataLength: int,
                 parent: ParsedNode = None, children: List[ParsedNode] = None):
        super(BlockNode, self).__init__(name, specNode, dataLength,
                                        parent=parent, children=children)

        self._kind = specNode.type_
        self._tagLength = self.TagLength[self._kind]
        self._lengthLength = self.LenLength[self._kind]
        self._headerStructString = self.HeaderString[self._kind]

        self.customPadding = bytes()

    @cached_property
    def ownDataOffset(self):
        return self._tagLength + self._lengthLength

    @property
    def localDataOffset(self):
        return self.localStartOffset + self.ownDataOffset

    def bakeBinary(self) -> bytearray:
        if self.isDeleted:
            yield bytes()
            return

        if self._changed:
            yield struct.pack(self._headerStructString, self._tagBytes, self._newDataLength)
        else:
            yield struct.pack(self._headerStructString, self._tagBytes, self.dataLength)

        yieldedLocalOffset = 0  # headers don't count here

        if self.children:
            for node in sorted(self.children, key=lambda x: x.localStartOffset):
                if node.localStartOffset > yieldedLocalOffset and self.dataLength > 0:
                    yield self.dataView[yieldedLocalOffset:node.localStartOffset]
                    yieldedLocalOffset = node.localStartOffset

                for x in node.bakeBinary():
                    yield x

                yieldedLocalOffset += node.fullLength

        yield self.dataView[yieldedLocalOffset:]

    def bakeXml(self, indent: str = XML_INDENT_STR, includeUnparsedData: bool = True, _depth: int = 0) -> str:
        if self.isDeleted:
            return

        if not self.parent:
            yield self._wrapXmlOpen()
            _depth = 1

        yield self._xmlBlockLine(indent, _depth, self.specNode.title, self.specNode.typeStr)

        yieldedLocalOffset = 0

        if self.children:
            for node in sorted(self.children, key=lambda x: x.localStartOffset):
                if node.localStartOffset > yieldedLocalOffset and self.dataLength > 0:
                    if includeUnparsedData:
                        yield self._xmlRawDataLine(indent, _depth + 1, node.localStartOffset - yieldedLocalOffset,
                                                   self.dataView[yieldedLocalOffset:node.localStartOffset])
                    yieldedLocalOffset = node.localStartOffset

                for x in node.bakeXml(indent=indent, _depth=_depth + 1, includeUnparsedData=includeUnparsedData):
                    yield x

                yieldedLocalOffset += node.fullLength

        # if self._changed and yieldedLocalOffset < self.getNewDataLength():
        #     yield self._xmlRawDataLine(indent, _depth + 1, self.getNewDataLength() - yieldedLocalOffset, '')
        # elif not self._changed and yieldedLocalOffset < self.dataLength:
        #     yield self._xmlRawDataLine(indent, _depth + 1, self.dataLength - yieldedLocalOffset, '')

        if yieldedLocalOffset < self.dataLength:
            if includeUnparsedData:
                yield self._xmlRawDataLine(indent, _depth + 1, self.dataLength - yieldedLocalOffset,
                                           self.dataView[yieldedLocalOffset:])

        yield ParsedNode._xmlCloseLine(indent, _depth, 'head')

        if not self.parent:
            yield self._wrapXmlClose(indent)

    @cached_property
    def _tagBytesKey(self):
        return (self.name, self._tagLength, self.customPadding)

    @cached_property
    def _tagBytes(self):
        if self._tagBytesKey not in self.TagBytesCache:
            self.TagBytesCache[self._tagBytesKey] = ParsedNode._padded(
                bytes(self.name, 'utf8') + self.customPadding, self._tagLength)

        return self.TagBytesCache[self._tagBytesKey]

    def _lengthChanged(self, by):
        if not self._changed:
            self._newDataLength = self.dataLength
            self._changed = True

        if (self._newDataLength + by) > (256 ** self._lengthLength):
            # raise an error and abort without storing the new size, and
            # without notifying parents of the (attempted) change
            raise InvalidSizeError(f"cannot resize block to {self._newDataLength} bytes, "
                                   f"maximum size is {256 ** self._lengthLength} bytes "
                                   f"({self._lengthLength} bytes length declaration)")

        self._newDataLength += by

        if self.parent and hasattr(self.parent, '_lengthChanged'):
            self.parent._lengthChanged(by)


class DataNode(ParsedNode):
    def decodeToPy(self, useNewIfChanged: bool = True, extraData: Dict[str, Any] = {}) -> Any:
        if self.isChanged:
            # The last argument passes custom extra data and extra data from the spec node
            # as keyword arguments to the transcoder. Custom values take precedence over
            # values from the spec node when the dicts are merged.
            return decoders.bin2py(self.getNewData(), self.specNode, subtree=self.parent, **(self.specNode.extraData | extraData))

        return decoders.bin2py(self.dataView, self.specNode, subtree=self.parent, **(self.specNode.extraData | extraData))

    def decodeToStr(self, useNewIfChanged: bool = True, extraData: Dict[str, Any] = {}) -> str:
        return decoders.py2xml(self.decodeToPy(useNewIfChanged), self.specNode, subtree=self.parent, **(self.specNode.extraData | extraData))

    def encodeFromPy(self, newData: Any, setImmediately: bool = True, extraData: Dict[str, Any] = {}) -> bytes:
        converted = decoders.py2bin(newData, self.specNode, subtree=self.parent, **(self.specNode.extraData | extraData))

        if setImmediately:
            self.setData(converted)

        return converted

    def encodeFromStr(self, newData: str, setImmediately: bool = True, extraData: Dict[str, Any] = {}) -> bytes:
        return self.encodeFromPy(decoders.xml2py(newData, self.specNode, subtree=self.parent, **(self.specNode.extraData | extraData)), setImmediately)

    @cached_property
    def canResize(self):
        if self.specNode.length == math.inf:
            return True
        else:
            return False

    def getNewData(self):
        if self._changed:
            return memoryview(self._dataStore[self._newDataKey]).toreadonly()
        else:
            return memoryview().toreadonly()

    def setData(self, newData: [bytes, bytearray, memoryview]):
        newLength = len(newData)
        allowedLength = self.specNode.length

        if self.isMemoryBacked and not self.isChanged:
            currentLength = self.dataLength
        else:
            currentLength = self._newDataLength

        log.debug(f"SETTING DATA: {newData} @ {newLength} bytes VERSUS {currentLength} bytes, MEMORY {self.isMemoryBacked}, NDATA {self._newDataLength}")

        if newLength != allowedLength or newLength != currentLength:
            if newLength == allowedLength or self.canResize:
                try:
                    if self.parent and hasattr(self.parent, '_lengthChanged'):
                        self.parent._lengthChanged(newLength - currentLength)
                except InvalidSizeError:
                    raise InvalidSizeError("cannot add new data because parent block is full")
            else:
                raise InvalidSizeError("new data of non-resizable block must be exactly "
                                       f"'{allowedLength}' bytes long, got '{newLength}'")

        self._changed = True
        self._newDataLength = newLength
        self._dataStore[self._newDataKey] = newData[:]  # copy!

    def bakeBinary(self):
        if self.isDeleted:
            yield bytes()
            return

        if self._changed:
            yield self.getNewData()[:]
        else:
            yield self.dataView[:]

    def bakeXml(self, indent: str = XML_INDENT_STR, includeUnparsedData: bool = True, _depth: int = 0) -> str:
        if self.isDeleted:
            return

        if not self.parent:
            yield self._wrapXmlOpen()
            _depth = 1

        if self._changed:
            yield self._xmlDataLine(indent, _depth, self.getNewDataLength(), self.specNode.type_, self.decodeToStr(True))
        else:
            yield self._xmlDataLine(indent, _depth, self.dataLength, self.specNode.type_, self.decodeToStr(True))

        if not self.parent:
            yield self._wrapXmlClose(indent)

    def saveBodyBinary(self, fileOut: str, overwrite=False) -> None:
        self._doBakeFile(fileOut, self.bakeBinary, 'wb', overwrite=overwrite)

    def saveBodyString(self, fileOut: str, overwrite=False) -> None:
        def yieldBody():
            yield self.decodeToStr(True)

        self._doBakeFile(fileOut, yieldBody, 'w', extra=lambda f: f.write('\n'), overwrite=overwrite)


class GroupNode(ParsedNode):
    """ Just a container for DataNode elements.

        Groups must be statically sized and cannot get new children once parsed.
        As their size cannot change, they don't have to propagate size changes.

        TODO update docs
    """

    def bakeBinary(self):
        if self.isDeleted:
            yield bytes()
            return

        yieldedLocalOffset = 0

        if self.children:
            for node in sorted(self.children, key=lambda x: x.localStartOffset):
                if node.localStartOffset > yieldedLocalOffset and self.dataLength > 0:
                    yield self.dataView[yieldedLocalOffset:node.localStartOffset]
                    yieldedLocalOffset = node.localStartOffset

                for x in node.bakeBinary():
                    yield x

                yieldedLocalOffset += node.fullLength

        yield self.dataView[yieldedLocalOffset:]

    def bakeXml(self, indent: str = XML_INDENT_STR, includeUnparsedData: bool = True, _depth: int = 0) -> str:
        if self.isDeleted:
            return

        if not self.parent:
            yield self._wrapXmlOpen()
            _depth = 1

        yield self._xmlGroupLine(indent, _depth)

        yieldedLocalOffset = 0

        if self.children:
            for node in sorted(self.children, key=lambda x: x.localStartOffset):
                if node.localStartOffset > yieldedLocalOffset and self.dataLength > 0:
                    if includeUnparsedData:
                        yield self._xmlRawDataLine(indent, _depth + 1, node.localStartOffset - yieldedLocalOffset,
                                                   self.dataView[yieldedLocalOffset:node.localStartOffset])
                    yieldedLocalOffset = node.localStartOffset

                for x in node.bakeXml(indent=indent, _depth=_depth + 1, includeUnparsedData=includeUnparsedData):
                    yield x

                yieldedLocalOffset += node.fullLength

        if yieldedLocalOffset < self.dataLength:
            if includeUnparsedData:
                yield self._xmlRawDataLine(indent, _depth + 1, self.dataLength - yieldedLocalOffset,
                                           self.dataView[yieldedLocalOffset:])

        yield ParsedNode._xmlCloseLine(indent, _depth, 'group')

        if not self.parent:
            yield self._wrapXmlClose(indent)

    @staticmethod
    def _calculateLength(specNode: SpecNode) -> SpecNode.Interval:
        minGroupLength = 0
        maxGroupLength = 0

        # Only clearly delimited types are allowed to be with dynamic length or
        # dynamic count. Otherwise it is impossible to determine when a group
        # ends.
        allowedVariableLengthTypes = set(['cstring'])

        for child in specNode.children:
            if child.kind == 'data':
                if child.length * child.count.max == math.inf:
                    if child.typeStr not in allowedVariableLengthTypes:
                        raise InvalidGroupMemberError(f"variable-length blocks of type '{child.typeStr}' ({child}) are not allowed in a group")

                    minGroupLength += 0  # minimum length of a variable length block is 0
                    maxGroupLength += 1024  # TODO choose a less magical magic number
                else:
                    minGroupLength += child.length * child.count.max  # parsed in _postProcessXmlNode
                    maxGroupLength += child.length * child.count.max  # parsed in _postProcessXmlNode
            else:
                raise InvalidGroupMemberError(f"groups must not have non-data children but group '{specNode}' has '{child}'")

        return SpecNode.Interval._make([minGroupLength, maxGroupLength])

    def _lengthChanged(self, by):
        if not ctxDynamicGroupsEnabled.get():
            raise ImmutableNodeError("groups cannot be changed after creation, "
                                     f"attempted to change length of '{self}' by '{by}'")

        if not self._changed:
            self._newDataLength = self.dataLength
            self._changed = True

        self._newDataLength += by

        if self.parent and hasattr(self.parent, '_lengthChanged'):
            self.parent._lengthChanged(by)


class NewDataFactory:
    @staticmethod
    def loadDataXmlFile(dataFile) -> ParsedNode:
        if os.path.getsize(dataFile) > 1 * 1024 * 1024 * 50:
            raise RuntimeError(f"file '{dataFile}' is larger than 50 MiB, prefer loading it in chunks")

        with open(dataFile, "r") as f:
            root = NewDataFactory.makeExtractedNodes(f.read())
        return root

    @staticmethod
    def makeExtractedNodes(dataXml, modCache: 'ModCache' = None) -> ParsedNode:
        xmltree = SpecNodeFactory.makeSpecNode(dataXml, debug=False)

        if modCache is None:
            modCache = ModCache()

        return NewDataFactory.__makeTree(xmltree, loadValues=True, modCache=modCache)

    @staticmethod
    def makeEmptyNodes(specNode: SpecNode) -> ParsedNode:
        return NewDataFactory.__makeTree(specNode, loadValues=False)

    @staticmethod
    def __makeTree(specTree: SpecNode, loadValues: bool, modCache: 'ModCache' = None) -> ParsedNode:
        with ParseMode(parseStatus=ParseMode.State.Disable,
                       dynamicGroups=ParseMode.State.Enable) as ctx:
            assert ctx  # silence pyflakes
            root = NewDataFactory._makeRootLevelNodesFromSpec(specTree, loadValue=loadValues, modCache=modCache)[0]

            if modCache and not modCache.tree:
                modCache.tree = root

            children = NewDataFactory._makeChildrenNodes(root, loadValue=loadValues, modCache=modCache)

            while children:
                parsedChildren = []

                for node in children:
                    parsedChildren += NewDataFactory._makeChildrenNodes(node, loadValue=loadValues, modCache=modCache)

                children = parsedChildren

        return root

    @staticmethod
    def _makeRootLevelNodesFromSpec(specNode: SpecNode, parentNode: ParsedNode = None, loadValue: bool = False, modCache: 'ModCache' = None) -> List[SpecNode]:
        kind = specNode.kind

        if kind == 'file':
            if parentNode:
                raise InvalidRootParentError(f"file root nodes must not have a parent: {specNode}")
            else:
                node = ParsedNode('root', specNode, 0, parent=None)
                return [node]

        count = specNode.count  # parsed in _postProcessXmlNode

        if count.min > 1 and not parentNode:
            raise InvalidRootParentError(f"cannot make multiple nodes without a parent: {specNode}")
        elif count.min == 0 and not parentNode:
            count = 1  # optional nodes are only created if they are made stand-alone
        else:
            count = count.min

        nodes = []

        for i in range(0, count):
            if kind == 'data':
                baseLength = specNode.length
                if baseLength == math.inf:
                    baseLength = 0

                node = DataNode('data', specNode=specNode, dataLength=0, parent=None)
                node.parent = parentNode

                if loadValue and specNode.value:
                    extraData = {}

                    if modCache:
                        extraData['mod'] = modCache
                        extraData['tree'] = modCache.tree
                        extraData['node'] = node

                    # FIXME mods pass the parsed file's tree which means mods cannot
                    # reference nodes that have been added in the same <change>. To
                    # select modded contents, the selection must happen in a separate
                    # <change> command. Is that expected behavior?
                    node.encodeFromStr(specNode.value, True, extraData=extraData)
                else:
                    node.setData(bytes(baseLength))
            elif kind == 'head':
                node = BlockNode(specNode.title, specNode, 0, parent=None)
                node.parent = parentNode

                if pad := getattr(specNode, '@padding', None):
                    node.customPadding = bytes.fromhex(pad)
            elif kind == 'group':
                node = GroupNode('group', specNode, 0, parent=None)
                node.parent = parentNode
            else:
                raise UnknownSpecTypeError(f"error: unknown spec node type '{kind}'")

            if parentNode:
                node.localStartOffset = parentNode.getNewDataLength()
            else:
                node.localStartOffset = 0

            node.isMemoryBacked = False
            nodes += [node]

        return nodes

    @staticmethod
    def _makeChildrenNodes(parsedRoot: ParsedNode, loadValue: bool = False, modCache: 'ModCache' = None) -> Tuple[SpecNode]:
        if parsedRoot.specNode.kind == 'data':
            # data nodes cannot have children
            return []

        for specNode in parsedRoot.specNode.children:
            NewDataFactory._makeRootLevelNodesFromSpec(specNode, parentNode=parsedRoot, loadValue=loadValue, modCache=modCache)

        if parsedRoot.dataLength == 0 and not parsedRoot.children:
            if parsedRoot.specNode.allowEmpty:
                return []
            else:
                raise MissingDataError(f"block '{parsedRoot}' must not be empty but it is")

        return parsedRoot.children


class ModCache:
    def __init__(self, tree: ParsedNode = None, offsets={}, values={}):
        self._offsets: Dict[str, int] = offsets
        self._values: Dict[str, str] = values

        self.tree = tree
        self.baseOffset = 0
        self.currentSelects: Dict[str, [List[ParsedNode], str]] = {}

    def storeOffset(self, key, value):
        self._offsets[key] = self.baseOffset + value

    def getStoredOffset(self, key):
        return self._offsets[key]

    def storeValue(self, key, value):
        self._values[key] = value

    def getStoredValue(self, key, do_raise=True, default=None):
        if not do_raise:
            return self._values.get(key, default)
        else:
            # default is ignored in this case
            return self._values[key]

    def getReferencedNode(self, tree: ParsedNode, pathOrReference: str):
        if pathOrReference[0] == '@':
            ref = pathOrReference.split('/')[0]
            path = '/'.join(pathOrReference.split('/')[1:])

            if ref not in self.currentSelects or not self.currentSelects[ref]:
                raise ValueError(f'cannot get referenced node for {pathOrReference} - possibly a bug')

            if path:
                return self.currentSelects.get(ref, [])[0].getFirstPath(path)
            else:
                return self.currentSelects.get(ref, [])[0]

        return tree.getFirstPath(pathOrReference)


class Parser:
    def close(self):
        ParsedNode._dataBuffer = None
        self._dataBuffer.close()
        self._dataFileOpen.close()

    def __enter__(self):
        return contextlib.closing(self).__enter__()

    def __exit__(self, *args):
        pass

    def __init__(self, dataFile, structure: str = '', part: str = '', customStructure: str = '', customSpecFile: str = '',
                 progressCallback: ProgressCallback = noOpProgressCallback):
        if isinstance(dataFile, FilePath):
            dataFile = str(dataFile)

        self._dataFile = dataFile

        # Nodes will be added to this dict once they have been parsed. Each
        # node can only be parsed a single time because otherwise it would be
        # added multiple times to the tree.
        # - If a block node is in this list, its children can still be unparsed.
        # - If a node is detached from the tree, it will *not* be removed from this list
        #   because nodes live independently of the parser. However, nodes can/must be
        #   removed manually through forgetNode().
        # - The dict maps start offsets to parsed nodes
        self._finishedNodes: Dict[int, ParsedNode] = {}

        if customStructure:
            self._specDefinition = Spec.from_xml_string(customStructure)
        elif customSpecFile:
            self._specDefinition = Spec.from_custom_file(customSpecFile, part)
        elif structure:
            self._specDefinition = Spec.by_type(structure, part)
        else:
            self._specDefinition = Spec.by_magic(dataFile)

        log.info(f'determined file type: {self._specDefinition.title} ({self._specDefinition.ident})')
        self._specRoot = self._specDefinition.node

        self._dataFileOpen = open(self._dataFile, "rb")

        # memory-map the file, size 0 means whole file
        self._dataBuffer = mmap.mmap(self._dataFileOpen.fileno(), 0, access=mmap.ACCESS_READ)

        # prepare the output root node
        self.parsedRoot = ParsedNode('root', self._specRoot, self._dataBuffer.size(), parent=None)
        self.parsedRoot._dataBuffer = self._dataBuffer

        self.progressCallback = progressCallback

    @property
    def tree(self) -> ParsedNode:
        return self.parsedRoot

    @property
    def progressCallback(self) -> ProgressCallback:
        return self._progresser

    @progressCallback.setter
    def progressCallback(self, callback: ProgressCallback) -> None:
        if callback:
            self._progresser = callback
        else:
            self._progresser = noOpProgressCallback

    def _enforceNodeIsRelated(self, node: ParsedNode) -> bool:
        # NOTE: parsedParentNode must belong to the tree originally parsed with
        #       this parser. Everything else is undefined behavior. This applies
        #       to all parse*() methods.
        if not isinstance(node, ParsedNode):
            raise UnrelatedNodeError(f"cannot parse non-node value '{node}' with type '{type(node)}'")

        if node is not self.parsedRoot and \
                node not in self.parsedRoot.descendants:
            raise UnrelatedNodeError(f"cannot parse out-of-tree node '{node}', "
                                     f"nodes must be descendants of '{self.parsedRoot}'")
        return True

    def parsePath(self, path: [str, Path], parsedParentNode: BlockNode = None,
                  progressCallback: ProgressCallback = None) -> None:
        """ Parse at least everything matched by a path, below parsedParentNode.

            Important note: this will generally parse more than was requested, while still
            being much faster than parseFully(). This applies too when using index selectors
            (the highlighted part in "ANNO/FIGUREN/ENTRY*** / 5***").

            Use getPath() or getFirstPath() to extract a specific node after it has been
            parsed here.

            Note: if progressCallback is None, the parser's default callback will be
            used. To disable the callback for one call to parsePath(), set the argument
            to noOpProgressCallback. Set it to any callable to use a custom callback
            for one call to parsePath().
        """
        if not progressCallback:
            progressCallback = self.progressCallback

        if parsedParentNode is None:
            parsedParentNode = self.parsedRoot

        self._enforceNodeIsRelated(parsedParentNode)

        if str(path) == '/':
            return parsedParentNode

        path = Path(str(path))

        with ParseMode(progressCallback=progressCallback) as ctx:
            assert ctx  # use it to make pyflakes happy

            ctxCurrentlyParsedTotal.set(parsedParentNode.fullLength)
            original_path = Path(str(path))

            if not path.elements[-1].matchAllIdents:
                path.elements += [Path.NoneElement()]

            if not path:
                self._doParseLevel(parsedParentNode, skippedIsProgress=True)
                return
            elif path.match(parsedParentNode, matchSubset=True):
                currentLevelNodes = self._doParseLevel(parsedParentNode, keepPath=(path.elements[0], ), skippedIsProgress=True)
            else:
                return

            previousElem = path.elements[0]
            checkMatchedIndex = False
            elem = None

            for i, elem in enumerate(path.elements[1:]):
                if not currentLevelNodes:
                    break

                nextLevelNodes = []
                fullMatchCount = 0

                if elem.isLastElement and \
                    (not path.selector.matchAll
                        and path.selector.index >= 0):
                    checkMatchedIndex = True

                for node in currentLevelNodes:
                    idElem = Path.NoneElement() if previousElem.matchAllIdents else Path.Element(f'#{previousElem.identChild}')
                    parsedNodes = self._doParseLevel(node, keepPath=(elem, idElem), skippedIsProgress=True)

                    if not previousElem.identElement or previousElem.matchIdent2(node):
                        nextLevelNodes += parsedNodes

                    if checkMatchedIndex:
                        if original_path.match(node):
                            fullMatchCount += 1

                        if fullMatchCount > original_path.selector.index:
                            break

                previousElem = elem
                currentLevelNodes = nextLevelNodes

            if elem and isinstance(elem, Path.GlobElement):
                for node in currentLevelNodes:
                    self._doParseFully(node, progressCallback, resetProgress=False)

    def parseFully(self, parsedParentNode: BlockNode = None,
                   progressCallback: ProgressCallback = None) -> None:
        """ Parse the whole tree, starting at parsedParentNode.

        Important note: this may take some time. It is often preferred to use a more
        fine-grained approach with better performance by parsing only the parts that
        are actually needed. Use parsePath() to parse a specific spec path, or
        parseLevel() to parse only the immediate children of a given node.

        For small files, the performance impact of parseFully() is negligible.

        Note: if progressCallback is None, the parser's default callback will be
        used. To disable the callback for one call to parseFully(), set the argument
        to noOpProgressCallback. Set it to any callable to use a custom callback
        for one call to parseFully().
        """
        self._doParseFully(parsedParentNode, progressCallback, resetProgress=True)

    def _doParseFully(self, parsedParentNode: BlockNode = None,
                      progressCallback: ProgressCallback = None,
                      resetProgress: bool = True) -> None:
        if not progressCallback:
            progressCallback = self.progressCallback

        if parsedParentNode is None:
            parsedParentNode = self.parsedRoot

        self._enforceNodeIsRelated(parsedParentNode)

        with ParseMode(progressCallback=progressCallback, resetProgress=resetProgress) as ctx:
            assert ctx  # use it to make pyflakes happy

            if resetProgress:
                ctxCurrentlyParsedTotal.set(parsedParentNode.fullLength)
                ctxCurrentlyParsedBytes.set(0)

            children = self._doParseLevel(parsedParentNode, skippedIsProgress=False)

            while children:
                parsedChildren = []

                for node in children:
                    parsedChildren += self._doParseLevel(node, skippedIsProgress=False)

                children = parsedChildren

    def parseLevel(self, parsedParentNode: BlockNode = None,
                   progressCallback: ProgressCallback = None) -> list[ParsedNode]:
        """ Parse only immediate children of parsedParentNode.

        This loads one level, i.e. direct children of parsedParentNode. Nodes are
        added to the tree as children of parsedParentNode, and returned as a list
        of nodes too.

        Note: if progressCallback is None, the parser's default callback will be
        used. To disable the callback for one call to parseLevel(), set the argument
        to noOpProgressCallback. Set it to any callable to use a custom callback
        for one call to parseLevel().
        """
        if not progressCallback:
            progressCallback = self.progressCallback

        if parsedParentNode is None:
            parsedParentNode = self.parsedRoot

        self._enforceNodeIsRelated(parsedParentNode)

        with ParseMode(progressCallback=progressCallback) as ctx:
            assert ctx  # use it to make pyflakes happy

            ctxCurrentlyParsedTotal.set(parsedParentNode.fullLength)
            children = self._doParseLevel(parsedParentNode, skippedIsProgress=True)

        return children

    def forgetNode(self, node: ParsedNode) -> bool:
        found = {k: v for k, v in self._finishedNodes.items() if v is node}

        for k, v in found.items():
            # print("FORGETTING", k, v)
            del self._finishedNodes[k]

        return True if found else False

    def _markBytesParsed(self, count: int) -> None:
        ctxCurrentlyParsedBytes.set(ctxCurrentlyParsedBytes.get() + count)
        ctxProgressCallback.get()(ctxCurrentlyParsedBytes.get() / ctxCurrentlyParsedTotal.get() * 100)

    def _doParseLevel(self, parsedRoot: BlockNode, keepPath: Tuple[Path.Element] = [],
                      skippedIsProgress: bool = True):
        """
        Load one level, i.e. children of parsedRoot.

        Important note: this does NOT enable a parsing context. This method MUST
        be called in a ParseMode context!
        """
        # self._dataBuffer.seek(parsedRoot.globalStartOffset, os.SEEK_SET)

        # Use the progress callback defined through the current parsing context
        # instead of the default progress callback defined on this parser.
        # The caller must specify if the default should be used when creating
        # the parsing context.
        # progressCallback = ctxProgressCallback.get()

        if not parsedRoot.isMemoryBacked:  # TODO properly determine if a node has been created without being parsed
            return parsedRoot.children  # assume this node is user-created and thus already fully parsed
        else:
            # print(parsedRoot)
            parsedRoot.seek(0, os.SEEK_SET)
            # progressCallback(self._dataBuffer.tell() / self._dataBuffer.size() * 100)

        if parsedRoot.specNode.kind == 'data':
            # data nodes cannot have children
            return []

        if parsedRoot.dataLength == 0:
            if parsedRoot.specNode.allowEmpty:
                return []
            else:
                raise MissingDataError(f"block '{parsedRoot}' must not be empty but it is")

        for specNode in parsedRoot.specNode.children:
            kind = specNode.kind

            if kind == 'file':
                continue

            # print("==>", specNode, self._finishedNodes.get(self._dataBuffer.tell()), self._dataBuffer.tell())
            # print(self._finishedNodes.get(self._dataBuffer.tell()) == specNode, self._checkAlreadyParsed(specNode, self._dataBuffer.tell()))

            # continueWithNextSpec = False
            # while already := self._checkAlreadyParsed(specNode, self._dataBuffer.tell(), doRaise=False):
            #     print("SKIPPING AT", self._dataBuffer.tell(), specNode, already)
            #
            #     if already.specNode is specNode:
            #         parsedRoot.seek(already.localStartOffset + already.dataLength, os.SEEK_CUR)
            #         continue
            #     else:
            #         continueWithNextSpec = True
            #         break
            #
            # if continueWithNextSpec:
            #     continue

            if not keepPath or [x for x in keepPath if x and x.matchTag(specNode)]:
                toKeep = True
                # print("keeping:", keepPath, specNode)
            else:
                toKeep = False
                # print("not keeping:", keepPath, specNode)

            count = specNode.count  # parsed in _postProcessXmlNode

            if kind == 'data':
                self._parseData(count, specNode, parsedRoot, toKeep, skippedIsProgress)
            elif kind == 'head':
                self._parseBlock(count, specNode, parsedRoot, toKeep, skippedIsProgress)
            elif kind == 'group':
                self._parseGroup(count, specNode, parsedRoot, toKeep, skippedIsProgress)
            elif kind != 'file':
                raise UnknownSpecTypeError(f"error: unknown spec node type '{kind}'")

        # progressCallback(self._dataBuffer.tell() / self._dataBuffer.size() * 100)
        return parsedRoot.children

    def _checkAlreadyParsed(self, specNode: SpecNode, globalOffset: int, doRaise: bool = True) -> [ParsedNode, None]:
        if globalOffset in self._finishedNodes:
            # print("IGNORING AT", globalOffset, specNode, self._finishedNodes[globalOffset])
            if doRaise and self._finishedNodes[globalOffset].specNode is not specNode:
                existing = self._finishedNodes[globalOffset]

                if existing.specNode.kind == 'group' and \
                        existing.specNode.children and existing.specNode.children[0] is specNode:
                    # Groups have no actual data so they share the starting globalOffset
                    # with their first child, so if the group node is stored here, it means that
                    # the child node was not parsed yet. The child node will overwrite the
                    # entry when it gets parsed.
                    return None  # "go ahead with parsing"
                elif existing.specNode.kind == 'data' and \
                        existing.index == 0 and existing.parent and \
                        existing.parent.specNode.kind == 'group' and \
                        existing.parent.specNode is specNode:
                    # If a group's first child is already parsed, then we can safely
                    # assume its parent group has been parsed as well.
                    return existing.parent  # "we already parsed this"
                elif specNode.kind == 'data' and \
                        specNode.length == math.inf:
                    # If a data node has variable length and is empty, then it can
                    # share its offset with the next node. In this case, we allow
                    # it to be parsed, assuming that it will not actually parse
                    # anything.
                    # FIXME these assumptions are probably wrong...
                    # FIXME won't this maybe cause nodes to be duplicated?
                    return None  # "go ahead with parsing"
                else:
                    raise MemoryError(f"attempted to read '{specNode}' at global {globalOffset}, "
                                      f"where '{self._finishedNodes[globalOffset].specNode}' was already parsed")

        return self._finishedNodes.get(globalOffset, None)

    def _parseData(self, count: SpecNode.Interval, specNode: SpecNode, parsedParentNode: BlockNode,
                   toKeep: bool, skippedIsProgress: bool = True, calculateActualSize: bool = False) -> None:
        found = 0
        length = specNode.length  # parsed in _postProcessXmlNode

        if length == math.inf:
            if calculateActualSize:
                if specNode.typeStr in ['cstring', 'pstring']:
                    backOffset = parsedParentNode.tell()
                    v = decoders.bin2py(parsedParentNode.dataView, specNode)
                    length = len(decoders.py2bin(v, specNode))
                    parsedParentNode.seek(backOffset, os.SEEK_SET)
                    log.debug(f"determined length: {length}, {v}, {backOffset}, {parsedParentNode.tell()}")
                else:
                    raise Exception('todo: add proper exception')
            else:
                length = parsedParentNode.dataLength - parsedParentNode.tell()

        while found < count.max:
            localStartOffset = parsedParentNode.tell()

            try:
                parsedParentNode.seek(length, os.SEEK_CUR)
            except OutOfBoundsError:
                if found < count.min:
                    raise MissingDataError(f"expected '{length}' more bytes of data at '{localStartOffset}' "
                                           f"in '{parsedParentNode}' for '{count}' instances of '{specNode}', "
                                           f"got {parsedParentNode.dataLength - parsedParentNode.tell()}")
                break

            globalOffset = self._dataBuffer.tell() - length

            if toKeep:
                doParseNode = True

                if existingNode := self._checkAlreadyParsed(specNode, globalOffset):
                    if existingNode.specNode.kind == 'group' and \
                            existingNode.specNode == parsedParentNode.specNode:
                        # The existing node at this offset is a group. As groups are invisible
                        # and share their start offset with their first child, we parse the
                        # current node and store it at this offset.
                        doParseNode = True
                    elif existingNode.specNode == specNode:
                        doParseNode = False

                        # if self._finishedNodes[globalOffset].parent is not parsedParentNode:
                        #     log.warn(f"existing parsed node at offset {globalOffset} has a different parent, "
                        #              f"reparenting {self._finishedNodes[globalOffset].parent} to {parsedParentNode}")

                        self._finishedNodes[globalOffset].parent = parsedParentNode
                    elif existingNode.specNode.kind == 'data' and \
                            existingNode.specNode.length == math.inf and \
                            existingNode.dataLength == 0:
                        # The existing node is an empty variable-length data block,
                        # so it can share its offset with the next node.
                        # FIXME We should store both nodes but that is not possible
                        # with the current architecture.
                        doParseNode = True
                    else:
                        doParseNode = False
                        raise BlockMismatchError(f"tried to parse {specNode} at offset {globalOffset} where {existingNode} was already parsed")

                if doParseNode:
                    parsed = DataNode('data', specNode, length, parent=None)
                    parsed.parent = parsedParentNode
                    parsed.localStartOffset = localStartOffset

                    self._finishedNodes[globalOffset] = parsed

            if toKeep or skippedIsProgress:
                self._markBytesParsed(parsedParentNode.tell() - localStartOffset)

            found += 1

    def _parseBlock(self, count: SpecNode.Interval, specNode: SpecNode, parsedParentNode: BlockNode,
                    toKeep: bool, skippedIsProgress: bool = True) -> None:
        found = 0

        while found < count.max:
            i = parsedParentNode.checkAhead(specNode)

            if i.ok:
                found += 1

                globalOffset = self._dataBuffer.tell()

                if toKeep:
                    doParseNode = True

                    if existingNode := self._checkAlreadyParsed(specNode, globalOffset):
                        if existingNode.specNode == specNode:
                            # if self._finishedNodes[globalOffset].parent is not parsedParentNode:
                            #     log.warn(f"existing parsed node at offset {globalOffset} has a different parent, "
                            #              f"reparenting {self._finishedNodes[globalOffset].parent} to {parsedParentNode}")

                            doParseNode = False
                            self._finishedNodes[globalOffset].parent = parsedParentNode
                        elif existingNode.specNode.kind == 'data' and \
                                existingNode.specNode.length == math.inf and \
                                existingNode.dataLength == 0:
                            # The existing node is an empty variable-length data block,
                            # so it can share its offset with the next node.
                            # FIXME We should store both nodes but that is not possible
                            # with the current architecture.
                            doParseNode = True
                        else:
                            doParseNode = False
                            raise BlockMismatchError(f"tried to parse {specNode} at offset {globalOffset} where {existingNode} was already parsed")

                    if doParseNode:
                        parsed = BlockNode(specNode.title, specNode, i.bodyLength, parent=None)
                        parsed.parent = parsedParentNode
                        parsed.localStartOffset = parsedParentNode.tell()
                        parsed.customPadding = i.customPadding

                        self._finishedNodes[globalOffset] = parsed

                parsedParentNode.seek(i.headLength, os.SEEK_CUR)
                parsedParentNode.seek(i.bodyLength, os.SEEK_CUR)

                if skippedIsProgress:
                    self._markBytesParsed(i.headLength + i.bodyLength)
                else:
                    self._markBytesParsed(i.headLength)
            elif found < count.min:
                raise MissingBlockError(f"could not find required block '{specNode}' at '{parsedParentNode.tell()}'")
            else:
                break

        return parsedParentNode

    def _parseGroup(self, count: SpecNode.Interval, specNode: SpecNode, parsedParentNode: BlockNode,
                    toKeep: bool, skippedIsProgress: bool = True) -> None:
        found = 0
        groupLength = GroupNode._calculateLength(specNode)

        while found < count.max:
            if parsedParentNode.dataLength - parsedParentNode.tell() < groupLength.min:
                if found < count.min:
                    raise MissingDataError(f"could not find required group '{specNode}' at '{parsedParentNode.tell()}' (got {found} of {count.min} instances)")
                else:
                    break

            log.debug(f'left: {parsedParentNode.dataLength - parsedParentNode.tell()}')

            globalOffset = self._dataBuffer.tell()

            if toKeep:
                doParseNode = True

                if existingNode := self._checkAlreadyParsed(specNode, globalOffset):
                    # Group nodes are not stored in self._finishedNodes because they
                    # are invisible, i.e. they share their start offset with their
                    # first child. Therefore, we have to check if the node that exists
                    # at this offset is a child of the same group that we are parsing now.
                    #
                    # Exception: before the first child is parsed, the offset *is* occupied
                    # by the group node itself. We have to check for this occasion.

                    if existingNode.specNode == specNode:
                        # The existing node is the same node that we are parsing now.
                        # No children have been parsed yet.
                        doParseNode = False
                    elif existingNode.parent.specNode == specNode:
                        # The existing node is the first child of the group we are
                        # parsing now.
                        doParseNode = False
                    elif existingNode.specNode.kind == 'data' and \
                            existingNode.specNode.length == math.inf and \
                            existingNode.dataLength == 0:
                        # The existing node is an empty variable-length data block,
                        # so it can share its offset with the next node.
                        # FIXME We should store both nodes but that is not possible
                        # with the current architecture.
                        doParseNode = True
                    else:
                        # The existing node at this offset is not a child of the group
                        # we are currently parsing. There must be something wrong.
                        doParseNode = False
                        raise BlockMismatchError(f"tried to parse group {specNode} at offset "
                                                 f"{globalOffset} where {existingNode} was already parsed")

                if doParseNode:
                    # Nothing has been parsed yet at this offset, so we store a reference
                    # to the group node itself. Once a child gets parsed, it will take
                    # over the place.

                    log.debug(f"do parse group: {min(parsedParentNode.dataLength - parsedParentNode.tell(), groupLength.max)}")

                    maxGroupLength = min(parsedParentNode.dataLength - parsedParentNode.tell(), groupLength.max)
                    parsedGroup = GroupNode('group', specNode, maxGroupLength, parent=None)
                    parsedGroup.parent = parsedParentNode
                    parsedGroup.localStartOffset = parsedParentNode.tell()

                    self._finishedNodes[globalOffset] = parsedGroup

            # NOTE: Recursive parsing breaks if the group's children are already
            #       parsed here, which means groups must be parsed as if they were
            #       normal blocks, which in turn means that groups must be parsed
            #       as a separate level.
            #
            # WARN: If the group has dynamic-size children though, we have to parse
            #       it anyway to determine its size.

            log.debug(f"parsing group {specNode} in {parsedParentNode}")

            if groupLength.min != groupLength.max:
                log.debug(f"parsing variable-length group {specNode}")

                backOffset = parsedParentNode.tell()
                log.debug(f"parent: {backOffset} / {parsedParentNode.tell()} / {parsedParentNode.fullLength}, group: {parsedGroup.tell()}, {groupLength.min}")

                # parsedParentNode.seek(groupLength.min, os.SEEK_CUR)
                log.debug(f"now actually at {parsedParentNode.tell()}")

                # log.debug(parsedGroup.dataView[parsedGroup.tell():parsedGroup.tell() + parsedGroup.dataLength].tobytes().decode('ascii'))
                index = parsedParentNode.dataView[parsedParentNode.tell() + groupLength.min:].tobytes().find(0x00)
                log.debug(">> " + parsedParentNode.dataView[parsedParentNode.tell()
                                                            + groupLength.min:groupLength.min
                                                            + parsedParentNode.tell()
                                                            + index].tobytes().decode('ascii'))

                if index >= 0:
                    # parsedGroup.seek(groupLength.min + index, os.SEEK_SET)
                    actualLength = groupLength.min + index + 1
                    parsedGroup.dataLength = actualLength
                    log.debug(f">> index: {index}, {groupLength.min}+{index}={groupLength.min+index}")
                else:
                    raise Exception()

                # for child in specNode.children:
                #     if child.type_ == 'cstring':
                #         chunksize = 50
                #         index = -1
                #
                #         for i, chunk in enumerate(tools.chunks(parsedGroup.dataView[parsedGroup.tell():], chunksize, parsedGroup.dataLength)):
                #             index = chunk.tobytes().find(0x00)
                #
                #             if index >= 0:
                #                 index = i * chunksize + index
                #                 parsedGroup.seek(index, os.SEEK_CUR)
                #
                #                 log.debug(parsedGroup.dataView[parsedGroup.tell():parsedGroup.tell()+index].tobytes().decode('ascii'))
                #
                #                 break
                #
                #         if index < 0:
                #             raise Exception('issue')
                #     else:
                #         self._parseData(child.count, child, parsedGroup, toKeep=False, calculateActualSize=False)

                # log.debug(f"parent: {backOffset} / {parsedParentNode.tell()} / {parsedParentNode.fullLength}, group: {parsedGroup.tell()}")

                # actualLength = parsedGroup.tell()
                # parsedGroup.dataLength = actualLength

                log.debug(f"parsed group and determined length {actualLength}, allowed range: {groupLength.min}-{groupLength.max}")
                # log.debug(f"kids: {parsedGroup.children[1].decodeToPy()}")

                # for child in parsedGroup.children:
                #     self.forgetNode(child)
                #
                # parsedGroup._resetChildren(parsedGroup, 0, [], [], [])

                # parsedParentNode.seek(backOffset, os.SEEK_SET)
            else:
                actualLength = groupLength.max

            parsedParentNode.seek(actualLength, os.SEEK_CUR)
            log.debug(f"now at {parsedParentNode.globalStartOffset + parsedParentNode.tell()}")

            if skippedIsProgress:
                self._markBytesParsed(actualLength)

            found += 1
