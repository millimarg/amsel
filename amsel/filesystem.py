# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import itertools
import errno
import stat
import enum
import re
import contextlib
from collections import defaultdict
from collections import namedtuple
from pathlib import Path as FilePath
from functools import cache
from typing import Dict, List

import fuse

from .kaitai_simple_fs import KaitaiSimpleFS
from .parser import ParsedNode
from .parser import Parser
from .spec import Spec
from .graphics import Palette
from .graphics import Image
# from .graphics import Animation


def mount(mountpath, filename, structure: str = '', part: str = '', customStructure: str = '', customSpecFile: str = ''):
    if customStructure:
        spec = Spec.from_xml_string(customStructure).node
    elif customSpecFile:
        spec = Spec.from_custom_file(customSpecFile, part).node
    elif structure:
        spec = Spec.by_type(structure, part).node
    else:
        spec = Spec.by_magic(filename).node

    if not spec:
        raise Exception("failed to determine file type")

    magic = getattr(spec, '@magic', '')

    if magic ==   '16|46 49 47 55 52 45 4E 00 00 00 00 00':
        print("--- Figuren file")
        fsClass = FigurenFS
    elif magic == '16|43 4F 4C 4F 52 53 00 00 00 00 00 00':
        print("--- Anlagen file")
        fsClass = AnlagenFS
    elif magic == '16|43 4F 4C 4F 52 00 00 00 00 00 00 00':
        print("--- Textures file")
        fsClass = TexturesFS
    else:
        print("--- Any file")
        fsClass = AnnoFS

    with fsClass(dataFile=filename, structure=structure,
                 part=part, customStructure=customStructure,
                 customSpecFile=customSpecFile) as fs:
        fuse.FUSE(fs, mountpath, nothreads=True, foreground=True)


class BytesWriter:
    def __init__(self, binaryInput=True):
        self._binaryInput = binaryInput

        if self._binaryInput:
            self._data = bytearray()
        else:
            self._data = ''

    def write(self, data):
        self._data += data

    @property
    def data(self):
        if self._binaryInput:
            return bytes(self._data)
        else:
            return bytes(self._data, 'utf8')

    @property
    def length(self):
        return len(self._data)


class AnnoFS(KaitaiSimpleFS):
    class _Kind(enum.Enum):
        File = 1
        Dir = 2

    class _Group:
        # custom groups should use ints as value
        Raw = 'Raw'
        DirectoryFile = 'DirectoryFile'

    _Element = namedtuple('Element', 'kind group node')

    DirectoryFile = """
[Dolphin]
PreviewsShown=false
Timestamp=2024,1,1,0,0,0.000
Version=4
ViewMode=2
VisibleRoles=Icons_text,Icons_size

[Settings]
HiddenFilesShown=true
    """

    def close(self):
        for k, v in self._parsers.items():
            v.close()

    def __enter__(self):
        return contextlib.closing(self).__enter__()

    def __exit__(self, *args):
        pass

    def __init__(self, dataFile, structure: str = '', part: str = '', customStructure: str = '', customSpecFile: str = ''):
        self._dataFile = dataFile
        self._parsers = {}
        self._parsedNodes = {}

        self._parserArgs = {
            'dataFile': dataFile,
            'structure': structure,
            'part': part,
            'customStructure': customStructure,
            'customSpecFile': customSpecFile,
        }

        self._register_parser(self._Group.Raw, Parser(**self._parserArgs))
        self._setup_custom_properties()

        self.obj = self._parsers[self._Group.Raw].parsedRoot
        super(AnnoFS, self).__init__()

        stat_info = FilePath('/').stat()
        self.stat_times = {
            "st_atime": stat_info.st_atime,
            "st_mtime": stat_info.st_mtime,
            "st_ctime": stat_info.st_ctime,
        }

        if not hasattr(self, '_directoryFiles'):
            self._directoryFiles = {}

    def _setup_custom_properties(self):
        # reimplement this function in derived classes to setup custom properties
        # that are later used in, e.g., generate_tree().
        return

    def _setup_root_tree(self):
        # Do not overwrite this method. Call it from generate_tree() before
        # anything else.
        print("building tree...")
        self._setup_group_dirs()
        self._load_tree_level_for_group(self._Group.Raw, self.obj)
        print("treeing done")

    def generate_tree(self):
        # Overwrite this method and do not call the super class implementation.
        # Instead, call self._setup_root_tree() before anything else.
        self._setup_root_tree()

    def _register_parser(self, key: _Group, parser: Parser) -> Parser:
        # Use Parser(**self._parserArgs) to open a new parser for the main file.
        self._parsers[key] = parser
        self._parsedNodes[key] = {}
        return parser

    def _add_directory_file(self, atPath: str):
        """ Add a .directory file. To be called from implementations. """
        if not hasattr(self, '_directoryFiles'):
            self._directoryFiles = {}

        if atPath in self._directoryFiles:
            return

        self.add_obj_to_path(atPath.split('/') + ['.directory'], self._Element._make((self._Kind.File, self._Group.DirectoryFile, None)))
        self._directoryFiles[atPath] = 1

    def _setup_group_dirs(self) -> None:
        self.add_obj_to_path(['raw'], self._Element._make((self._Kind.Dir, self._Group.Raw, None)))

    def _load_tree_level_for_group(self, group, node) -> bool:
        if group == self._Group.Raw:
            self._load_raw_tree_level(node)
            return True
        elif group == self._Group.DirectoryFile:
            return True  # nothing to load, as this groups must not hold folders

        return False

    def _get_size_for_group(self, group, node) -> int:
        raise NotImplementedError("must be implemented in subclasses")

    def _get_body_for_group(self, group, node, offset, length) -> bytes:
        raise NotImplementedError("must be implemented in subclasses")

    def _load_raw_tree_level(self, node):
        print("building tree from children...")

        if not hasattr(node, 'children'):
            print(f"nothing to tree for {node}")
            return

        if node in self._parsedNodes[self._Group.Raw]:
            print("already done")
            return
        else:
            print("- parsing level...")
            self._parsers[self._Group.Raw].parseLevel(node)
            self._parsedNodes[self._Group.Raw][node] = True
            print("- parsed")

        children = node.children

        specials = []  # [x for x in children if x.specNode.kind == 'head' and x.name == 'TEXTURE']
        dirs = [x for x in children if x.specNode.kind in ('file', 'head', 'group') and x not in specials]
        files = [x for x in children if x not in dirs and x not in specials]

        for node in files:
            if node.specNode.kind == 'data':
                self.add_obj_to_path(self._build_path('raw', node, '.dat'), self._Element._make((self._Kind.File, self._Group.Raw, node)))
            else:
                print(f"invalid data ignored, type: {node.specNode.kind}")

        for node in dirs:
            self.add_obj_to_path(self._build_path('raw', node, ''), self._Element._make((self._Kind.Dir, self._Group.Raw, node)))

        for node in specials:
            lines = [bytes(x + '\n', 'utf8') for x in node.bakeXml()]
            node.xmldata = bytes([x for x in itertools.chain(*lines)])

            self.add_obj_to_path(self._build_path('raw', node, '.xml'), self._Element._make((self._Kind.Special, self._Group.Raw, node)))

        print("treeing done")

    @cache
    def _build_path(self, prefix, node: ParsedNode, suffix):
        name = self._format_name(node, suffix)
        path = [self._format_name(x, '') for x in node.path[:-1]]
        return [prefix] + path + [name]

    @cache
    def _format_name(self, node: ParsedNode, suffix):
        if node.specNode.ident:
            name = f"{node.index:03} {node.name} ({node.specNode.ident}){suffix}"
        else:
            name = f"{node.index:03} {node.name}{suffix}"

        return name

    def get_file_attrs(self, obj: _Element):
        permissions = stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH

        if obj.kind == self._Kind.Dir:
            file_type = stat.S_IFDIR
            size = 4096

            if obj.group == self._Group.Raw:
                self._load_raw_tree_level(obj.node)
            else:
                self._load_tree_level_for_group(obj.group, obj.node)
        else:
            file_type = stat.S_IFREG

            if obj.group == self._Group.Raw:
                size = obj.node.dataLength
            elif obj.group == self._Group.DirectoryFile:
                size = len(self.DirectoryFile)
            else:
                size = self._get_size_for_group(obj.group, obj.node)

        mode = file_type | permissions

        return {
            **self.stat_times,
            "st_nlink": 1,
            "st_mode": mode,
            "st_size": size,
            "st_gid": 0,
            "st_uid": 0,
        }

    def get_file_body(self, obj: _Element, offset, length):
        if obj.kind == self._Kind.Dir:
            raise fuse.FuseOSError(errno.EISDIR)

        if obj.group == self._Group.Raw:
            if obj.node.specNode.kind == 'data':
                return bytes(obj.node.dataView[offset:offset + length])
        elif obj.group == self._Group.DirectoryFile:
            return self.DirectoryFile[offset:offset + length]
        else:
            return self._get_body_for_group(obj.group, obj.node, offset, length)


class FigurenFS(AnnoFS):
    class _Group(AnnoFS._Group):
        ByPath = 1
        ByIdent = 2
        ByFlatPath = 3

        Images = 'img'

    _ByGroups: List[_Group] = frozenset([_Group.ByPath, _Group.ByIdent, _Group.ByFlatPath])
    _BasePaths: Dict[_Group, str] = {
        _Group.ByPath: 'by-path',
        _Group.ByIdent: 'by-id',
        _Group.ByFlatPath: 'by-path-flat',
    }

    _FileNode = namedtuple('FileNode', 'filename node')
    _ImagesNode = namedtuple('ImagesNode', 'basepath node')
    _ImageHeader = namedtuple('ImageHeaderNode', 'textures index')
    _DecodedImage = namedtuple('DecodedImage', 'length png')

    _ParsePaths = (
        'ANNO/FIGUREN/ENTRY/PATH/data',
        'ANNO/FIGUREN/ENTRY/#entry-id',
        'ANNO/TEXTURES/#zoom-level-id',
        'ANNO/TEXTURES/#data-entries-count',
        'ANNO/TEXTURES/#image-data',
    )
    _EntryPath = 'ANNO/FIGUREN/ENTRY'
    _EntryIdPath = '#entry-id'
    _EntryPathPath = 'PATH/data'
    _EntryParent = 'FIGUREN'

    def _setup_custom_properties(self):
        self.entriesByPath: Dict[str, List[self._FileNode]] = defaultdict(list)
        self.entryXmlData: Dict[str, bytes] = {}
        self._decodedImages: Dict[str, self._DecodedImage] = {}

    def _getPathForEntry(self, entry: ParsedNode) -> str:
        path = entry.getFirstPath(self._EntryPathPath + " / 1")

        if path:
            return path.decodeToPy().replace('\\', '/').strip('/')
        else:
            return ''

    def _loadImagesForEntry(self, node: _ImagesNode) -> None:
        entry = node.node

        if entry.parent.specNode.title == self._EntryParent:
            self.byPath.parsePath('HAEUSER/ENTRY/GFXDATA/#original-file-name', entry)
            self.byPath.parsePath('HAEUSER/ENTRY/TEXTURES/#zoom-level-id', entry)
            self.byPath.parsePath('HAEUSER/ENTRY/SHADOW/GFXDATA/#original-file-name', entry)
            self.byPath.parsePath('HAEUSER/ENTRY/SHADOW/TEXTURES/#zoom-level-id', entry)
            self.byPath.parsePath('COLORS/ENTRY/*', entry)

            for anim in entry.getPath('HAEUSER/ENTRY / *'):
                path = anim.getFirstPath('GFXDATA/#original-file-name').decodeToPy().replace('\\', '/')
                path = node.basepath + path.split('/')

                self.add_obj_to_path(path, self._Element._make((self._Kind.Dir, self._Group.Images,
                                     self._ImagesNode._make([path, anim]))))

                if shadow := anim.getFirstPath('SHADOW'):
                    path = shadow.getFirstPath('GFXDATA/#original-file-name').decodeToPy().replace('\\', '/')
                    path = node.basepath + ['_ shadow'] + path.split('/')

                    self.add_obj_to_path(path, self._Element._make((self._Kind.Dir, self._Group.Images,
                                         self._ImagesNode._make([path, shadow]))))

        elif entry.specNode.title == 'TEXTURES':
            # heads = [x.decodeToPy() for x in entry.getPath('#image-header / *')]
            # self.byPath.parsePath('#texture-count', entry)
            self.byPath.parseFully(entry)
            count = entry.getFirstPath('#texture-count').decodeToPy()

            isShadow = (entry.parent.specNode.title == 'SHADOW')

            for i in range(1, count + 1):
                for path in (node.basepath + [f'{i:04d}{"_s" if isShadow else ""}.png'], node.basepath + [f'{i:04d}{"_s" if isShadow else ""}.txt']):
                    self.add_obj_to_path(path, self._Element._make((self._Kind.File, self._Group.Images,
                                         self._FileNode._make(['/'.join(path), self._ImageHeader._make([entry, i])]))))

        elif entry.parent.specNode.title in ('HAEUSER', 'ENTRY'):
            tex = entry.getPath('TEXTURES / *')

            if not tex:
                print("no images in", '/'.join(node.basepath))
                return

            levels = {
                0: '1 - close',
                1: '2 - medium',
                2: '3 - far',
            }

            for i in tex:
                lvl = i.getFirstPath('#zoom-level-id').decodeToPy()
                path = node.basepath + [levels[lvl]]
                self.add_obj_to_path(path, self._Element._make((self._Kind.Dir, self._Group.Images,
                                     self._ImagesNode._make([path, i]))))

    def _decodeImage(self, node: _FileNode):
        if node.filename in self._decodedImages:
            return

        # node.node is self._ImageHeader
        tex: ParsedNode = node.node.textures
        index: int = node.node.index
        zoom = tex.getFirstPath('#zoom-level-id').decodeToPy()
        head = tex.getPath(f'#image-header / {index}').decodeToPy()

        block = self.byPath.tree.getPath(f'ANNO/TEXTURES@zoom-level-id:{zoom}/#image-data / 1')
        body = block.decodeToPy()[head.data_offset:head.data_offset + head.data_length]

        if tex.parent.parent.parent.specNode.title == 'HAEUSER':
            # it's a shadow node, go one more step up
            entry = tex.parent.parent.parent.parent
        else:
            entry = tex.parent.parent.parent

        colors = entry.getFirstPath(f'COLORS/ENTRY@palette-id:{head.palette_id}/BODY/#palette-entries').decodeToPy()
        palette = Palette(head.palette_id, '', colors)

        img = Image(head, body, palette, node.filename)

        try:
            data = BytesWriter()
            img.get_png().write(data)
        except Exception:
            print(f'failed to read image {node.filename}')

        iniData = img.to_ini(getBytes=True)

        name = re.sub(r'\.(png|txt)$', '', node.filename)
        self._decodedImages[name + '.png'] = self._DecodedImage._make([data.length, data.data])
        self._decodedImages[name + '.txt'] = self._DecodedImage._make([len(iniData), iniData])

    def generate_tree(self):
        self._setup_root_tree()

        self.byPath = self._register_parser(self._Group.ByPath, Parser(**self._parserArgs))

        for i in self._ParsePaths:
            self.byPath.parsePath(i)

        entries = self.byPath.tree.getPath(self._EntryPath + " / *")

        for entry in entries:
            entryId = entry.getFirstPath(self._EntryIdPath + " / 1").decodeToPy()
            string = self._getPathForEntry(entry)
            xmlName = f'{entryId:04} {string.split("/")[-1]}.xml'

            paths = {
                self._Group.ByPath: string,
                self._Group.ByIdent: f'{entryId:04} {string.split("/")[-1]}/{"/".join(string.split("/")[:-1]).replace("/", " - ")}'.strip('/'),
                self._Group.ByFlatPath: string.replace("/", " - "),
            }

            print("loaded:", paths[self._Group.ByIdent])

            for group in self._ByGroups:
                self.add_obj_to_path([self._BasePaths[group]] + paths[group].split('/'), self._Element._make((self._Kind.Dir, group, paths[group])))
                self.entriesByPath[paths[group]].append(self._FileNode._make((xmlName, entry)))

    def _setup_group_dirs(self):
        super(FigurenFS, self)._setup_group_dirs()

        for group in self._ByGroups:
            self.add_obj_to_path([self._BasePaths[group]], self._Element._make((self._Kind.Dir, group, None)))
            self._add_directory_file(self._BasePaths[group])

    def _load_tree_level_for_group(self, group, node):
        if super(FigurenFS, self)._load_tree_level_for_group(group, node):
            return

        if not node:
            return

        if group in self._ByGroups:
            # node is a path string
            for i in self.entriesByPath[node]:
                path = [self._BasePaths[group]] + node.split('/') + [i.filename]
                imgPath = [self._BasePaths[group]] + node.split('/') + [i.filename[:-4] + ' images']
                self.add_obj_to_path(path, self._Element._make((self._Kind.File, group, i)))
                self.add_obj_to_path(imgPath, self._Element._make((self._Kind.Dir, self._Group.Images, self._ImagesNode._make([imgPath, i.node]))))
        elif group == self._Group.Images:
            # node is a _ImagesNode
            self._loadImagesForEntry(node)

    def _get_size_for_group(self, group, node: _FileNode):
        if group in self._ByGroups:
            if node.filename in self.entryXmlData:
                return len(self.entryXmlData[node.filename])

            self.byPath.parseFully(node.node)
            lines = [bytes(x + '\n', 'utf8') for x in node.node.bakeXml()]
            self.entryXmlData[node.filename] = bytes([x for x in itertools.chain(*lines)])
            return len(self.entryXmlData[node.filename])
        elif group == self._Group.Images:
            self._decodeImage(node)
            return self._decodedImages[node.filename].length

        return 4096

    def _get_body_for_group(self, group, node, offset, length):
        if group in self._ByGroups:
            return self.entryXmlData[node.filename][offset:offset + length]
        elif group == self._Group.Images:
            return self._decodedImages[node.filename].png[offset:offset + length]


class AnlagenFS(FigurenFS):
    _ParsePaths = (
        'ANNO/ANLAGEN/ENTRY/HAEUSER/ENTRY/TITLE/data',
        'ANNO/ANLAGEN/ENTRY/PRODTYPE/data',
        'ANNO/ANLAGEN/ENTRY/WARE/data',
        'ANNO/ANLAGEN/ENTRY/COLORS/#mapped-palette-id',
        'ANNO/ANLAGEN/ENTRY/#entry-id',
        'ANNO/TEXTURES/#zoom-level-id',
        'ANNO/TEXTURES/#data-entries-count',
        'ANNO/TEXTURES/#image-data',
        'ANNO/COLORS/ENTRY/BODY/#palette-entries',
        'ANNO/COLORS/ENTRY/#palette-id',
    )
    _EntryPath = 'ANNO/ANLAGEN/ENTRY'
    _EntryIdPath = '#entry-id'
    _EntryPathPath = 'HAEUSER/ENTRY/TITLE/data'
    _EntryParent = 'ANLAGEN'

    def _getPathForEntry(self, entry: ParsedNode) -> str:
        # The TITLE element is not mandatory for Anlagen entries. If it is missing,
        # we build a path from the PRODTYPE and WARE elements.

        if path := super(AnlagenFS, self)._getPathForEntry(entry):
            return path

        # prodtype = '/'.join(sorted(entry.getFirstPath('PRODTYPE/data').decodeToPy()))
        prodtype = entry.getFirstPath('PRODTYPE/data').decodeToPy()  # single item
        ware = '/'.join(sorted(entry.getFirstPath('WARE/data').decodeToPy()))  # list

        return prodtype + '/' + ware

    def _decodeImage(self, node: FigurenFS._FileNode):
        if node.filename in self._decodedImages:
            return

        # node.node is self._ImageHeader
        tex: ParsedNode = node.node.textures
        index: int = node.node.index
        zoom = tex.getFirstPath('#zoom-level-id').decodeToPy()
        head = tex.getPath(f'#image-header / {index}').decodeToPy()

        block = self.byPath.tree.getPath(f'ANNO/TEXTURES@zoom-level-id:{zoom}/#image-data / 1')
        body = block.decodeToPy()[head.data_offset:head.data_offset + head.data_length]

        if tex.parent.parent.parent.specNode.title == 'HAEUSER':
            # it's a shadow node, go one more step up
            entry = tex.parent.parent.parent.parent
        else:
            entry = tex.parent.parent.parent

        mappedPaletteId = entry.getPath(f'COLORS/#mapped-palette-id / {head.palette_id + 1}').decodeToPy()

        if mappedPaletteId > 0:
            colors = self.byPath.tree.getFirstPath(f'ANNO/COLORS/ENTRY@palette-id:{mappedPaletteId}/BODY/#palette-entries').decodeToPy()
        else:
            colors = None

        palette = Palette(head.palette_id, '', colors)
        img = Image(head, body, palette, node.filename)

        try:
            data = BytesWriter()
            img.get_png().write(data)
        except Exception as e:
            print(f'failed to read image {node.filename}: {e}')

        iniData = img.to_ini(getBytes=True)

        name = re.sub(r'\.(png|txt)$', '', node.filename)
        self._decodedImages[name + '.png'] = self._DecodedImage._make([data.length, data.data])
        self._decodedImages[name + '.txt'] = self._DecodedImage._make([len(iniData), iniData])


class TexturesFS(FigurenFS):
    class _Group(AnnoFS._Group):
        ById = 1
        ByName = 2
        ByKey = 3

        Images = 'img'

    _ByGroups: List[_Group] = frozenset([_Group.ById, _Group.ByName, _Group.ByKey])
    _BasePaths: Dict[_Group, str] = {
        _Group.ById: 'by-id',
        _Group.ByName: 'by-name',
        _Group.ByKey: 'by-key',
    }

    _ParsePaths = (
        'ANNO/TEXTURE/#entry-id',
        'ANNO/TEXTURE/NAME/#file-name',
        'ANNO/COLOR/#palette-id',
    )
    _EntryPath = 'ANNO/TEXTURE'
    _EntryIdPath = '#entry-id'
    _EntryPathPath = 'NAME/#file-name'
    _EntryParent = 'ANNO'

    def _loadImagesForEntry(self, node: FigurenFS._ImagesNode) -> None:
        entry = node.node

        if entry.parent.specNode.title == self._EntryParent:
            # self.byPath.parsePath(self._EntryPathPath, entry)
            # self.byPath.parsePath('HEAD/#image-header', entry)
            # self.byPath.parsePath('BODY/#image-data', entry)

            for path in (node.basepath + ['image.png'], node.basepath + ['image.txt']):
                self.add_obj_to_path(path, self._Element._make((self._Kind.File, self._Group.Images,
                                     self._FileNode._make(['/'.join(path), self._ImageHeader._make([entry, 0])]))))

    def _decodeImage(self, node: FigurenFS._FileNode):
        if node.filename in self._decodedImages:
            return

        # node.node is self._ImageHeader
        entry: ParsedNode = node.node.textures
        head = entry.getFirstPath('HEAD/#image-header').decodeToPy()
        body = entry.getFirstPath('BODY/#image-data').decodeToPy()

        # self.byPath.parsePath(f'ANNO/COLOR@palette-id:{head.palette_id}/BODY/#palette-entries')
        colors = self.byPath.tree.getFirstPath(f'ANNO/COLOR@palette-id:{head.palette_id}/BODY/#palette-entries').decodeToPy()
        palette = Palette(head.palette_id, '', colors)

        img = Image(head, body, palette, node.filename)
        name = re.sub(r'\.(png|txt)$', '', node.filename)

        iniData = img.to_ini(getBytes=True)
        self._decodedImages[name + '.txt'] = self._DecodedImage._make([len(iniData), iniData])

        try:
            data = BytesWriter()
            img.get_png().write(data)
            self._decodedImages[name + '.png'] = self._DecodedImage._make([data.length, data.data])
        except Exception:
            print(f'failed to read image {node.filename}')

    def generate_tree(self):
        self._setup_root_tree()

        self.byPath = self._register_parser(self._Group.ByName, Parser(**self._parserArgs))
        self.byPath.parseFully()

        skipGroups = []
        incOptions = (
            self._dataFile + '.inc',
            self._dataFile[:-4] + '.inc',
        )

        for i in incOptions:
            if FilePath(i).exists():
                self.keyParser = self._register_parser(self._Group.ByKey, Parser(i, structure='access'))
                self.keyParser.parseFully()

                if keys := self.keyParser.tree.getFirstPath('ANNO/ACCESS/data'):
                    self.keys = {str(x.uid): x.string for x in keys.decodeToPy()}
                else:
                    self.keys = {}

                break
        else:
            self.keys = {}
            self.keyParser = None
            skipGroups = (self._Group.ByKey, )

        entries = self.byPath.tree.getPath(self._EntryPath + " / *")

        for i, entry in enumerate(entries):
            entryId = entry.getFirstPath(self._EntryIdPath + " / 1").decodeToPy()
            string = self._getPathForEntry(entry)

            if not string:
                string = self.keys.get(str(entryId), f'{i:04d}')

            xmlName = f'{entryId:04} {string.split("/")[-1]}.xml'

            paths = {
                self._Group.ByName: string,
                self._Group.ById: f'{entryId:04} {string}',
                self._Group.ByKey: self.keys.get(str(entryId), string),
            }

            print("loaded:", paths[self._Group.ByName])

            for group in self._ByGroups:
                if group in skipGroups:
                    continue

                self.add_obj_to_path([self._BasePaths[group]] + paths[group].split('/'), self._Element._make((self._Kind.Dir, group, paths[group])))
                self.entriesByPath[paths[group]].append(self._FileNode._make((xmlName, entry)))
