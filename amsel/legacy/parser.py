# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import re
import os
import logging as log
from pathlib import Path
from typing import BinaryIO
from bs4 import BeautifulSoup

from ..tools import log_fatal
from ..tools import save_xml
from .soupext import Block as _BlockBase


class Block(_BlockBase):
    # VALID_HEADER_RE = re.compile(r'^([A-Z][_A-Z]+[0-9]*)[\x00\x01]*$')
    VALID_HEADER_RE = re.compile(r'^([A-Z][_A-Z]+[0-9]*)[\x00]*$')

    @classmethod
    def read_head_at(cls, file, offset_start: int, head_type):
        # TODO add option to skip validation and return all data
        #      This is required for reading some files where headers have been scrambled
        #      with arbitrary data (e.g. Textures/ScapeIcon.inc).
        file.seek(offset_start)

        if type(head_type) is str:
            head_type = cls.TYPES[head_type]

        try:
            decoded = file.read(head_type.name_len).decode('windows-1252')
        except UnicodeDecodeError:
            return None

        match = re.match(cls.VALID_HEADER_RE, decoded)

        if match:
            return match.groups()[0]
        else:
            return None

    @classmethod
    def read_length_at(cls, file: BinaryIO, offset_start, head_type):
        if type(head_type) is str:
            head_type = cls.TYPES[head_type]

        file.seek(offset_start)
        read = file.read(head_type.length_len)
        return int.from_bytes(read, byteorder='little', signed=False)

    @classmethod
    def scan_for_head(cls, file, offset_start: int, head_type: str, max_offset: int):
        skipped = 0

        for i in range(0, 64):
            if (offset_start+skipped+cls.TYPES[head_type].decl_len+1) > max_offset:
                return None

            if head := Block.read_head_at(file, offset_start+skipped, head_type):
                return {'skipped': skipped, 'name': head}
            else:
                skipped += 1
                continue

    @classmethod
    def guess_head_at(cls, file: BinaryIO, offset_start: int, max_scan_offset: int, max_data_offset: int):
        if (max_scan_offset-offset_start) < (cls.SHORT.decl_len):
            return None

        if head := cls.scan_for_head(file, offset_start, "short", max_scan_offset):
            skipped = head['skipped']
            head = head['name']
            found_type = 'short'
        else:
            return None

        # found a valid short head, now check if it actually is a long one
        if (max_scan_offset-offset_start-skipped) >= cls.LONG.decl_len:
            actually_long = Block.read_head_at(file, offset_start+skipped, "long")

            if actually_long:
                found_type = 'long'
                head = actually_long

        length = Block.read_length_at(file, offset_start+skipped+cls.TYPES[found_type].name_len, found_type)
        target_offset = offset_start+skipped+cls.TYPES[found_type].decl_len+length
        max_scan_offset = min(max_scan_offset, max_data_offset)

        if target_offset > max_scan_offset:
            err = "invalid guess: {h} (type: {t}, length: {l}) exceeds max offset ({x}: {y}) at {a}"
            log.debug(err.format(h=head, t=found_type, l=length,
                                 x=('offset' if max_scan_offset < max_data_offset else 'file'),
                                 y=max_scan_offset, a=target_offset))
            return None

        return {
            'head': head,
            'head_len': cls.TYPES[found_type].decl_len,
            'length': length,
            'skipped': skipped,
            'type': found_type
            }


class _ParserBase:
    class _InfileContextManager():
        def __init__(self, parser, as_main):
            self._parser = parser
            self._is_main = as_main
            self._stream = None

        def __enter__(self, *args):
            if self._is_main:
                # We open the file as 'rb', as this manager is only intended for
                # opening a *binary* *input* file.
                self._stream = open(self._parser.infile, 'rb')
                self._parser._input_manager = self
            else:
                self._stream = self._parser._input_manager._stream
            return self._stream

        def __exit__(self, *args):
            if self._is_main:
                self._stream.close()
                self._parser._input_manager = None
            else:
                # the file will be closed by the main context manager
                pass

    def __init__(self, infile):
        self.xml = BeautifulSoup('', 'xml')

        self._is_parsed = False
        self._infile = infile
        self._infile_name = Path(infile).name
        self._infile_size = os.path.getsize(infile)

        # important: this variable is managed by the context manager!
        # Use 'with parser.open_infile() as fd' to read from the input file.
        self._input_manager = None

    def _do_parse(self) -> bool:
        # override this in derived classes
        log_fatal(NotImplementedError, f'parsing is not implemented for this parser')
        return False

    @property
    def infile(self):
        return self._infile

    def open_infile(self):
        """ Context manager. """
        if self._input_manager:
            return self._InfileContextManager(self, as_main=False)
        else:
            return self._InfileContextManager(self, as_main=True)

    def parse(self):
        if self._is_parsed:
            return self
        if self._do_parse():
            # implemented by derived classes
            self._is_parsed = True
        else:
            log.error(f'parsing failed! error code: {self.error_no}')
            self._is_parsed = False
        return self

    def load_all(self, exclude=[], with_data=True, with_pointers=True):
        log.info(f"loading and preparing output... (excluding: {exclude})")
        self.parse()
        self.xml.ext.update_xml(recursive=True, exclude=exclude, with_data=with_data,
                                with_pointers=with_pointers)
        log.info("finished loading and preparing output")

    def save_xml(self, path_or_stream, force=False, load_all=False, with_pointers=True):
        with self.open_infile() as fd:
            self.parse()
            if load_all:  # = load all data
                self.load_all(with_data=True, with_pointers=with_pointers)
            elif with_pointers:
                self.load_all(with_data=False, with_pointers=True)
            save_xml(path_or_stream, self.xml, force)


class BinaryAnalyzer(_ParserBase):
    def __init__(self, infile):
        super().__init__(infile)

        self._current_node = self.xml
        self._current_level = 0
        self._current_offset = 0
        self._path = []

    def _do_parse(self) -> bool:
        with self.open_infile() as fd:
            success = self._do_parse_recursive(offset_start=0, length=self._infile_size,
                                               parent_head_name='file', parent_type=None)
        self._bake_counts()
        return success

    @property
    def _path_str(self):
        return '/'.join(self._path)

    def _bake_counts(self):
        # convert counts saved as class members to XML attributes
        # They have to be class members so they don't interfere when comparing tags.
        # important: file must be parsed!
        for x in self.xml.find_all():
            if x.ext.count > 0:
                x['count'] = x.ext.count

    def _do_parse_recursive(self, offset_start, length, parent_head_name, parent_type):
        offset_current = offset_start
        offset_end = offset_start + length
        self._current_level += 1
        self._path.append(parent_head_name)
        log.info(f"{offset_start:12d}/{self._infile_size:d} - level {self._current_level:3d}: {self._path_str}")

        if self._current_level > 1:
            tag = self.xml.new_tag("head", title=parent_head_name, type=parent_type)
            tag.ext.count = 1

            tag.ext.parser = self
            tag.ext.spec_node = None
            # this is not optimal, we shouldn't have to fix the offset:
            tag.ext.original_offset = offset_start - \
                (0 if not parent_type else Block.TYPES[parent_type].decl_len)
            tag.ext.original_length = length
        else:
            with self.open_infile() as fd:
                fd.seek(0)
                # 32 = 12+4 + 12+4 -> two long headers with length declarations
                magic = fd.read(32).hex(sep=' ', bytes_per_sep=1).upper()
            tag = self.xml.new_tag("file", source=self._infile_name, type="spec",
                                   title=self._infile_name, magic=f'0|{magic}')

        self._current_node.append(tag)
        self._current_node = tag

        while offset_current < offset_end:
            with self.open_infile() as fd:
                found = Block.guess_head_at(fd, offset_current, offset_end, self._infile_size)

            if found:
                if found['skipped'] > 0:
                    data_node = self.xml.new_tag("data", type="raw", length=found['skipped'])
                    data_node.ext.parser = self
                    data_node.ext.spec_node = None
                    data_node.ext.original_offset = offset_current
                    data_node.ext.original_length = length
                    self._current_node.append(data_node)

                self._do_parse_recursive(offset_current+found['skipped']+found['head_len'],
                                         found['length'], found['head'], found['type'])
                offset_current = offset_current+found['skipped']+found['head_len']+found['length']

                if offset_current > offset_end:
                    log.error("current:{c} > end:{e} [skipped: {s}, head: {h}, len: {l}, name: {n}]".
                              format(c=offset_current, e=offset_end, s=found['skipped'],
                                     h=found['head_len'], l=found['length'], n=found['head']))
            else:
                # read remaining data
                data_node = self.xml.new_tag("data", type="raw", length='*')
                data_node.ext.parser = self
                data_node.ext.spec_node = None
                data_node.ext.original_offset = offset_current
                data_node.ext.original_length = offset_end - offset_current
                self._current_node.append(data_node)
                break

        if self._current_node.previous_sibling and self._current_node == self._current_node.previous_sibling:
            trashed = self._current_node.previous_sibling.extract()  # throw it away
            precount = trashed.ext.count if trashed.ext.count > 0 else 1
            self._current_node.ext.count = precount + 1

        self._current_level -= 1
        self._current_node = self._current_node.parent
        del self._path[-1]
        return True
