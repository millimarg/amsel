# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''


def from_bin(data, parsed_node) -> None:
    parsed_node.ext.data_type = 'raw'
    return DecRaw.bin2py(data=data, parsed_node=parsed_node)


def to_xml(parsed_node) -> None:
    parsed_node.ext.load_data()
    return DecRaw.py2xml(parsed_node)


# Raw data.
#
# Simply any data of any length. This format is used if the structure is not
# known or if no decoder is available yet.
class DecRaw():
    @classmethod
    def _save_xml(cls, target_node, stringify_callback=str):
        as_py = target_node.ext.data

        if as_py is not None:
            as_str = stringify_callback(as_py)
        else:
            as_str = ''

        return as_str

    @classmethod
    def bin2py(cls, data: bytearray, parsed_node) -> None:
        return data

    @classmethod
    def py2xml(cls, parsed_node) -> None:
        return cls._save_xml(parsed_node, lambda x: x.hex(sep=' ', bytes_per_sep=128))
