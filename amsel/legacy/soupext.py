# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import logging as log
from typing import Any

from bs4 import BeautifulSoup
from bs4 import Tag as XmlTag

from ..tools import log_fatal
from . import decoders

SOUP_EXTENSIONS_READY = False


# ------------------------------------------------------------------------------
# Extensions to BeautifulSoup
#
# setup_soup_extensions is called from __init__.py.
# ------------------------------------------------------------------------------

class _HeadType:
    def __init__(self, typename, name_len, length_len):
        self.typename = typename
        self.name_len = name_len
        self.length_len = length_len

    @property
    def decl_len(self):
        return self.name_len + self.length_len


class Block:
    LONG = _HeadType('long', 12, 4)
    SHORT = _HeadType('short', 6, 2)
    TYPES = {'long': LONG, 'short': SHORT}


class SoupExtensions:
    _LOAD_FROM_DONE = 0
    _LOAD_FROM_BIN = 1
    _LOAD_FROM_XML = 2

    _HAVE_SOURCE_INFO = True

    def __init__(self, xml_node):
        self._xml_node = xml_node
        self.parser = None
        self.spec_node = None
        self.original_length = 0
        self.count = 0

        self._data = None
        self._original_offset = 0
        self._original_offset_hex = '0'
        self._load_from = self._LOAD_FROM_BIN

        self._cached_binary = bytearray()
        self._is_binary_dirty = True

    def _is_excluded(self, node, exclude) -> bool:
        if exclude and ((node.ext.is_head_node and node.ext.head_name in exclude)
                        or (node.ext.ident and f'#{node.ext.ident}' in exclude)):
            return True
        return False

    def from_xml(self, force=False) -> None:
        self._propagate_load_source(self._LOAD_FROM_XML, force)

    def load_data(self, recursive=False, exclude=[]) -> None:
        if self._is_excluded(self._xml_node, exclude):
            log.debug(f'load_data called on excluded node ({self}, {exclude})')
            return

        self.data  # request to load

        for ch in (self.valid_children if recursive else []):
            if not self._is_excluded(ch, exclude):
                ch.ext.load_data(recursive=recursive, exclude=exclude)

    def update_xml(self, recursive=False, exclude=[], with_data=True, with_pointers=True, force=False) -> None:
        if self._is_excluded(self._xml_node, exclude):
            log.debug(f'update_xml called on excluded node ({self}, {exclude})')
            return

        # also include updated data if it has been loaded before
        if self.is_data_node and (with_data or self._xml_node.attrs.get('value', '')):
            self.data_str = decoders.to_xml(parsed_node=self._xml_node)

        if with_pointers and not self.is_root_node:
            self._xml_node.attrs['original-offset-hex'] = self.original_offset_hex
            self._xml_node.attrs['original-length'] = self.original_length

        if recursive:
            if force:
                recursive_list = self._xml_node.children
            else:
                recursive_list = self.valid_children
        else:
            recursive_list = []

        for ch in recursive_list:
            if ch.name and not self._is_excluded(ch, exclude):
                ch.ext.update_xml(recursive=recursive, exclude=exclude, with_data=with_data,
                                  with_pointers=with_pointers, force=force)

    @property
    def valid_children(self):
        if isinstance(self._xml_node, BeautifulSoup):
            if found := self._xml_node.find('file', recursive=False):
                return [found]
            return []
        elif self.is_root_node or self.is_head_node:
            # if self._xml_node.find_parent('group'):
            #     return self._xml_node.find_all(['head', 'data'], recursive=False)
            # else:
            #     return self._xml_node.find_all(['head', 'data', 'group'], recursive=False)
            return self._xml_node.find_all(['head', 'data', 'group'], recursive=False)
        elif self.is_group_node:
            # return self._xml_node.find_all(['head', 'data'], recursive=False)
            # nested groups are not allowed when parsing binary files but are
            # no problem for extracts and for reencoding
            return self._xml_node.find_all(['head', 'data', 'group'], recursive=False)

        return []

    @property
    def is_root_node(self) -> bool:
        # A document has two root nodes: the main "meta" node which holds the
        # whole document, and the "real" root node <file>.
        return isinstance(self._xml_node, BeautifulSoup) or self._xml_node.name == 'file'

    @property
    def is_data_node(self) -> bool:
        return self._xml_node.name == 'data'

    @property
    def is_group_node(self) -> bool:
        return self._xml_node.name == 'group'

    @property
    def is_head_node(self) -> bool:
        return self._xml_node.name == 'head'

    @property
    def data_type(self) -> str:
        return self._xml_node.attrs.get('type', '') if self.is_data_node else ''

    @data_type.setter
    def data_type(self, new):
        if self.is_data_node:
            self._xml_node.attrs['type'] = new

    @property
    def head_type(self) -> _HeadType:
        if self.is_head_node:
            return Block.TYPES[self._xml_node.attrs.get('type', None)]
        return None

    @head_type.setter
    def head_type(self, new):
        if self.is_head_node:
            self._xml_node.attrs['type'] = new

    @property
    def original_offset(self) -> int:
        if self._original_offset == 0 and self._load_from == self._LOAD_FROM_XML:
            # probably not working...
            self.original_offset = self._xml_node.attrs.get('original-offset', self.start_offset)
        return self._original_offset

    @original_offset.setter
    def original_offset(self, new):
        self._original_offset = new
        self._original_offset_hex = f'{new:X}'

    @property
    def original_offset_hex(self) -> str:
        return self._original_offset_hex

    @property
    def data(self) -> Any:
        self._is_binary_dirty = True  # always mark cache dirty as we don't know if data will change

        if not self.is_data_node:
            log.debug(f'cannot load data: not a data node ({self})')
            return None

        if self._data is not None:
            return self._data

        if self._load_from == self._LOAD_FROM_BIN:
            if not self.parser:
                log_fatal(AttributeError, f'soup extensions must be initialized with a '
                                          f'parser to load from binary ({self})')

            with self.parser.open_infile() as fd:
                fd.seek(self.original_offset)
                raw_data = fd.read(self.original_length)
                self.data = decoders.from_bin(data=raw_data, parsed_node=self._xml_node)

        self._load_from = self._LOAD_FROM_DONE  # also set in data.setter
        return self._data

    @data.setter
    def data(self, new):
        if self.is_data_node:
            self._data = new
            # don't attempt to load data again if it has been set manually
            self._load_from = self._LOAD_FROM_DONE
        else:
            log.debug(f'cannot set data on non-data note ({self})')

    @property
    def data_str(self) -> str:
        # XML data is rebuilt every time as there is no way of telling if
        # the underlying data has changed.
        self.update_xml()

        if self.is_data_node:
            return self._xml_node.attrs.get('value', '')
        return ''

    @data_str.setter
    def data_str(self, new):
        self._xml_node.attrs['value'] = new


def _global_monkey_getter(monkey_self) -> SoupExtensions:
    if hasattr(monkey_self, '_monkey_extensions') and \
            monkey_self._monkey_extensions is not None:
        return monkey_self._monkey_extensions

    ext = SoupExtensions(monkey_self)
    monkey_self._monkey_extensions = ext
    return ext


def setup_soup_extensions():
    global SOUP_EXTENSIONS_READY

    if SOUP_EXTENSIONS_READY:
        return

    BeautifulSoup.ext = property(_global_monkey_getter)
    XmlTag.ext = property(_global_monkey_getter)

    SOUP_EXTENSIONS_READY = True
