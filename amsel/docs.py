# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import re
import os
import logging as log
import inspect
import textwrap
# import base64
from pathlib import Path

import markupsafe
from jinja2 import Environment
from jinja2 import FileSystemLoader
from bs4 import BeautifulSoup

from . import decoders
from .spec import Spec
from .tools import is_list


EXTRA_INFO_LINKS = {
    'D': 'drkohler',
    'B': 'Dickerbaer',
    'A': 'Admiral Drake',
    'Bm': "Bomi's Tools",
    'Dw': 'DWOb',
}


ALLOWED_TAGS = re.compile(r'&lt;(/?(p|tt|em|b|br/?|pre|ul|ol|li|(a( href=&#34;.*?&#34;)?)|'
                          r'sub|sup|table|tr|th|td|details( open)?|summary|div( class=&#34;highlight&#34;)?|span/|span( class=&#34;.*?&#34;)?))&gt;')


def formatSpecDocs(text, tags=False):
    replaced = str(text)

    # enclosed filters
    replaced = re.sub(r'@topic:(.*?):(.*?)@', r'<a href="#topic-\1">\2</a>', replaced)  # link to a topic chapter (user must provide link text)
    replaced = re.sub(r'@f:(.*?)@', r'<a href="#format-\1">\1</a>', replaced)           # link to a data type
    replaced = re.sub(r'@s:(nb-)?(.*?)@', r'<tt><a href="#file-\1\2">\2</a></tt>', replaced)     # link to a file specification
    replaced = re.sub(r'@i:(.*?)@', r'<a href="#image-format-\1">\1</a>', replaced)     # link to an image format
    replaced = re.sub(r'@l:(.*?):(.*?)@', r'<a href="\2">\1</a>', replaced)             # link to $2 with text $1, e.g. @l:name:example.org@
    replaced = re.sub(r'@ext:(.*?)@', r'<tt class="filetype">\1</tt>', replaced)        # mark file type names
    replaced = re.sub(r'@t:(.*?)@', r'<tt>\1</tt>', replaced)                           # mark tag strings or filenames etc.

    # paths
    # usage: @p:<file>/BLOCK/BLOCK/#data-ident@
    # note:  this is not a full-fledged path string, i.e. idents and selectors are not allowed
    for i in re.findall(r'@p:.*?@', replaced):
        parts = [x.strip() for x in re.sub(r'@p:(.*?)@', r'\1', i).strip().split('/')]
        name = parts[-1]
        replaced = replaced.replace(i, f'<a href="#file-{"-".join(parts)}">{name}</a>')

    if tags:  # needed by transcoder docs
        replaced = re.sub(ALLOWED_TAGS, r'<\1>', replaced)

    # word suffixes
    replaced = re.sub(r'@x\b', r'<sub>16</sub>', replaced)   # mark hexadecimal values
    replaced = re.sub(r'@d\b', r'<sub>10</sub>', replaced)   # mark decimal values
    replaced = re.sub(r'@o\b', r'<sub>8</sub>', replaced)    # mark octal values
    replaced = re.sub(r'@b\b', r'<sub>2</sub>', replaced)    # mark binary values
    replaced = re.sub(r'([\d\w]+)@t\b', r'<tt>\1</tt>', replaced)       # mark tag strings

    # standalone markers
    for k, v in EXTRA_INFO_LINKS.items():
        # TODO properly link further readings somewhere
        # note: {k} is kept in the target because it is meant as links in the text, not footnotes
        replaced = re.sub(f'@{k}\\b', f'<small><tt>[{k}]</tt></small>', replaced)    # mark sources

    replaced = re.sub(r'\bTODO\b', r'<b><tt>[TODO]</tt></b>', replaced)     # mark todo notes

    return replaced

# def _encode_inline_image(path, kind="png"):
#     with open(path, 'rb') as fd:
#         encoded = base64.b64encode(fd.read())
#
#     return f'data:image/{kind};base64,{encoded}'


def _wrap_all_str(values, prefix, suffix):
    return [f'{prefix}{x}{suffix}' for x in values]


def _convert_path(elements):
    """ Convert a list of path elements to a path string.
        Note: path elements must not include the spec name.

        Example: [ANNO, FIGUREN, ENTRY, #entry-id]
                    becomes
                 ANNO/FIGUREN/ENTRY/#entry-id
    """
    elements = [x for x in elements if x.strip()]

    if not elements:
        return ''

    return '/'.join(elements)


def _calculate_local_offset(block: BeautifulSoup) -> str:
    offset = 0

    for i in block.previous_siblings:
        if i.name == 'group' and not hasattr(i.attrs, 'length'):
            i['length'] = _calculate_group_length(i)

        if i.name == 'head':
            offset = '?'
            break

        try:
            offset += int(i.get('length', '0')) * int(i.get('count', 1))
        except ValueError:
            offset = '?'
            break
        except AttributeError:
            continue

    return str(offset)


def _calculate_group_index(block: BeautifulSoup) -> int:
    return len([x for x in block.previous_siblings if x.name == 'group']) + 1


def _calculate_group_length(block: BeautifulSoup) -> str:
    try:
        length = sum([int(x['length']) * int(x.get('count', 1)) for x in block.find_all('data')])  # groups can only have data children
    except ValueError:
        length = '(n)'

    return length


def build_docs(args):
    def _replace_special(escaped: str, tags=False):
        replaced = formatSpecDocs(escaped, tags)
        return markupsafe.Markup(replaced)

    def _reduced_escape(unsafe: str):
        safe = str(markupsafe.escape(str(unsafe)))
        safe = re.sub(ALLOWED_TAGS, r'<\1>', safe)
        safe = safe.replace('&#34;', '"')  # FIXME do this only for the tags listed in ALLOWED_TAGS
        return markupsafe.Markup(safe)

    # load all spec files
    sorted_defs = [v for k, v in sorted(Spec.get_definitions().items(), key=lambda x: x[1].title)]

    # load all decoders and documentation comments (lines above the
    # definition, not docstrings)
    dec_full = decoders.load_decoders()
    dec_list = decoders.load_decoders()
    dec_list = [(k, inspect.getcomments(v)) for k, v in dec_list.items()]

    # move custom decoders (c_foobar) to the end
    dec_list = sorted(dec_list, key=lambda x: x[0].startswith('c_'))

    # split comments into paragraphs, mark all indented paragraphs as "pre-formatted"
    # The first line is used as brief description (<what>), the full text is used
    # as detailed description (<details>).
    formats = []
    for name, comments in dec_list:
        if comments:
            lines = [re.sub(r'^#[ ]?', '', x) for x in comments.split('\n')]
        else:
            log.error(f"no documentation for decoder: {name}")
            lines = []

        xmldoc = dec_full[name].xmldoc[4:]
        seps = dec_full[name].xmldoc[0:3]

        lines += ['<br>',
                  f'<b>XML representation:</b> {xmldoc}',
                  '',
                  '  example: (without enclosing bars)',
                  f'  | {dec_full[name].xmlex} |',
                  '  ',
                  '  separators:',
                  ]

        if 'f' in seps:
            lines.append(f'  - fields: "{dec_full[name].CODED_SPLIT_FIELDS}"')

        if 'i' in seps:
            lines.append(f'  - items:  "{dec_full[name].CODED_SPLIT_ITEMS}"')

        if 'l' in seps:
            lines.append(f'  - lists:  "{dec_full[name].CODED_SPLIT_LISTS}"')

        if seps == 'xxx':
            lines[-1] += ' none'

        if lines:
            short = markupsafe.Markup(markupsafe.escape(lines[0]))
        else:
            short = ''

        if len(lines) > 1:
            details = '\n'.join(lines[1:]).strip().split('\n\n')
            # build a list of tuples: (text, pre-formatted?)
            # pre-formatted code will not be escaped and must be placed in <code> tags
            details = [((_replace_special(markupsafe.escape(x), tags=True) if x[0:2] != '  ' else x), (True if x[0:2] == '  ' else False))
                       for x in details]
        else:
            details = []

        formats.append((name, short, details))

    # create the jinja2 environment
    os.chdir(Path(Spec.get_spec_dir()) / 'doc')
    env = Environment(loader=FileSystemLoader(Path(Spec.get_spec_dir()) / 'doc'),
                      trim_blocks=True, extensions=['jinja2_highlight.HighlightExtension',
                                                    'jinja2.ext.do', 'jinja2.ext.loopcontrols'])
    env.filters['dedent'] = textwrap.dedent
    env.filters['dedent_pre'] = lambda x: f'<pre>{textwrap.dedent(x)}</pre>'
    env.filters['flags'] = _replace_special
    env.filters['xe'] = _reduced_escape
    env.filters['escape_x'] = _reduced_escape
    env.filters['is_list'] = is_list
    env.filters['wrap_all_str'] = _wrap_all_str
    env.filters['path'] = _convert_path
    # env.filters['inline_image'] = _encode_inline_image

    env.globals['calculate_local_offset'] = _calculate_local_offset
    env.globals['calculate_group_index'] = _calculate_group_index
    env.globals['calculate_group_length'] = _calculate_group_length

    # output: no file (yet)

    # env.from_string('r{% include "../common.j2" as c %} ' + )

    print(env.get_template('index.html').render(specfiles=sorted_defs, formats=formats))

