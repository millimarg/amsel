#!/bin/bash

# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

for i in *.ogg; do
    ffmpeg -i "$i" -c:a pcm_s16le "${i%.ogg}.wav"
done
