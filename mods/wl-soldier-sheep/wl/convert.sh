#!/bin/bash

# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

printf -- "%s\n" "
Preparation:

- get the game: https://github.com/widelands/widelands
- get the files:
    widelands/data/tribes/workers/empire/soldier/*.png
    widelands/data/tribes/workers/empire/soldier/init.lua
- crop: convert walk_se_1.png -crop 23x39 walk_se.png
- link to missing direction graphics:
    ln -s 2-walk_ne_1.png 1-walk_n_1.png
    ln -s 6-walk_sw_1.png 5-walk_s_1.png


Hotspots:
                        hotspot:
0 - stehen - idle       5, 34
4 - gehen - walk        9, 35
7 - grasen - attack     ... cf. init.lua
17 - sterben - die
"

rm -r 2-recolored
rm -r 3-cropped

mkdir 2-recolored
mkdir 3-cropped

echo "base palette..."

montage -mode concatenate -background transparent -tile 3x 1-orig/*.png all-0.png
mogrify -channel RGBA -matte -colorspace transparent -colors 256 all-0.png


echo walk...

cnt=0

for i in 1-orig/*walk*.png; do
    convert "$i" -remap all-0.png "2-recolored/walk-$cnt.png"
    ((cnt++))
done

for i in 2-recolored/walk-*.png; do
    convert "$i" -crop 3x4@ "3-cropped/${i#2-recolored/}"
done

rm 3-cropped/walk-*-10.png
rm 3-cropped/walk-*-11.png


echo idle...

convert 1-orig/idle_1.png -remap all-0.png 2-recolored/idle.png
convert 2-recolored/idle.png -crop 6x7@ 3-cropped/idle.png
rm 3-cropped/idle-40.png
rm 3-cropped/idle-41.png


echo attack...

for i in 0; do
    convert 1-orig/atk_fail_e_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 3x4@ "3-cropped/attack-$i.png"
done

for i in 1; do
    convert 1-orig/atk_ok_e_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 3x4@ "3-cropped/attack-$i.png"
done

for i in 2; do
    convert 1-orig/eva_fail_e_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 3x4@ "3-cropped/attack-$i.png"
done

for i in 3; do
    convert 1-orig/eva_ok_e_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 4x5@ "3-cropped/attack-$i.png"
done

for i in 4; do
    convert 1-orig/atk_fail_w_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 3x4@ "3-cropped/attack-$i.png"
done

for i in 5; do
    convert 1-orig/atk_ok_w_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 3x4@ "3-cropped/attack-$i.png"
done

for i in 6; do
    convert 1-orig/eva_fail_w_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 3x4@ "3-cropped/attack-$i.png"
done

for i in 7; do
    convert 1-orig/eva_ok_w_1.png -remap all-0.png "2-recolored/attack-$i.png"
    convert "2-recolored/attack-$i.png" -crop 4x5@ "3-cropped/attack-$i.png"
done

rm 3-cropped/attack-{0..2}-1{0,1}.png
rm 3-cropped/attack-{4..6}-1{0,1}.png


echo die...

convert 1-orig/die_e_1.png -remap all-0.png 2-recolored/die.png
convert 2-recolored/die.png -crop 4x5@ 3-cropped/die.png


echo zoom...

mkdir -p 3-cropped/0
mkdir -p 3-cropped/1
mkdir -p 3-cropped/2

mv 3-cropped/*.png 3-cropped/0

for i in 3-cropped/0/*.png; do
    convert "$i" -resize 50% "${i//\/0\//\/1\/}"
done

for i in 3-cropped/1/*.png; do
    convert "$i" -resize 50% "${i//\/1\//\/2\/}"
done

for i in {1..2}; do
    montage -mode concatenate -background transparent "3-cropped/$i"/*.png "all-$i.png"
    mogrify -channel RGBA -matte -colorspace transparent -colors 256 "all-$i.png"

    for x in "3-cropped/$i"/*.png; do
        mogrify -remap "all-$i.png" "$x"
    done
done


echo palettes...

for i in {0..2}; do
    # NOTE the name should be 23 bytes long (same as the original). This makes it
    #      easier to compare original and changed Figuren.dat files to verify changes.
    amsel to gfx pmk "all-$i.png" -o "wl-soldier-$i" -n "widelands-soldier$1.gpl"
done
