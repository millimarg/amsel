
# Amsel -- Anno Modding System and Exploration Library

<!--
This file is part of amsel.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
-->

Amsel is a library and toolset for viewing, analysing, and modifying binary data
files of the game [Anno 1503](https://en.wikipedia.org/wiki/Anno_1503).</p>

**Modding:** With the Amsel toolset you can build mods for Anno 1503 in an
intuitive manner without having to edit files using a hex editor. Mods are
simple XML files that can be expanded using the powerful
[Jinja2](https://jinja.palletsprojects.com/en/3.1.x/templates/) template language.
See the [mods folder](https://gitlab.com/millimarg/amsel/mods) for examples,
and browse [the documentation](https://anno-urbis.gitlab.io/amsel) to get you started!

**Coding:** The library provides low-level access to the
[binary data structures](https://anno-urbis.gitlab.io/amsel),
and has an easy interface for modifying files programmatically.

**Studying:** Various tools provide ways to understand the data. Amsel lets you
mount a binary data file as a virtual filesystem so that you can browse it with
a file manager. [A₂E](https://gitlab.com/millimarg/ae) is a GUI toolkit for
viewing and editing game files.


## Project status

**IN DEVELOPMENT**

Amsel is the culmination of various other projects that are now discontinued.
All core components of the library and modding system are working as intended
but documentation is still severely lacking.

Most of the implementation can be understood as a proof-of-concept rather than
the final product. The core of the project is the spec file schema and the
structured file format documentation.

Especially in the spec files, there are many gaps that still need to be filled
with info from existing sources. Details from older projects have to be ported,
drkohler's AnlagenProjekt documentation should be incorporated into the Anlagen
spec, and bits of information all over the forums have to be assembled.

Amsel can be used for building mods but the mod file format may change in the
future. Nothing is set in stone.


## How to get started

Amsel runs best in a Python virtual environment.

```bash
# Fetch the code
git clone https://gitlab.com/millimarg/amsel
pushd amsel

# Setup the environment
./amsel-py.sh -h

# Prepare data folders
mkdir -p "$HOME/.local/share/amsel"
ln -s "$PWD/spec" "$HOME/.local/share/amsel/spec"
ln -s "$PWD/amsel/mod-templates" "$HOME/.local/share/amsel/mod-templates"

# Build the documentation
./amsel-py.sh build-docs > docs.html

# Check out the tools/ and mods/ folders, and browse the documentation
# to get started: https://anno-urbis.gitlab.io/amsel

./amsel-py.sh -h
./amsel-py.sh mod -h
```


## Contributing

There are many ways you can contribute, and all contributions are welcome!

- use the library and build mods
- work on library documentation
- improve spec files and structure documentation
- build tools for studying and using Anno files
- ...and so much more!

**Need some easy tasks?** You can help a lot by finding hints at file structures
in the AnnoZone forums, and importing these bits of information into the spec
files! Always add a link to the original source of the information, of course.

Files are documented in [XML description files](https://gitlab.com/millimarg/amsel/-/tree/main/spec)
which are then used to build the [documentation](https://anno-urbis.gitlab.io/amsel).


## Other failed approaches

There are a number of libraries that serve a similar purpose as `amsel`, just
in a generalized manner. I tried and failed to adapt them for modding Anno 1503,
so I finally came back to writing my own parser.

Most of the libraries had issues with reading structured data, and/or were
unable to write modified data.

- [Mr. Crowbar](https://mrcrowbar.readthedocs.io/en/latest/)
- [Construct](https://construct.readthedocs.io/)
- [Kaitai](https://github.com/kaitai-io/kaitai_fs)
- [Hachoir](https://github.com/vstinner/hachoir)
- [PyFFI](https://github.com/niftools/pyffi)


## Acknowledgments

Thank you to Dickerbaer, Admiral Drake, DWOb, drkohler, and the
[AnnoZone](https://annozone.de/forum) community for your support and
insights!


## License

Copyright (C) 2020-2024  Emily Margit Mueller (millimarg)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <a href="https://www.gnu.org/licenses/">www.gnu.org/licenses</a>.
