# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

paste <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/#entry-id' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/PATH/#path' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/HEAD/#movement-speed' -v --no-count) | sort -t $'\t' --key='1n' > figuren-movement-speed-id
paste <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/#entry-id' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/PATH/#path' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/HEAD/#movement-speed' -v --no-count) | sort -t $'\t' --key='3n' > figuren-movement-speed-value

paste <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/#entry-id' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/PATH/#path' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/HEAD/#turning-speed' -v --no-count) | sort -t $'\t' --key='1n' > figuren-turning-speed-id
paste <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/#entry-id' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/PATH/#path' -v --no-count) <(amsel to psel figuren.dat 'ANNO/FIGUREN/ENTRY/HEAD/#turning-speed' -v --no-count) | sort -t $'\t' --key='3n' > figuren-turning-speed-value
