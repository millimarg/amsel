# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

for i in BEWOHNTYPE ROUTETYPE FORSCHTYPE WARE BAUWARE; do amsel to psel Figuren.dat "ANNO/FIGUREN/ENTRY/$i/data" -v --no-count | sort -u > "spec/values/figuren-$i"; done
