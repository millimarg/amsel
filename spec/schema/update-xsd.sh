#!/bin/bash
# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

self="$(readlink -m "$(dirname "$0")")"
trang "$self/schema.rnc" "$self/schema.xsd"
