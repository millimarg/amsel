#!/bin/bash
# This file is part of amsel.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

fail=0

schema="schema"
if [[ ! -f "$schema.xsd" || "$schema.rnc" -nt "$schema.xsd" ]]; then
    echo "converting relax-ng schema to xsd..."
    trang "$schema.rnc" "$schema.xsd"
    (( fail += $? ))
fi

temp="$(mktemp -d)"

echo "checking spec files..."
pushd ..

for i in *.xml; do
    amsel dump-spec "${i%.xml}" > "$temp/$i"
    jing -c "schema/$schema.rnc" "$temp/$i" && echo "$i: ok" || { (( fail += $? )) && echo "$i: fail"; }
done

# echo "SPEC ==================="
# jing -c "$schema.rnc" ../*.xml && echo "ok" || { (( fail += $? )) && echo "fail"; }
# echo "META ==================="
# jing -c "$schema.rnc" ../meta/*.xml && echo "ok" || { (( fail += $? )) && echo "fail"; }
# echo "PART ==================="
# jing -c "$schema.rnc" ../parts/*.xml && echo "ok" || { (( fail += $? )) && echo "fail"; }
# echo "MODS ==================="
# jing -c "$schema.rnc" ../mods/*.xml && echo "ok" || { (( fail += $? )) && echo "fail"; }

if (( "$fail" == 0 )); then
    rm -rf "$temp"
else
    echo "review issues in resolved spec files at: $temp"
fi

exit "$fail"
