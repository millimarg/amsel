<?xml version="1.0" encoding="utf-8"?>
<file source="fenster.inc" title="Texture Index File" type="spec" magic="16|41 43 43 45 53 53 00 00 00 00 00 00">
<!--
	This file is part of amsel.
	SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
	SPDX-License-Identifier: GPL-3.0-or-later
-->

	<what>Texture index files (@ext:*.inc@).</what>
	<details>
		<p>Each @s:tex@ file (@ext:*.tex@) has an associated @s:access@ file (@ext:*.inc@)
		containing an index of entry groups, mapping entry IDs to strings. The file
		has the same file name as the associated @s:tex@ file. There is no reference
		to the other file inside this file.</p>

		<p>Entries are defined in groups and only the first entry of a groups is noted
		in this file. All subsequent entries in the @s:tex@ file belong to the group,
		up until the next group starts.</p>

		<p>Example: the @s:access@ file defines <tt>0 = LOREM</tt>, <tt>10 = IPSUM</tt>, and <tt>11 = DOLOR</tt>.
		This means all entries with ID numbers from 0-9 (inclusive) belong to group
		<tt>LOREM</tt>, while group <tt>IPSUM</tt> has only one entry with ID number 10.
		The last group <tt>DOLOR</tt> contains all remaining entries from ID number 11
		to the end of the @s:tex@ file.</p>

		<p>Some @s:tex@ files only have one entry and the corresponding @s:access@
		file specifies the original file name for the entry.</p>
	</details>

	<head count="1" title="ANNO" type="long" allow-mangled="true">
		{% include "meta/anno.xml" %}
		<head count="1" title="ACCESS" type="long" allow-empty="true">
			<what>Secondary main header defining the file type.</what>
			<data length="*" type="c_cstring_list_csv" id="groups">Maps the first ID number of each group to a string.</data>
		</head>
	</head>
</file>
