{% extends "topic_base.html" %}

{#
  This file is part of amsel.
  SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
  SPDX-License-Identifier: GPL-3.0-or-later
#}

{% set id = 'topic-block-types' %}
{% set iid = 'block-type' %}
{% set name = 'Block Types' %}

{% block text %}
<p>Each block's name or header identifies the block. Nearly in every file,
headers consist of three parts: title and padding followed by the declaration of
the length of following data.</p>

<p>There are two types of block headers. The main difference is that long headers
can hold more data as short headers: short headers have a maximum length of 65'535
bytes, whereas long headers can declare a length of 4'294'967'295 bytes. This limits
the total size of a game data file to about 4 GiB. No game data file is larger than
300 MiB, though.</p>
{% endblock %}

{% block table %}
	<tr id="{{ iid }}-long">
		<td>long (16 bytes)</td>
		<td>Long block headers start with a zero-padded string of <b>12 bytes</b> followed by the declaration of the block's length in <b>4 bytes</b> (<a href="#format-uint">uint</a>).

			<pre>{%- filter dedent %}
				zero = .

				COLORS......nnnn

				followed by nnnn bytes of data, i.e. the block's "body".
			{% endfilter -%}</pre>
		</td>
	</tr>
	<tr id="{{ iid }}-short">
		<td>short (8 bytes)</td>
		<td>Short block headers start with a zero-padded string of <b>6 bytes</b> followed by the declaration of the block's length <b>2 bytes</b> (<a href="#format-uint">uint</a>).

			<pre>{%- filter dedent %}
				zero = .

				BODY..nn

				followed by nn bytes of data, i.e. the block's "body".
			{% endfilter -%}</pre>
		</td>
	</tr>
{% endblock %}

{% block textbelow %}
<h3 id="{{ iid }}-allow-empty">Empty blocks</h3>
<p>
	<p>Regular blocks must not be empty unless all children are marked as optional.</p>

	<p>Some blocks have required children but can still be empty. In other words, either
	they have all their children following the specification, or they have a length of
	zero and have no children at all.</p>

	<p>This is similar to the block being marked as optional - but if an optional block
	is empty, its header would not appear in the file.</p>
</p>

<h3 id="{{ iid}}-allow-mangled">Mangled blocks</h3>
<p>
	<p>Regular blocks have a tag string that consists of plain <tt>ASCII</tt> characters,
	followed by padding with zeros up to a certain width.</p>

	<p>Some blocks have other values than zeros in their padding, which is probably
	a way to prevent these files from being loaded by the game. Sometimes, they also
	have parts of a second tag string in the padding, which might be a hint at why
	these files were disabled.</p>

	<p>Common values in the padding are 01@x, <tt>HEAD.RE</tt>, <tt>BODY.RE</tt>,
	10 48@x, 90 00 10 48@x, 10@x, and E0 E1 E2 E3@x. Most, if not all, files with mangled
	tags are found in the <tt>Textures/</tt> folder of the game.</p>
</p>
{% endblock %}
