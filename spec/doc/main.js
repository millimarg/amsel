// This file is part of amsel.
// SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
// SPDX-License-Identifier: GPL-3.0-or-later

function copyToClipboard(elem) {
    if (!elem) {
        console.error('cannot copy invalid element:', elem)
        return
    }

    var copy = elem.attributes['data-path'].value
    navigator.clipboard.writeText(copy)
    console.log('copied', copy)

    var tooltip = document.getElementById(copy)
    tooltip.innerHTML = "Copied: " + copy
}

function resetClipboardTooltip(elem) {
    if (!elem) {
        console.error('cannot reset invalid element:', elem)
        return
    }

    var copy = elem.attributes['data-path'].value
    var tooltip = document.getElementById(copy)
    tooltip.innerHTML = "Copy to clipboard"
}
