<section>
{#
  This file is part of amsel.
  SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
  SPDX-License-Identifier: GPL-3.0-or-later
#}

{# This template expects 'spec' to be a Spec.Definition object in the context. #}

	{% macro linked_count(item) -%}
		{%- if item.attrs.count == '+' -%}
			{%- set tmp_count_link = 'plus' -%}
			{%- set tmp_count_descr = 'required' -%}
			{%- set tmp_count_text = 'one or more' -%}
		{%- elif item.attrs.count == '?' -%}
			{%- set tmp_count_link = 'optional' -%}
			{%- set tmp_count_descr = 'optional' -%}
			{%- set tmp_count_text = 'zero or one' -%}
		{%- elif item.attrs.count == '*' -%}
			{%- set tmp_count_link = 'any' -%}
			{%- set tmp_count_descr = 'optional' -%}
			{%- set tmp_count_text = 'zero or more' -%}
		{%- elif item.attrs.count == '-' -%}
			{%- set tmp_count_link = 'na' -%}
			{%- set tmp_count_descr = 'na' -%}
			{%- set tmp_count_text = 'undefined' -%}
		{%- else -%}
			{%- set tmp_count_link = 'fixed' -%}
			{%- set tmp_count_descr = 'required' -%}
			{%- set tmp_count_text = item.attrs.count|e -%}
		{%- endif -%}
		<span><a href="#count-{{ tmp_count_link }}">{% if item.attrs.count %}{{ item.attrs.count|e }}{% else %}1{% endif %}&nbsp;<small><i>{{ tmp_count_descr }}</i></small></a></span>
	{%- endmacro %}

	{% macro description(item, which='what', wrap='span', id='', otherwise='', pre='', post='') -%}
		{# usage: description(item, which='what', wrap='p', id='', otherwise'<p>nothing</p>', pre='&nbsp;') #}
		{%- set temp_descr = item.find(which, recursive=False).contents -%}
		{%- if temp_descr|list -%}
			{{ pre }}<{{ wrap|e }} {% if id %}id="{{ id|e }}"{% endif %}>
			{%- for i in temp_descr -%}
				{%- if i.name == 'pre' -%}
					<pre><code>{{ i.string|dedent|trim }}</code></pre>
				{%- else -%}
					{{ i|xe|flags }}
				{%- endif -%}
			{%- endfor -%}
			</{{ wrap|e }}>{{ post }}
		{%- elif otherwise -%}
			{{ otherwise }}
		{%- endif -%}
	{%- endmacro %}

	{% macro isunkown(item) -%}
		{#- expands to non-empty if the given item is 'unknown'; doesn't check for documentation -#}
		{%- if (item.attrs.id == 'unknown') or (item.name == 'data' and item.attrs.type == 'raw' and (item.attrs.id == 'unknown' or not item.attrs.id)) -%}UNKNOWN!{%- endif -%}
	{%- endmacro %}

	{# --- Header and description: #}

	<h2 id="file-{{ spec.ident|e }}">{{ spec.title|e }} <small><a href="#toc">[TOC]</a></small></h2>
	<p><strong>Magic:</strong> <tt>
		{%- if spec.magic -%}
			{{ spec.magic_offset~"@d"|e|flags }}|{{ spec.magic_encoded }} ({{ spec.magic_readable }})
		{%- else -%}
			not available
		{%- endif -%}
	</tt></p>
	<p><strong>Identifier:</strong> <tt>{{ spec.ident|e }}</tt></p>

	{{ description(spec.xml.find('file'), which='what', wrap='p', id='what', otherwise='') }}
	{{ description(spec.xml.find('file'), which='details', wrap='p', id='details', otherwise='') }}

	{# --- Structure overview: #}

	<p>
		{% set root = spec.xml.find('file').find_all(['head', 'group'], recursive=False) %}
		{% set root_head_count = root|length %}
		{% set root_node_count = spec.xml.find('file').find_all(['head', 'data', 'group'], recursive=false)|length %}

		{% if root_head_count > 0 %}

			<h3 id="struct-{{ spec.ident|e }}">Structure</h3>

			{% if root_head_count != root_node_count %}
				<p>This file has data and blocks at its <a href="#file-{{ spec.ident|e }}-root">root level</a>.
					Data fields are not part of this hierarchy outline, see the
					<a href="#file-{{ spec.ident|e }}-root">root level</a> section for all fields.</p>
			{% endif %}

			<ol>
				{# The structure overview is built using a recursive loop over all <head> and <group> tags.
					The link target path is constructed from all <head> titles and groups using a namespace variable. #}
				{% set struct = namespace(path=[spec.ident]) %}

				{%- for item in root recursive %}
					{%- if item.name == 'group' -%}
						{% set group_index = calculate_group_index(item) %}
						{% set struct.path = struct.path + [item.name~"-"~group_index] %}
					{%- else -%}
						{% set struct.path = struct.path + [item.attrs.title] %}
					{%- endif -%}

					{% set next = item.find_all(['head', 'group'], recursive=False) %}
					<li id="struct-{{ struct.path|join('-') }}">{{-''-}}
						<a href="#file-{{ struct.path|join('-') }}">
							{%- if item.name == 'group' and item.attrs.id -%}
								{{- item.attrs.id }} (group)
							{%- else -%}
								{{- item.attrs.title|e or item.name -}}
							{%- endif -%}
						</a>{{-''-}}

						{{- description(item, which='what', wrap='span', id='', otherwise='', pre=':&nbsp;', post='') -}}
						{%- if next|list -%}
							<ol>{{ loop(next) }}</ol>
						{%- endif %}</li>
						{% set struct.path = struct.path[:-1] %}
				{%- else %}
					(no structure available)
				{%- endfor %}
			</ol>
		{% else %}
			{# there is no hierarchy to print #}
		{% endif %}
	</p>

	{# --- Structure details: #}

	{% set struct = namespace(path=[], spec_path=[]) %}

	{%- for item in spec.xml.find_all(['file', 'head', 'group'], recursive=False) recursive %}
		<div>
			{% if item.name == 'file' %}
				{% set struct.path = [spec.ident] %}
				{% set struct.spec_path = [] %}

				{% if root_head_count == 0 %}
					<h3 id="file-{{ struct.path|join('-')|e }}-root">Structure
						<small><a href="#file-{{ spec.ident }}">[description]</a></small>
					</h3>
				{% else %}
					<h3 id="file-{{ struct.path|join('-')|e }}-root">Root level
						{%- if root_head_count > 0 -%}&nbsp;<small><a href="#struct-{{ struct.path|join('-')|e }}">[structure]</a></small>&nbsp;{%- endif -%}
						<small><a href="#file-{{ spec.ident }}">[description]</a></small>
					</h3>
				{% endif %}

				{% if root_head_count == 0 %}
					<p>This file has no blocks at its root level and has no hierarchical structure.</p>
				{% elif root_head_count == root_node_count %}
					<p>This file has only one main header and no additional data at its root level.</p>
				{% else %}
					<p>This file has data and blocks at its root level. Note that data fields are
						not part of the hierarchy outline above.</p>
				{% endif %}
			{% else %}
				{%- if item.name == 'group' -%}
					{% set group_index = calculate_group_index(item) %}
					{% set struct.path = struct.path + [item.name~"-"~group_index] %}

					{% if item.attrs.id %}
						{% set struct.spec_path = struct.spec_path + ['#' + item.attrs.id] %}
						{% set title = item.attrs.id %}
					{% else %}
						{% set struct.spec_path = struct.spec_path + [item.name] %}
						{% set title = item.name %}
					{% endif %}
				{%- else -%}
					{% set struct.spec_path = struct.spec_path + [item.attrs.title] %}
					{% set struct.path = struct.path + [item.attrs.title] %}
					{% set title = item.attrs.title %}
				{%- endif -%}

				<h3 id="file-{{ struct.path|join('-')|e }}">{{ title|e }} <small><a href="#struct-{{ struct.path|join('-')|e }}">[structure]</a></small></h3>

				<div class="copy-to-clipboard-container">
					<p><strong>Path:</strong> <small>
						{%- for p in struct.path -%}
							{%- set path = struct.path[:loop.index]|join('-')|e -%}

							{%- if loop.index == 1 -%}
								{%- set path = path~"-root" -%}
							{%- endif -%}

							<a href="#file-{{ path }}">{%-
								if p.startswith('group-')
									-%}{% if item.attrs.id %}#{{ item.attrs.id|e }}{% else %}{{ item.name|e }}{% endif %}{%-
								else
									-%}{{ p|e }}{%-
								endif
							-%}</a>

							{%- if not loop.last %} / {% endif %}
						{%- endfor -%}</small>&nbsp;
						<span class="tooltip">
							<button class="copy-to-clipboard" onclick="copyToClipboard(this)" onmouseout="resetClipboardTooltip(this)" data-path="{{
								struct.spec_path|path|e
							}}">
								<span class="tooltiptext" id="{{ struct.spec_path|path|e }}">Copy to clipboard</span>
								📋
							</button>
						</span>
					</p>
				</div>
				<p>{{- '' -}}
					{%- if item.name == 'group' -%}
						<strong>Type:</strong> <dfn title="a group of fixed-width data fields that can occur repeatedly in a certain order">group</dfn>
					{%- else -%}
					<strong>Type:</strong> <a href="#block-type-{{ item.attrs.type|e }}">{{ item.attrs.type|e }}</a>{{- '' -}}
							{%- if item.attrs['allow-mangled'] == 'true' %}&nbsp;<small><i><a href="#block-type-allow-mangled">can be mangled</a></i></small>{% endif -%}
					{%- endif -%}
					&nbsp;/ <strong>Count:</strong> {{ linked_count(item) -}}
					&nbsp;<small><a href="#block-type-allow-empty">{% if item.attrs['allow-empty'] == 'true' %}(allowed to be empty){% else %}(must not be empty){% endif %}</a></small>
					{%- if item.attrs.id %}&nbsp;/ <strong>Identifier:</strong> <i>{{ item.attrs.id|e }}</i>{%- endif -%}
				</p>

				{{- description(item, which='what', wrap='p', id='what', otherwise='', pre='', post='') -}}
				{{- description(item, which='details', wrap='p', id='what', otherwise='', pre='', post='') -}}
			{% endif %}

			{% set block_contents = item.find_all(['head', 'group', 'data'], recursive=False) %}
			{% set extra_details = namespace(items=[]) %}

			{#- ---- block structure overview: -#}

			{% if block_contents|list %}
				<table style="text-align: left;">
					<tr>
						<th>count</th>
						<th>length <dfn title="in bytes">(B)</dfn></th>
						<th><dfn title="offset of this field, counted in bytes from the beginning of this block">local offset</dfn></th>
						<th>format</th>
						<th>identifier</th>
						<th>content</th>
					</tr>
					{%- for blk in block_contents recursive -%}
						{%- set details_id = 'details-'~(struct.path|join('-')|e)~'-'~(loop.index)~'-'~(loop.depth) -%}
						<tr id="{{ details_id }}-overview">
							<td>{# --- count #}{{ linked_count(blk) }}</td>
							<td>{# --- length #}
								{%- if blk.attrs.length == "*" -%}
									<a href="#length-rest">*</a>
								{% elif blk.attrs.length %}
									<a href="#length=fixed">{{ blk.attrs.length }}</a>
								{%- elif blk.name == 'head' and blk.attrs.type == 'long' -%}
									<a href="#length-plus">(16+n)</a>
								{%- elif blk.name == 'head' and blk.attrs.type == 'short' -%}
									<a href="#length-plus">(8+n)</a>
								{%- elif blk.name == 'group' -%}
									<a href="#length-fixed">{{ calculate_group_length(blk) }}</a>
								{%- else -%}
									-
								{%- endif -%}
							</td>
							<td>{# --- local offset #}
								{{- calculate_local_offset(blk) -}}
							</td>
							<td>{# --- format #}
								{%- if blk.name == 'head' -%}
									block (<a href="#block-type-{{ blk.attrs.type|e }}">{{ blk.attrs.type|e }}</a>)
								{%- elif blk.name == 'group' -%}
									<dfn title="a group of fixed-width data fields that can occur repeatedly in a certain order">group</dfn>
								{%- else -%}
									<a href="#format-{{ blk.attrs.type|e }}">{{ blk.attrs.type|e }}</a>
								{%- endif -%}
							</td>
							{% if blk.attrs.id %}{# --- identifier #}
							<td class="copy-to-clipboard-container" id="file-{{ struct.path|join('-')|e }}-#{{ blk.attrs.id|e }}">{{-
								blk.attrs.id|e -}}&nbsp;
								<span class="tooltip">
									<button class="copy-to-clipboard" onclick="copyToClipboard(this)" onmouseout="resetClipboardTooltip(this)"
									{%- set data_path = struct.spec_path + ['#' + blk.attrs.id] -%} data-path="{{ data_path|path|e }}">
										<span class="tooltiptext" id="{{ data_path|path|e }}">Copy to clipboard</span>
										📋
									</button>
								</span>
							</td>
							{% else %}
							<td />
							{% endif %}
							<td>{# --- description #}
								{#- WARNING this doesn't use description() because if/elif/elif -#}
								{%- set data_descr = blk.find('what', recursive=False).contents -%}

								{%- if blk.name == 'data' and not data_descr|list and blk.string -%}
									{%- set data_descr = [blk.string] -%}
								{%- endif -%}

								{%- if loop.depth > 1 %}{% for i in range(loop.depth-1) %}&gt;{% endfor %}&nbsp;{% endif -%}

								{%- if blk.name == 'head' -%}
									<a href="#file-{{ (struct.path+[blk.attrs.title])|join('-')|e }}">{{ blk.attrs.title|e }}</a>
								{%- elif blk.name == 'group' -%}
									<a href="#file-{{ (struct.path+[blk.name~"-"~calculate_group_index(blk)])|join('-')|e }}">{{ blk.name|e }}</a>
								{%- endif -%}

								{%- if data_descr|list -%}
									{% if blk.name in ('head', 'group') %}:&nbsp;{%- endif -%}
									<span>{% for i in data_descr %}{{ i|xe|flags }}{% endfor %}</span>
								{%- elif loop.depth > 1 or blk.name == 'group' -%}
									<small>{% if isunkown(blk) %}unknown {% endif %}:&nbsp;group element</small>
								{%- elif isunkown(blk) -%}
									<small>unknown</small>
								{%- endif -%}
								{%- if blk.name != 'head' and blk.find('details', recursive=False).contents|list -%}
									{%- set extra_details.items = extra_details.items + [(details_id, blk)] -%}
									&nbsp;<a href="#{{ details_id }}">read more ({{ extra_details.items|length }})</a>
								{%- endif -%}
							</td>
						</tr>
					{% endfor %}
				</table>
			{% endif %}

			{#- ---- additional details entries -#}

			{% for key, extra in extra_details.items %}
				<p id="{{ key }}"><strong><a href="#{{ key }}-overview">{{ loop.index }}{% if extra.attrs.id %} {{ extra.attrs.id }}{% endif %}:</a>
					{{- description(extra, which='what', wrap='span', id='', otherwise='&nbsp;<span>'~extra.attrs.id~'</span>', pre='&nbsp;', post='') -}}
				</strong></p>
				{{- description(extra, which='details', wrap='p', id='', otherwise='', pre='<div>', post='</div>') -}}
			{% endfor %}
		</div>

		{#- ---- recursion for groups and children -#}

		{% set next = item.find_all(['head', 'group'], recursive=False) %}

		{%- if next|list -%}
			{{ loop(next) }}
		{%- endif %}</li>
		{% set struct.spec_path = struct.spec_path[:-1] %}
		{% set struct.path = struct.path[:-1] %}
	{%- endfor %}
</section>
