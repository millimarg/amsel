{% extends "topic_base.html" %}

{#
  This file is part of amsel.
  SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
  SPDX-License-Identifier: GPL-3.0-or-later
#}

{% set id = 'topic-mod-structure' %}
{% set name = 'Introduction' %}

{% block text -%}
<p>Mods are files describing changes to to one or multiple game data files. When
	applying a mod, these files are read and dynamically modified. This way can
	be possible to install multiple mods that modify the same files.</p>

<p>Mods exist in three stages: as source, resolved, and compiled files. A mod
	can consist of multiple input files in the source and resolved stages. These
	files are combined into one file when the mod is compiled.</p>
{%- endblock %}

{% block table -%}
	<tr>
		<td>source</td>
		<td>
			<p>Mod source files are XML@t files that can optionally use Jinja2@t templating
			to e.g. include other files. Refer to a @l:tutorial:https://ttl255.com/jinja2-tutorial-part-1-introduction-and-variable-substitution/@
			and @l:the documentation:https://jinja.palletsprojects.com/en/3.1.x/templates/@ for more information.</p>

			Source files also include PNG@t graphics, palettes, etc.
      </td>
	</tr>
	<tr>
		<td>resolved</td>
		<td>
			<p>Resolved mod files no longer contain any Jinja2@t templating. All template commands
			have been resolved, but mod commands are still in their source form. External files
			are still external.</p>

			Resolving a mod file is useful for finding problems with resources or templates
			without having to compile all graphics (which can be time consuming).
		</td>
	</tr>
	<tr>
		<td>compiled</td>
		<td>
			<p>Compiled mods no longer contain templating and all external files have been included.
			All graphics are compiled into the game's custom binary format and all mod commands
			are replaced by actual data nodes.</p>

			Mods are intended to be distributed and installed in compiled form, which can be
			optionally gzip@t compressed. All mods are required to provide the source material
			for rebuilding the mod, though.
		</td>
	</tr>
{%- endblock %}

{% block textbelow -%}

<h2>Directory structure</h2>

<p>A mod lives in a directory of its own. The main XML@t mod file must be called @ext:main.xml@.
Uncompressed compiled mods have by convention the file extension (file name suffix) @ext:.a2x@.
Compressed mods use @ext:.a2z@ as file extension.</p>

<div class="highlight"><pre>
<span class="cm">Required structure:</span>

<span class="nt">my-mod/</span>				<span class="cm">--&gt; compiles to</span> <span class="s">my-mod.a2z</span>
└── <span class="s">main.xml</span>

<span class="cm">Structure with additional mod-specific files:</span>

<span class="nt">my-mod/</span>				<span class="cm">--&gt; compiles to</span> <span class="s">my-mod.a2z</span>
├── <span class="s">main.xml</span>
|
└── <span class="nt">graphics/</span>
	├── <span class="na">sidebar.png</span>
	└── <span class="na">sidebar.gpl</span>
</pre></div>



<h2>Mod file structure</h2>

<p>Mod files are processed from top to bottom: references must be declared before they
can be used in statements. Some mod commands are translated during compilation into
multiple smaller statements that only appear later in the file.</p>

{% highlight 'xml+jinja' %}
<?xml version="1.0" encoding="utf-8"?>
<file type="mod">
	<!-- 1. metadata -->
	...

	<!-- 2. preparation -->
	...

	<!-- 3. changesets -->
	...
</file>
{% endhighlight %}


<h3>Value types</h3>

<p>Variable values are highlighted with curly braces: {{'{type}'}}, e.g. {{'{obj-path}'}}.</p>

{% macro valtype(name, description) %}
	<tr>
		<td><span class="highlight"><pre><span class="s">{{ '{' ~ name|e ~ '}' }}</span></span></div></td>
		<td>{{ description|flags }}</td>
	</tr>
{% endmacro %}

<div>
	<table>
		<tr>
			<th>Statement</th>
			<th>Explanation</th>
		</tr>

		{# {{ valtype('', '') }} #}
		{{ valtype('string', 'any string, may contain linebreaks') }}
		{{ valtype('line', 'any string, must not contain linebreaks') }}
		{{ valtype('token', 'token string containing only alphanumeric characters plus dash (a-z A-Z 0-9 -), no whitespace') }}
		{{ valtype('version', 'version string following semantic versioning in the form major.minor.patch, e.g. @t:1.0.0@') }}
		{{ valtype('obj-path', 'object path for selecting a structure in a file, e.g. @t:/ANNO/FIGUREN/ENTRY@@@t:101@ ' ~
							   '(see @topic:paths:Object Paths@ for documentation)') }}
		{{ valtype('file-path', 'file path for selecting a resource file, e.g. @t:image.png@') }}
		{{ valtype('spec-id', 'file type identifier, e.g. @t:tex@') }}
		{{ valtype('x-<something>', 'highly specific variables, documented where they are used') }}
	</table>
</div>


<h3>1. Metadata</h3>

<p>Metadata describes the mod. Valid metadata must be provided when writing a mod,
and it can be used by mod management tools to identify installed mods or search for
updates.</p>

{% highlight 'xml+jinja' %}
<?xml version="1.0" encoding="utf-8"?>
<file type="mod">
	<!-- 1. metadata -->
	<meta id="my-mod" author="my-name" version="0.1.0" xver="0.1.0">
		<title>Short title in one line</title>
		<details>
			More detailed description in paragraphs...
		</details>
	</meta>

	<!-- 2. preparation -->
	...

	<!-- 3. changesets -->
	...
</file>
{% endhighlight %}

<div>
	<table>
		<tr>
			<th>Statement</th>
			<th>Explanation</th>
		</tr>
		<tr>
			<td>
{% highlight 'xml+jinja' %}
<meta
	id="{token}"
	author="{line}"
	version="{version}"
	xver="{version}"
>
{% endhighlight %}
			</td>
			<td>
				<ul>
					<li>id: short identifier for the mod, must be unique among an author's mods</li>
					<li>author: handle or name of the mod's author</li>
					<li>version: version number of the mod, must follow @l:Semantic Versioning:https://semver.org@</li>
					<li>xver: version of the modding library that was used when developing the mod, required to track compatibility;
						follows @l:Semantic Versioning:https://semver.org@</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td>
{% highlight 'xml+jinja' %}
<title>{line}</title>
{% endhighlight %}
			</td>
			<td>
				Short title describing the mod in one line.
			</td>
		</tr>
		<tr>
			<td>
{% highlight 'xml+jinja' %}
<details>
	{string}
</details>
{% endhighlight %}
			</td>
			<td>
				More detailed description, may contain line breaks and some markup tags.
				The description should explain what the mod does, and why it applies
				these changes. More in-depth explanations of how this is achieved should
				be added as comments in the modding code itself.
			</td>
		</tr>
	</table>
</div>


<h3>2. Preparation</h3>

<p>Preparation steps are processed before anything else in the mod file.</p>

{% highlight 'xml+jinja' %}
<?xml version="1.0" encoding="utf-8"?>
<file type="mod">
	<!-- 1. metadata -->
	...

	<!-- 2. preparation -->
	<select-file id="my-file" spec="tex" prompt="Statusbar textures file" default="ToolGfx/statusbaraddon.tex" />
	<prepare action="combine-figuren" />

	<!-- 3. changesets -->
	...
</file>
{% endhighlight %}

<div>
	<table>
		<tr>
			<th>Statement</th>
			<th>Explanation</th>
		</tr>
		<tr>
			<td>
{% highlight 'xml+jinja' %}
<select-file
	id="{token}"
	spec="{spec-id}"
	prompt="{line}"
	default="{file-path}"
/>
{% endhighlight %}
			</td>
			<td>
				<p>Ask the user to specify the path to an input file. The file will be
				loaded and can be referenced in changesets by its ID.</p>

				<p>All selections will be prompted before applying the mod.</p>

				<ul>
					<li>id: handle for later references</li>
					<li>spec: required @l:file format:#toc-spec@ for the input file</li>
					<li>prompt: line that will be shown to the user so they can select the right file</li>
					<li>default: optional default path that will be suggested to the user</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td>
{% highlight 'xml+jinja' %}
<prepare
	action="{x-action}"
/>
{% endhighlight %}
			</td>
			<td>
				<p>Run a predefined preparation action. Some complex operations that
				are commonly required are available through this shortcut.</p>

				<ul>Actions:
					<li>combine-figuren: combine @ext:Figuren???.dat@ and @ext:FigurenHeader.dat@
						files into one file called @ext:Figuren.dat@.</li>
				</ul>
			</td>
		</tr>
	</table>
</div>


<h3>3. Changesets</h3>

<p>TBD.</p>

{% highlight 'xml+jinja' %}
<?xml version="1.0" encoding="utf-8"?>
<file type="mod">
	<!-- 1. metadata -->
	...

	<!-- 2. preparation -->
	...

	<!-- 3. changesets -->
	<changeset>
		...
	</changeset>
</file>
{% endhighlight %}


<h3>Basic example mod</h3>

{% highlight 'xml+jinja' %}
<?xml version="1.0" encoding="utf-8"?>
<file type="mod">
	<meta id="my-mod" author="my-name" version="0.1.0" xver="0.1.0">
		<title>An example mod</title>
		<details>
			This example mod switches some values without changing anything
			in the end.
		</details>
	</meta>
</file>
{% endhighlight %}


<h2>Mod and spec file schema</h2>

<p>Currently the best documentation for the mod file format is the schema
specification for mods and spec files. It is included below for easy access.</p>

{% highlight 'rnc' %}
{% include "files/schema.rnc" %}
{% endhighlight %}


{%- endblock %}
